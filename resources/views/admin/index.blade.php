<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8" data-ng-app="app"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9" data-ng-app="app"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" data-ng-app="app"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
    <!-- START @META SECTION -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Alex Nikitin">
    <title data-ng-bind="$state.current.data.pageTitle + ' | Admin area'"></title>
    <!--/ END META SECTION -->

    <!-- START @FAVICONS -->
    <!--<link data-ng-href="img/ico/angularjs/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
    <link data-ng-href="img/ico/angularjs/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link data-ng-href="img/ico/angularjs/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link data-ng-href="img/ico/angularjs/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed" sizes="57x57">
    <link data-ng-href="img/ico/angularjs/apple-touch-icon.png" rel="shortcut icon" sizes="16x16">-->
    <!--/ END FAVICONS -->

    <!-- START @FONT STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
    <!--/ END FONT STYLES -->

    <!-- START @GLOBAL MANDATORY STYLES -->
    <link data-ng-href="/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link data-ng-href="/vendor/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link data-ng-href="/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link data-ng-href="/vendor/angular-loading-bar/build/loading-bar.min.css" rel="stylesheet">
    <link data-ng-href="/vendor/toastr/toastr.css" rel="stylesheet">
    <link data-ng-href="/vendor/tr-ng-grid/trNgGrid.min.css" rel="stylesheet">
    <!--/ END GLOBAL MANDATORY STYLES -->

    <!-- START @PAGE LEVEL STYLES -->
    <link id="load_css_before"/>
    <!--/ END PAGE LEVEL STYLES -->

    <!-- START @THEME STYLES -->
    <link data-ng-href="/css/reset.css" rel="stylesheet">
    <link data-ng-href="/css/layout.css" rel="stylesheet">
    <link data-ng-href="/css/components.css" rel="stylesheet">
    <link data-ng-href="/css/plugins.css" rel="stylesheet">
    <link data-ng-href="/css/angularjs-theme.css" rel="stylesheet" id="theme">
    <!--/ END THEME STYLES -->

    <!-- START @ANGULARJS STYLES -->
    <link data-ng-href="/css/angular-custom.css" rel="stylesheet">
    <!-- END @ANGULARJS STYLES -->

    <link data-ng-href="/css/custom.css" rel="stylesheet">


    <!-- START @IE SUPPORT -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/vendor/html5shiv/dist/html5shiv.min.js"></script>
    <script src="/vendor/respond-minmax/dest/respond.min.js"></script>
    <![endif]-->
    <!--/ END IE SUPPORT -->
</head>
<!--/ END HEAD -->

<body data-ng-controller="WrapperController" class="page-session page-sound page-header-fixed page-sidebar-fixed">

<!--[if lt IE 9]>
<p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- START @WRAPPER -->
<section id="wrapper">

    <!-- START @HEADER -->
    <header data-ng-include="'/app/partials/header.html'" id="header"></header> <!-- /#header -->
   {{-- @include('admin.header') --}}
    <!--/ END HEADER -->

    <!-- START @SIDEBAR LEFT -->
   <aside data-sidebar-left-nicescroll data-ng-include="'/app/partials/sidebar-left.html'" id="sidebar-left" class="sidebar-circle"></aside><!-- /#sidebar-left-->
   {{-- @include('admin.sidebar-left') --}}
    <!--/ END SIDEBAR LEFT -->

    <!-- START @PAGE CONTENT -->
    <section id="page-content">

        <!-- Start page header -->
       <div class="header-content" data-ng-include="'/app/partials/header-content.html'"></div><!-- /.header-content-->
       {{-- @include('admin.header-content') --}}
        <!--/ End page header -->

        <!-- Start body content -->
        <div data-ui-view class="body-content animated fadeIn"></div><!-- /.body-content -->
        <!--/ End body content -->

        <!-- Start footer content -->
       <footer class="footer-content" data-ng-include="'/app/partials/footer.html'"></footer><!-- /.footer-content-->
       {{-- @include('admin.footer') --}}
        <!--/ End footer content -->

    </section><!-- /#page-content -->
    <!--/ END PAGE CONTENT -->

</section><!-- /#wrapper -->
<!--/ END WRAPPER -->

<!-- START @BACK TOP -->
<div data-back-top id="back-top" class="animated pulse circle">
    <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->

<!-- START @CORE PLUGINS -->
<script src="/vendor/jquery/dist/jquery.min.js"></script>
<script src="/vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendor/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="/vendor/jquery.sparkline.min/index.js"></script>
<script src="/vendor/jquery-easing-original/jquery.easing.min.js"></script>
<script src="/vendor/ionsound/js/ion.sound.min.js"></script>
<script src="/vendor/bootbox/bootbox.js"></script>
<script type="text/javascript" src="/vendor/toastr/toastr.min.js"></script>
<!--/ END CORE PLUGINS -->

<!-- BEGIN @CORE ANGULARJS PLUGINS -->
<script src="/vendor/angular/angular.js"></script>
<script src="/vendor/angular-sanitize/angular-sanitize.min.js"></script>
<script src="/vendor/angular-touch/angular-touch.min.js"></script>
<script src="/vendor/angular-animate/angular-animate.min.js"></script>
<script src="/vendor/oclazyload/dist/ocLazyLoad.min.js"></script>
<script src="/vendor/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="/vendor/angular-loading-bar/build/loading-bar.min.js"></script>
<script type="text/javascript" src="/vendor/angular-restmod/dist/angular-restmod-bundle.js"></script>
<script type="text/javascript" src="/vendor/tr-ng-grid/trNgGrid.min.js"></script>
<script type="text/javascript" src="/vendor/angular-form-for/dist/form-for.min.js"></script>
<script type="text/javascript" src="/js/restmod.custom.style.js"></script>
<!-- END @CORE ANGULARJS PLUGINS -->

<!-- Bootstrapping -->
<script type="text/javascript" src="/app/app.module.js"></script>
<script type="text/javascript" src="/app/config.js"></script>
<script type="text/javascript" src="/app/directives.js"></script>
<!--blocks-->
<script type="text/javascript" src="/app/blocks/exception/exception.module.js"></script>
<script type="text/javascript" src="/app/blocks/exception/exception-handler.provider.js"></script>
<script type="text/javascript" src="/app/blocks/exception/exception.js"></script>
<script type="text/javascript" src="/app/blocks/logger/logger.module.js"></script>
<script type="text/javascript" src="/app/blocks/logger/logger.js"></script>
<script type="text/javascript" src="/app/blocks/router/router.module.js"></script>
<script type="text/javascript" src="/app/blocks/router/routehelper.js"></script>
<!--core-->
<script type="text/javascript" src="/app/core/core.module.js"></script>
<script type="text/javascript" src="/app/core/constants.js"></script>
<script type="text/javascript" src="/app/core/common.js"></script>
<script type="text/javascript" src="/app/core/config.js"></script>
<!-- data -->
<script type="text/javascript" src="/app/data/data.module.js"></script>
<!-- /data -->
<!-- START @PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/app/wrapper.js"></script>
<script type="text/javascript" src="/app/admin/dashboard/dashboard.module.js"></script>
<script type="text/javascript" src="/app/admin/users/users.module.js"></script>
<script type="text/javascript" src="/app/admin/home/module.js"></script>
<script type="text/javascript" src="/app/admin/auportfolio/module.js"></script>
<script type="text/javascript" src="/app/admin/nzportfolio/module.js"></script>
<script type="text/javascript" src="/app/admin/concentrated/module.js"></script>
<script type="text/javascript" src="/app/admin/stocks/module.js"></script>
<script type="text/javascript" src="/app/admin/notes/notes.module.js"></script>
<script type="text/javascript" src="/app/admin/reports/module.js"></script>
<script type="text/javascript" src="/app/admin/thematics/module.js"></script>
<!--/ END PAGE LEVEL SCRIPTS -->
<!--/ END JAVASCRIPT SECTION -->

</body>
<!--/ END BODY -->

</html>
