<div class="products-container">
    <div class="section">
        <div class="row products-row ">
            <div class="products-column" style="font-size: 1.8em;">
                <h3>All Australasian Trading Management members receive the following features</h3>
            </div>
        </div>
    </div>

    <div class="section">
        <div class="row products-row ">
            <div class="col-lg-4 col-md-4 col-sm-4 products-column">

                <h3>ATM's Model Portfolios</h3>

                <div class="products-model-portfolios">
                    <p>ATM Australian Equity Portfolio<br>
                        ATM NZ Equity Portfolio</p>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 products-column">

                <h3>Our Daily Newsletter - At the Moment News</h3>

                <p>Which discusses ATM's take on global market moves, trade recommendations and strategies, including how
                    portfolios are positioned.</p>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 products-column">

                <h3>Market Mover Reports</h3>

                <p>Analysing major market events and the implications for investors.</p>

            </div>
        </div>
    </div>

<div class="section">
    <div class="row products-row ">
        <div class="col-lg-4 col-md-4 col-sm-4 products-column">

            <h3>Stock Specific Reports</h3>

            <p>Covering a wide range of companies across all industries and of all sizes. ATM will often recommend
                opportunities in smaller companies which are not widely followed by the market.</p>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 products-column">

            <h3>Top Trade Ideas</h3>

            <p>ATM publishes proprietary top trade ideas, which are high conviction calls suitable for investors with a
                higher risk tolerance.</p>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 products-column">

            <h3>Investor Education Centre</h3>

            <p>The financial world can be full of complicated jargon and ATM seeks to explain often overcomplicated
                concepts to its members in an easy to understand format.</p>

        </div>
    </div>
</div>

</div>