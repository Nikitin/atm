<div class="container-right-column">
    <h2 class="members_info_main_right_column_title">ATM TOP TRADES</h2>
    <table class="member_home_table topTradesBlock tablesorter">
        <thead class="">
        <tr>
            <th>Trade<!--<i class="fontello-icon icon-sort"></i>--></th>
            <th>Initial Level<!--<i class="fontello-icon icon-sort"></i>--></th>
            <th>Current Level<!--<i class="fontello-icon icon-sort"></i>--></th>
        </tr>
        </thead>
        <tbody class="">
        @if($topTrades)
        @foreach($topTrades as $topTrade)
            <tr>
                <td>{{isset($topTrade[0]) ? $topTrade[0] : ' '}}</td>
                <td>{{isset($topTrade[1]) ? $topTrade[1] : ' '}}</td>
                <td>{{isset($topTrade[2]) ? $topTrade[2] : ' '}}</td>
            </tr>
        @endforeach
            @else
            <p>There is not data</p>
        @endif
        </tbody>
    </table>
</div>