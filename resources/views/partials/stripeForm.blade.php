@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    {{--<script type="text/javascript" src="https://js.stripe.com/v2/stripe-debug.js"></script>--}}
    <script src="{{asset('/js/front/billing.js')}}"></script>
@endsection

<div class="form_group">
    <select name="plan" data-validation="notempty">
        <option value="">Choose plan</option>
        <option value="trial">Trial</option>
        <option value="annual">Annual</option>
        <option value="6_months">6 months</option>
    </select>
</div>

<div class="form_group">
    <label>
        <input type="text" size="20" data-stripe="number"  placeholder="Card Number*"/>
    </label>
</div>

<div class="form_group">
    <label>
        <input type="text" size="4" data-stripe="cvc"  placeholder="CVC*"/>
    </label>
</div>

<div class="form_group">
    <label>
        <span>Expiration (MM/YYYY)</span>
        <input type="text" size="2" data-stripe="exp-month" />
    </label>
    <span> / </span>
    <input type="text" size="4" data-stripe="exp-year" />
    <input type="hidden" id="publishable-key" value="{{env('STRIPE_KEY')}}" />
    <div style="display: none" class="payment-errors alert alert-danger"></div>
</div>