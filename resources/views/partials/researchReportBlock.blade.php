{{--<div class="col-lg-3 col-md-3 col-sm-3 col-lg-push-0 col-md-push-0 col-sm-push-3 col-lg-offset-1 col-md-offset-0">--}}

    <div class="research-report-right-column">
        <h2 class="members_info_main_right_column_title">Hot Topics</h2>

        <div class="top-perfoming-container">
            @if($ytdNotes)
                @foreach($ytdNotes as $note)
                    <a title="{{$note->title}}" href="{{$note->stockLink}}">
                        <div class="top_perfoming_title clearfix">
                            <span class="top_perfoming_name">{{$note->title}}</span>
                            <span class="top_perfoming_percent">+{{$note->ytd}}% Last 12 Months</span>
                        </div>
                        <div class="project-image wow">
                            <img src="/uploads-min{{Croppa::url($note->image, 260, 260, ['resize'])}}" alt="{{$note->title}}"/>
                        </div>
                    </a>
                @endforeach
            @endif
        </div>
    </div>
