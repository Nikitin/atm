<div class="container_who_we_are">
    <section class="section">
        <p class="who_we_are_title">Recent Picks</p>
    </section>
    <div class="row main_section_image full_width clearfix">
        @if($ytdNotes)
       @foreach($ytdNotes as $note)
                <div class="col-lg-15 col-md-6 col-sm-6">
                    <div class="project-image wow fadeInLeft-one" style="max-height: 350px;">
                        <a href="/notes/{{$note->slug}}">
                            <img src="/uploads-min{{Croppa::url($note->image, 475, 475, ['resize'])}}" style="margin-top: -50px;" alt="{{$note->title}}"/>
                        </a>
                    </div>

                    <div class="description_main_image">
                        <a href="/notes/{{$note->slug}}">
                            <h4 class="test">{{$note->title}}</h4>
                        </a>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>{{$note->ytd}}% <small>Last 12 Months</small></span>
                        </div>

                    </div>
                </div>
           @endforeach
        @endif
    </div>
</div>