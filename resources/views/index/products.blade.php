@extends('layouts.landing')
@section('title', 'Products ')
@section('content')

        <!-- CRITICAL CSS -->
        <style>
            .navbar-toggle,a{background-color:transparent}*,:after,:before{box-sizing:border-box;outline:0}.clearfix:after,.navbar-collapse:after,.navbar-header:after,.row:after{clear:both}ol,ul{list-style:none}@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans'),local('OpenSans'),url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')}[class*=" icon-"],[class^=icon-]{font-family:fontello;font-style:normal;font-weight:400;line-height:1em}.fontello-icon,[class*=" icon-"]:before,[class^=icon-]:before{font-family:fontello;font-style:normal;font-weight:400;text-decoration:inherit;font-variant:normal;line-height:1em;-webkit-font-smoothing:antialiased;display:inline-block;text-transform:none;text-align:center;-moz-osx-font-smoothing:grayscale}.nav_header a,a.button,button{text-transform:uppercase}@font-face{font-family:fontello;src:url(../../../public/fonts/fontello.eot);src:url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'),url(../../../public/fonts/fontello.woff?34375466) format('woff'),url(../../../public/fonts/fontello.ttf?34375466) format('truetype'),url(../../../public/fonts/fontello.svg?34375466) format('svg');font-weight:400;font-style:normal}.fontello-icon{font-size:18px;color:#93a6b0}[class*=" icon-"]:before,[class^=icon-]:before{speak:none;width:1em;margin-right:.2em;margin-left:.2em}.icon-facebook:before{content:'\e802'}.icon-twitter:before{content:'\e804'}.icon-google:before{content:'\e805'}html{font-family:sans-serif;-webkit-tap-highlight-color:transparent;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}a{color:#008fd5;text-decoration:none;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}body{background-color:#fff}button{font:inherit;overflow:visible;-webkit-appearance:button;cursor:pointer;font-family:inherit;line-height:inherit}button::-moz-focus-inner{border:0;padding:0}figure{margin:0}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0}.row{margin-left:-15px;margin-right:-15px}.col-lg-12,.col-lg-4,.col-lg-6,.col-md-12,.col-md-4,.col-md-6,.col-sm-12,.col-sm-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px}@media (min-width:768px){.col-sm-12,.col-sm-6{float:left}.col-sm-12{width:100%}.col-sm-6{width:50%}.col-sm-push-3{left:25%}}@media (min-width:992px){.col-md-12,.col-md-4,.col-md-6{float:left}.col-md-12{width:100%}.col-md-6{width:50%}.col-md-4{width:33.33333333%}.col-md-push-0{left:auto}}@media (min-width:1200px){.col-lg-12,.col-lg-4,.col-lg-6{float:left}.col-lg-12{width:100%}.col-lg-6{width:50%}.col-lg-4{width:33.33333333%}.col-lg-push-0{left:auto}}.collapse{display:none}.navbar-collapse{overflow-x:visible;padding-right:15px;padding-left:15px;border-top:1px solid transparent;box-shadow:inset 0 1px 0 rgba(255,255,255,.1);-webkit-overflow-scrolling:touch}@media (min-width:768px){.navbar-header{float:left}.navbar-collapse{width:auto;border-top:0;box-shadow:none}.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}.navbar-toggle{position:relative;float:right;margin-right:15px;margin-top:8px;margin-bottom:8px;background-image:none;border-radius:4px}.navbar-toggle .icon-bar{display:block;height:2px;border-radius:1px}.navbar-toggle .icon-bar+.icon-bar{margin-top:4px}@media (min-width:768px){.navbar-toggle{display:none}}.clearfix:after,.clearfix:before,.navbar-collapse:after,.navbar-collapse:before,.navbar-header:after,.navbar-header:before,.row:after,.row:before{content:" ";display:table}.navbar-toggle .icon-bar{background-color:#008fd5;width:35px;margin:7px 0}@-ms-viewport{width:device-width}.row-inline{letter-spacing:-4px;font-size:0}.row-inline>[class*=col-]{float:none;display:inline-block;letter-spacing:0;font-size:14px;vertical-align:top}.pre_header .navbar-header,.pre_header a[title=ATM]{float:left}.row-inline.align-bottom>[class*=col-]{vertical-align:bottom}.navbar-toggle{border:1px solid #008fd5;padding:5px 10px}.navbar-header{margin-left:25px}a,body,div,figcaption,figure,h3,header,html,i,img,li,nav,ol,p,section,span,ul{margin:0;padding:0;border:0;font-size:100%;vertical-align:baseline}img{vertical-align:middle;border:0}figcaption,figure,header,nav,section{display:block}body,html{height:100%;min-width:320px;color:#73848e}body{font-family:'Open Sans',sans-serif;font-size:14px;line-height:22px;font-weight:400;color:#73848e}.nav_header a,h3{color:#23323a}.clearfix:after,.clearfix:before{display:table;content:" "}.section{max-width:1400px;margin:0 auto;padding:0 15px}.lower_header,.pre_header{padding:15px 0}.back-to-top{display:none}:active,:focus{outline:0}.pre_header_right_column{text-align:right}.pre_header_right_column>a{display:inline-block;vertical-align:middle}.navbar-header{display:none}.pre_header_right_column a i{font-size:1.4em;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}.lower_header{z-index:100;position:relative;background-color:rgba(255,255,255,.67);border-bottom:1px solid rgba(245,245,245,.5);-webkit-transition:background-color ease-in-out .15s;transition:background-color ease-in-out .15s}.nav_header li{display:inline-block;margin-left:30px;text-align:left;-webkit-transition:background-color,padding ease-in-out .15s;transition:background-color,padding ease-in-out .15s}.nav_header li:first-child{margin-left:0}.wrapper{-webkit-transition:.3s filter linear;transition:.3s filter linear}a.button,button{background-color:rgba(0,143,213,.73);color:#fff;font-size:1em;padding:10px 20px;display:inline-block;border:none;-webkit-transition:all .3s;transition:all .3s}a.button-member-login{margin-left:10px}.button-member-login-rubber{display:none!important}.products-container{padding-top:20px;padding-bottom:45px}.products-container figure img{position:relative;border-bottom:8px solid #1e73be;width:100%;height:auto}.products-container figure>figcaption{padding:25px 35px}.products-container figure{border:1px solid rgba(0,0,0,.12)}.products-container .products-column h3{text-align:center;border-bottom:1px solid rgba(0,0,0,.12);color:#1e73be;font-size:2.15em;line-height:1.3;margin:0;padding:0 0 24px;font-weight:400;text-transform:uppercase}.products-container .products-column p{margin-top:15px;text-align:justify}.products-container .products-column ol{margin-top:15px;list-style-type:decimal;display:inline-block;min-width:15em}.products-container .products-row:first-child{margin-bottom:35px}.products-container figure>figcaption ol li{text-align:left}.products-model-portfolios{text-align:center}.pre_header a.button,button{margin:5px 0}.products-container .products-row{margin-top:35px}.products-container .products-row:first-child{margin-top:0}@media screen and (max-width:991px){.pre_header>.row-inline.align-bottom>[class*=col-]{vertical-align:top}.products-container .products-column.col-sm-6.col-sm-push-3{margin-top:35px}}@media screen and (max-width:767px){.header{text-align:center}.nav_header{margin-bottom:35px}.nav_header li{display:block;cursor:pointer;padding:10px 0;border-bottom:1px solid #0182c4;margin-left:0!important}.pre_header_right_column{display:block;margin-top:15px}.products-container .products-column{margin-top:35px}.products-container [class*=col-]:first-child.products-column{margin-top:0}.navbar-header{display:block}.products-row{margin-top:35px}.products-container .products-row:first-child{margin-top:0}}@media screen and (min-width:768px){.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}@media screen and (max-width:450px){.pre_header_right_column{display:none}.button-member-login-rubber{display:block!important}.pre_header a[title=ATM]>img{max-width:100%}.pre_header .navbar-header,.pre_header a[title=ATM]{float:none;margin-left:0}.navbar-toggle{float:none;margin-right:0}}
        </style>

    <div class="products-container">
        <div class="section">
            <div class="row  products-row">
                <div class="col-lg-4 col-md-4 col-sm-6 products-column">
                    <figure>
                        <img src="/img/Pie-Chart.jpg" alt="Products daily">
                        <figcaption>
                            <h3>ATM's Model Portfolios</h3>
                            <div class="products-model-portfolios">
                                <ol>
                                    <li>ATM Australian Equity Portfolio</li>
                                    <li>ATM NZ Equity Portfolio</li>
                                </ol>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 products-column">
                    <figure>
                        <img src="/img/products_daily.jpg" alt="Products daily">
                        <figcaption>
                            <h3>Our Daily Newsletter - At the Moment News</h3>
                            <p>Which discusses ATM's take on global market moves, trade recommendations and strategies, including how portfolios are positioned.</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-lg-push-0 col-md-push-0 col-sm-push-3 products-column">
                    <figure>
                        <img src="/img/products_market.jpg" alt="Products market">
                        <figcaption>
                            <h3>Market Mover Reports</h3>
                            <p>Analysing major market events and the implications for investors.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <!--<div class="row section">
            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-push-1 col-md-push-1 col-sm-push-1">
                <div class="row investor-valuation-container row-inline align-middle">
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="wrench">
                            <i class="fontello-icon icon-wrench"></i>
                        </div>
                        <div class="investor-valuation">
                            <h2>Investor Valuation Tool</h2>
                            <h3>For investors we have created an advisory tool to help you with investment decisions</h3>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 align-right">
                        <a href="#" class="slider-button">
                            Read More</a>
                    </div>
                </div>
            </div>

        </div> -->
        <div class="section">
            <div class="row  products-row">
                <div class="col-lg-4 col-md-4 col-sm-6 products-column">
                    <figure>
                        <img src="/img/products_stock.jpg" alt="Products stock">
                        <figcaption>
                            <h3>Stock Specific Reports</h3>
                            <p>Covering a wide range of companies across all industries and of all sizes. ATM will often recommend opportunities in smaller companies which are not widely followed by the market.</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 products-column">
                    <figure>
                        <img src="/img/products_top_trade.jpg" alt="Products top trade">
                        <figcaption>
                            <h3>Top Trade Ideas</h3>
                            <p>ATM publishes proprietary top trade ideas, which are high conviction calls suitable for investors with a higher risk tolerance.</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-lg-push-0 col-md-push-0 col-sm-push-3 products-column">
                    <figure>
                        <img src="/img/products_investor.jpg" alt="Products investor">
                        <figcaption>
                            <h3>Investor Education Centre</h3>
                            <p>The financial world can be full of complicated jargon and ATM seeks to explain often overcomplicated concepts to its members in an easy to understand format.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

    </div>

@endsection