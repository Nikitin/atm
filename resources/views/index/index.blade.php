@extends('layouts.landing')
@section('title', 'Main page ')

    @section('modal')
    @include('index._modal')
    @endsection
    @section('content')

            <!-- CRITICAL CSS -->
    <style>
        .video-container {
            top: 50%;
            width: 100%;
            max-width: 650px;

            position: absolute;
            right: 20px;
            z-index: 1000;
            margin-top: -170px;
        }

        .video {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 5px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe, .video-container object, .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .slider-container {
            position: relative;
        }

        @media screen and (max-width: 991px) {
            .video-container {
                display: none;
            }
        }

        .banner {
            padding-top: 60px;
            padding-bottom: 25px;
        }

        .banner-title {
            color: white;
            line-height: 1.2;
        }

        .banner a.button.biggest {
            border-radius: 5px;
            color: #FFF;
            border: 1px solid #4cae4c;
            width: 100%;
            max-width: 250px;
            margin-top: 0;
            padding: 25px 0;
            background-color: rgba(92, 184, 92, 0.69);
        }

        .banner .button-container {
            position: relative;
            top: 155px;
            width: 100%;
        }

        .banner .row {
            height: 100%;
        }

        .banner .banner-column {
            position: relative;
            height: 100%;
        }

        .banner ul {
            text-align: left;
        }

        .banner .banner-description {
            display: inline-block;
            position: relative;
            top: 330px;
            left: 15px;
            color: white;
            font-size: 2.2em;
            font-weight: 600;
            line-height: 100%;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .banner li {
            padding-left: 15px;
            padding-right: 15px;
            border-left: 3px solid white;
            float: left;
        }

        .banner li:first-child {
            padding-left: 0;
            border-left: none;
        }

        @media screen and (min-width: 992px) {
            .banner .banner-column:first-child {
                margin-left: -30px;
            }
        }



    </style>

    <div class="page-content clearfix">

        <main class="main">
            <section class="banner">
                <div class="row section">
                    <div class="col-lg-6 col-md-7 banner-column align-center">
                        <h1 class="banner-title">Stock Markets. Simplified</h1>

                        <h2 class="banner-title">15 DAY FREE TRIAL MEMBERSHIP</h2>

                        <div class="button-container align-center" style="top: 40px;">
                            <a href="/auth/register" class="button biggest" title="Sign up" style="font-size: 2em">
                                <i class="fontello-icon icon-info-circled"></i>SIGN UP
                            </a>
                        </div>

                        <ul class="banner-description">
                            <li>Performance</li>
                            <li>Independence</li>
                            <li>Boutique</li>
                        </ul>


                    </div>
                    <div class="col-lg-6 col-md-5 banner-column align-center">
                        <div class="video-container">
                            <div class="video">
                                <iframe src="https://www.youtube.com/embed/ts5uwWMfNn8" frameborder="0"
                                        allowfullscreen=""
                                        webkitallowfullscreen="" msallowfullscreen=""></iframe>
                            </div>
                        </div>
                        {{--<h1 class="banner-title">15 DAY FREE TRIAL<br>MEMBERSHIP</h1>

                        <div class="button-container">
                            <a href="/auth/register" class="button biggest" title="Sign up" style="font-size: 2em">
                                <i class="fontello-icon icon-info-circled"></i>SIGN UP
                            </a>
                        </div>
                        <p class="banner-description">Access our expert reports & recommendations</p>--}}
                    </div>

                </div>

            </section>

            @include('index._ytdNotes')
            <div class="container_freetrial">
                <section class="section">
                    <div class="row row-inline align-middle">
                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <h2>
                                <span>15 DAY FREE TRIAL MEMBERSHIP:</span>
                                Access our expert reports & recommendations free of charge with no obligation
                            </h2>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 align-right">
                            <a href="/auth/register" class="button biggest" title="Sign up" style="font-size: 2em">
                                <i class="fontello-icon icon-info-circled"></i>SIGN UP
                            </a>
                        </div>
                    </div>
                </section>
            </div>
            <div class="clearfix container_recent_posts">

                @include('index._recent-posts')

            </div>
            <div class="container_services_list clearfix">
                <section class="section">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="bg1b-container wow fadeInLeft-one clearfix" data-wow-offset="70">
                                <img alt="Australian Trading Management" class="container_services_list_left_image"
                                     src="img/bg1b.png" alt="">
                                <img alt="Australian Trading Management" class="bg1b-img" src="img/bg1b2.png" alt=""/>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <ul class="services-list ">
                                <li class="wow fadeInUp-two" data-wow-offset="50">
                                    <img src="img/loupe.png" alt="loupe">

                                    <h3>Independent Research </h3>

                                    <p>ATM is an independent research house unattached to brokers and banks, hence we
                                        have no conflicts of interest and our only goal is to generate positive returns
                                        for our members</p>
                                </li>
                                <li class="wow fadeInUp-two" data-wow-offset="50" data-wow-delay="0.15s">
                                    <img src="img/compass.png" alt="compas">

                                    <h3>Performance </h3>

                                    <p>We believe performance means making money for our members and the foundation of
                                        our investment philosophy is to generate positive returns for our members</p>
                                </li>
                                <li class="wow fadeInUp-two" data-wow-offset="50" data-wow-delay="0.30s">
                                    <img src="img/globe.png" alt="globe">

                                    <h3>Boutique Investments</h3>

                                    <p>We invest where we see the most value, which often means not following the main
                                        stream. ATM invest in companies of all sizes across all industries, and we will
                                        often take advantage of opportunities in smaller companies which are not widely
                                        followed by the market</p>
                                </li>
                                <li class="wow fadeInUp-two" data-wow-offset="50" data-wow-delay="0.45s">
                                    <img src="img/airplane.png" alt="airplane">

                                    <h3>Simplistic</h3>

                                    <p>We make our research easy to understand and concise. We take complex issues and
                                        simplify them so that you can make informed and accurate decisions.</p>
                                </li>


                            </ul>
                        </div>
                    </div>
                </section>
            </div>

            <div class="container_freetrial">
                <section class="section">
                    <div class="row row-inline align-middle">
                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <h2>
                                <span>FREE DAILY NEWSLETTER:</span> Get Daily Reports Directly to Your Email
                            </h2>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 align-right">
                            <a href="" title="Subscribe" class="button biggest" style="font-size: 2em"
                               data-toggle="modal" data-target="#atm-modal">
                                <i class="fontello-icon icon-info-circled"></i>SUBSCRIBE
                            </a>
                        </div>
                    </div>
                </section>
            </div>

            <div class="blockquote-container section">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2 class="recent_posts_title">What People Say</h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="testimonial wow fadeInUp-one">
                            <div class="testimonial-header">
                                <div class="testimonial-image">
                                </div>
                                <div class="testimonial-meta">
                                    <span class="testimonial-author">James Wong</span>
                                    <span class="testimonial-job">Accountant</span>
                                </div>
                            </div>
                            <blockquote class="testimonial-quote">I was even happier when I saw my latest bank
                                statements!
                            </blockquote>
                            <div class="testimonial-desc">
                                <p>I'm very happy with ATM's trade recommendations which have been timely and well
                                    researched. I was even happier when I saw my latest bank statements!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="testimonial wow fadeInUp-one">
                            <div class="testimonial-header">
                                <div class="testimonial-image">
                                </div>
                                <div class="testimonial-meta">
                                    <span class="testimonial-author">Mark Walker</span>
                                    <span class="testimonial-job">Engineer</span>
                                </div>
                            </div>
                            <blockquote class="testimonial-quote">Great ideas for my self-managed
                                superannuation.
                            </blockquote>
                            <div class="testimonial-desc">
                                <p>ATM's invests with understandable multi-year thematic ideas in mind,
                                    and
                                    I have been able to replicate their Australasian portfolios in my
                                    self-managed super fund.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection