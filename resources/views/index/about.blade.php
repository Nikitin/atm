@extends('layouts.landing')
@section('title', 'About ')
@section('content')

        <!-- CRITICAL CSS -->
<style>
    .navbar-toggle, a {
        background-color: transparent
    }

    *, :after, :before {
        box-sizing: border-box;
        outline: 0
    }

    .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
        clear: both
    }

    ul {
        list-style: none
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
    }

    [class*=" icon-"], [class^=icon-] {
        font-family: fontello;
        font-style: normal;
        font-weight: 400;
        line-height: 1em
    }

    .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
        font-family: fontello;
        font-style: normal;
        font-weight: 400;
        text-decoration: inherit;
        font-variant: normal;
        line-height: 1em;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        text-transform: none;
        text-align: center;
        -moz-osx-font-smoothing: grayscale
    }

    .nav_header a, a.button, button {
        text-transform: uppercase
    }

    @font-face {
        font-family: fontello;
        src: url(../../../public/fonts/fontello.eot);
        src: url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'), url(../../../public/fonts/fontello.woff?34375466) format('woff'), url(../../../public/fonts/fontello.ttf?34375466) format('truetype'), url(../../../public/fonts/fontello.svg?34375466) format('svg');
        font-weight: 400;
        font-style: normal
    }

    .fontello-icon {
        font-size: 18px;
        color: #93a6b0
    }

    [class*=" icon-"]:before, [class^=icon-]:before {
        speak: none;
        width: 1em;
        margin-right: .2em;
        margin-left: .2em
    }

    .icon-facebook:before {
        content: '\e802'
    }

    .icon-twitter:before {
        content: '\e804'
    }

    .icon-google:before {
        content: '\e805'
    }

    html {
        font-family: sans-serif;
        -webkit-tap-highlight-color: transparent;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%
    }

    a {
        color: #008fd5;
        text-decoration: none;
        -webkit-transition: color ease-in-out .15s;
        transition: color ease-in-out .15s
    }

    body {
        background-color: #fff
    }

    button {
        font: inherit;
        overflow: visible;
        -webkit-appearance: button;
        cursor: pointer;
        font-family: inherit;
        line-height: inherit
    }

    button::-moz-focus-inner {
        border: 0;
        padding: 0
    }

    .sr-only {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0
    }

    .row {
        margin-left: -15px;
        margin-right: -15px
    }

    .col-lg-12, .col-lg-3, .col-lg-6, .col-md-12, .col-md-6, .col-sm-12, .col-sm-6 {
        position: relative;
        min-height: 1px;
        padding-left: 15px;
        padding-right: 15px
    }

    @media (min-width: 768px) {
        .col-sm-12, .col-sm-6 {
            float: left
        }

        .col-sm-12 {
            width: 100%
        }

        .col-sm-6 {
            width: 50%
        }
    }

    @media (min-width: 992px) {
        .col-md-12, .col-md-6 {
            float: left
        }

        .col-md-12 {
            width: 100%
        }

        .col-md-6 {
            width: 50%
        }
    }

    @media (min-width: 1200px) {
        .col-lg-12, .col-lg-3, .col-lg-6 {
            float: left
        }

        .col-lg-12 {
            width: 100%
        }

        .col-lg-6 {
            width: 50%
        }

        .col-lg-3 {
            width: 25%
        }
    }

    .collapse {
        display: none
    }

    .navbar-collapse {
        overflow-x: visible;
        padding-right: 15px;
        padding-left: 15px;
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
        -webkit-overflow-scrolling: touch
    }

    @media (min-width: 768px) {
        .navbar-header {
            float: left
        }

        .navbar-collapse {
            width: auto;
            border-top: 0;
            box-shadow: none
        }

        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important
        }
    }

    .navbar-toggle {
        position: relative;
        float: right;
        margin-right: 15px;
        margin-top: 8px;
        margin-bottom: 8px;
        background-image: none;
        border-radius: 4px
    }

    .navbar-toggle .icon-bar {
        display: block;
        height: 2px;
        border-radius: 1px
    }

    .navbar-toggle .icon-bar + .icon-bar {
        margin-top: 4px
    }

    @media (min-width: 768px) {
        .navbar-toggle {
            display: none
        }
    }

    .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
        content: " ";
        display: table
    }

    .navbar-toggle .icon-bar {
        background-color: #008fd5;
        width: 35px;
        margin: 7px 0
    }

    @-ms-viewport {
        width: device-width
    }

    .row-inline {
        letter-spacing: -4px;
        font-size: 0
    }

    .row-inline > [class*=col-] {
        float: none;
        display: inline-block;
        letter-spacing: 0;
        font-size: 14px;
        vertical-align: top
    }

    .pre_header .navbar-header, .pre_header a[title=ATM] {
        float: left
    }

    .row-inline.align-bottom > [class*=col-] {
        vertical-align: bottom
    }

    .navbar-toggle {
        border: 1px solid #008fd5;
        padding: 5px 10px
    }

    .navbar-header {
        margin-left: 25px
    }

    a, body, div, h3, h4, header, html, i, img, li, nav, p, section, span, ul {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        vertical-align: baseline
    }

    img {
        vertical-align: middle;
        border: 0
    }

    header, nav, section {
        display: block
    }

    body, html {
        height: 100%;
        min-width: 320px;
        color: #73848e
    }

    body {
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        line-height: 22px;
        font-weight: 400;
        color: #73848e
    }

    .nav_header a, h3, h4 {
        color: #23323a
    }

    .clearfix:after, .clearfix:before {
        display: table;
        content: " "
    }

    .section {
        max-width: 1400px;
        margin: 0 auto;
        padding: 0 15px
    }

    .lower_header, .pre_header {
        padding: 15px 0
    }

    .back-to-top {
        display: none
    }

    :active, :focus {
        outline: 0
    }

    .pre_header_right_column {
        text-align: right
    }

    .pre_header_right_column > a {
        display: inline-block;
        vertical-align: middle
    }

    .navbar-header {
        display: none
    }

    .pre_header_right_column a i {
        font-size: 1.4em;
        -webkit-transition: color ease-in-out .15s;
        transition: color ease-in-out .15s
    }

    .lower_header {
        z-index: 100;
        position: relative;
        background-color: rgba(255, 255, 255, .67);
        border-bottom: 1px solid rgba(245, 245, 245, .5);
        -webkit-transition: background-color ease-in-out .15s;
        transition: background-color ease-in-out .15s
    }

    .nav_header li {
        display: inline-block;
        margin-left: 30px;
        text-align: left;
        -webkit-transition: background-color, padding ease-in-out .15s;
        transition: background-color, padding ease-in-out .15s
    }

    .nav_header li:first-child {
        margin-left: 0
    }

    .wrapper {
        -webkit-transition: .3s filter linear;
        transition: .3s filter linear
    }

    a.button, button {
        background-color: rgba(0, 143, 213, .73);
        color: #fff;
        font-size: 1em;
        padding: 10px 20px;
        display: inline-block;
        border: none;
        -webkit-transition: all .3s;
        transition: all .3s
    }

    .what-sets h3, .what-sets p, .what-sets-text-title {
        text-align: center
    }

    .what-sets-text h3, .what-sets-text h4 {
        font-weight: 400;
        text-transform: uppercase
    }

    .what-sets-img {
        border-bottom: 8px solid #1e73be
    }

    .what-sets-text {
        padding: 25px 35px
    }

    .what-sets-text h4 {
        font-size: 1em;
        color: gray;
        line-height: 1.5
    }

    .what-sets-text h3 {
        color: #1e73be;
        font-size: 2.15em;
        line-height: 1.3;
        margin: 0;
        padding: 0 0 24px
    }

    .what-sets-text-title {
        border-bottom: 1px solid rgba(0, 0, 0, .12)
    }

    .what-sets-text-detail {
        padding-top: 20px
    }

    .what-sets-column {
        border: 1px solid rgba(0, 0, 0, .12);
        box-shadow: 0 1px 0 rgba(0, 0, 0, .12)
    }

    .what-sets h3 {
        color: #363636;
        font-size: 2.3em;
        text-transform: none;
        font-weight: 100;
        line-height: 100%
    }

    .what-sets p {
        margin: 30px 0
    }

    .what-sets-container {
        padding: 45px 0
    }

    a.button-member-login {
        margin-left: 10px
    }

    .button-member-login-rubber {
        display: none !important
    }

    .what-sets-container-row {
        margin-top: 30px
    }

    .pre_header a.button, button {
        margin: 5px 0
    }

    @media screen and (max-width: 1200px) {
        .what-sets-container .what-sets-column {
            margin-top: 35px
        }
    }

    @media screen and (max-width: 991px) {
        .pre_header > .row-inline.align-bottom > [class*=col-] {
            vertical-align: top
        }
    }

    @media screen and (max-width: 767px) {
        .header {
            text-align: center
        }

        .nav_header {
            margin-bottom: 35px
        }

        .nav_header li {
            display: block;
            cursor: pointer;
            padding: 10px 0;
            border-bottom: 1px solid #0182c4;
            margin-left: 0 !important
        }

        .pre_header_right_column {
            display: block;
            margin-top: 15px
        }

        .navbar-header {
            display: block
        }
    }

    @media screen and (min-width: 768px) {
        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important
        }
    }

    @media screen and (max-width: 450px) {
        .pre_header_right_column {
            display: none
        }

        .button-member-login-rubber {
            display: block !important
        }

        .pre_header a[title=ATM] > img {
            max-width: 100%
        }

        .pre_header .navbar-header, .pre_header a[title=ATM] {
            float: none;
            margin-left: 0
        }

        .navbar-toggle {
            float: none;
            margin-right: 0
        }
    }
</style>

<div class="what-sets-container">
    <div class="section">
        <div class="row  what-sets">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h3>ATM’s INVESTMENT TEAM</h3>

                <p>Our Investment team has institutional experience at top tier global
                    investment banks across a wide range of fields including Wealth Management and Trading. The diverse
                    background of our team helps us apply a holistic approach to investment decisions, in order to
                    provide the best possible research for our members.</p>

                <h3>What sets us apart?</h3>
            </div>
        </div>
    </div>


    <div class="section">
        <div class="row clearfix what-sets-container-row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="what-sets-column">
                    <div class="what-sets-img">
                        <img class="img-responsive" src="/img/independant.jpg" alt="independant"/>
                    </div>
                    <div class="what-sets-text">
                        <div class="what-sets-text-title">
                            <h4>research</h4>

                            <h3>Independent</h3>
                        </div>
                        <p class="what-sets-text-detail">
                            Our views and research are completely independent in every sense. We are a research house
                            unattached to brokers, banks, and the firms that we research. Hence we have no conflicts of
                            interest and our only goal is to generate positive returns for our members. Our members'
                            interests always come first.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="what-sets-column">
                    <div class="what-sets-img">
                        <img class="img-responsive" src="/img/perfomance.jpg" alt="perfomance"/>
                    </div>
                    <div class="what-sets-text">
                        <div class="what-sets-text-title">
                            <h4>absolute returns</h4>

                            <h3>performance</h3>
                        </div>
                        <p class="what-sets-text-detail">
                            We believe performance means making money for our members and the foundation of our
                            investment philosophy is to generate positive returns for our members. This contrasts to the
                            fund management industry which is focused on beating benchmarks and will charge its
                            investors fees even when they lose money.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="what-sets-column">
                    <div class="what-sets-img">
                        <img class="img-responsive" src="/img/confidence.jpg" alt="confidence"/>
                    </div>
                    <div class="what-sets-text">
                        <div class="what-sets-text-title">
                            <h4>Investment ideas</h4>

                            <h3>boutique</h3>
                        </div>
                        <p class="what-sets-text-detail">
                            We invest where we see the most value, which often means not following the main stream. ATM
                            invests in companies of all sizes across all industries, and ATM will often take advantage
                            of
                            opportunities in smaller companies. We have created specialised Boutique Investment Sets
                            (BIS), which are based on a global theme
                            that we believe will drive growth in the underlying stocks.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="what-sets-column">
                    <div class="what-sets-img">
                        <img class="img-responsive" src="/img/products_investor.jpg" alt="confidence"/>
                    </div>
                    <div class="what-sets-text">
                        <div class="what-sets-text-title">
                            <h4>Easy to understand</h4>

                            <h3>SIMPLISTIC</h3>
                        </div>
                        <p class="what-sets-text-detail">
                            We make our research easy to understand and concise. We take complex issues and simplify
                            them so that you can make informed and accurate decisions.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section">
        <div class="row ">
            <div class="col-lg-4 col-md-4 col-sm-4 col-lg-push-4 col-md-push-4 col-sm-push-4 align-center register-link">
                <a title="Become a member" href="/auth/register" class="button biggest">
                    <i class="fontello-icon icon-info-circled"></i>BECOME A MEMBER
                </a>
            </div>
        </div>
    </div>


</div>

@endsection