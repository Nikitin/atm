@extends('layouts.landing')
@section('title', 'Sorry account blocked ')
@section('content')
<section class="section">
    <div class="container-account-locked">
        <img src="/img/account_locked_image.jpg" width="100%" height="100%" alt="dog"/>
        <h2>YOUR ACCOUNT HAS BEEN LOCKED</h2>
        <p class="account-locked-description">Your account has been locked. Please click on the following link to
            reset your password. Or call XXXXXXXX
        </p>
        <a href="password-recovery.html" class="button button-reset-password">reset your password</a>
    </div>
</section>
@endsection