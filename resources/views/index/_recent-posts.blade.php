@if(isset($notes))
    <section class="section">
        <h2 class="recent_posts_title">Recent Posts</h2>

        <div class="recent_posts">
            @foreach($notes as $key => $note)
                <div class="recent_posts_separate">
                    <div class="blog_post wow fadeInDown">
                        <div class="post_image">
                            <div class="recent_post_meta">
                                <span class="post_date">{{$note->updated_at->diffForHumans()}}</span>
                            </div>
                            <div class="post_thumbnail">
                                <a href="/notes/{{$note->slug}}" title="{{$note->title}}">
                                    <img src="/uploads-min{{Croppa::url($note->image, 260, 260, ['resize'])}}"
                                         alt="{{$note->title}}"/>
                                </a>
                            </div>
                        </div>
                        <div class="post_content">
                            <ul class="post_content_meta">
                                <li>ATM Team</li>
                                <li><a href="/notes/{{$note->slug}}" class="post_read_more">Read more</a></li>
                            </ul>
                            <h3><a href="/notes/{{$note->slug}}">{{$note->title}}</a></h3>

                            <p class="post_content_text">{{$note->quote}}</p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </section>
@endif