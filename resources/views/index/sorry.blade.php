@extends('layouts.landing')
@section('title', 'become-member ')
@section('content')

    <div class="container-account-locked">
        <img src="/img/chart_down.jpg" width="100%" height="100%" alt="chart-down"/>
        <h2>We are sorry you were unable to complete the subscription process.</h2>
        <p class="account-locked-description">
            If you have any questions you are welcome to call <a title="+64-27-878-3600" href="tel:+64278783600">+64-27-878-3600</a> (New Zealand) and <a title="+1800-026-778" href="tel:1800026778">1800-026-778</a> (Toll Free Australia)<br>or send an email to <a class="" target="_top" title="Write email to ATM" href="mailto:info@atmstrategy.com.au">info@atmstrategy.com.au</a>
        </p>
        <a target="_top" href="mailto:info@atmstrategy.com.au" title="Send an email" class="button ">Send an email</a>
    </div>

@endsection