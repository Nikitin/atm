@section('scripts')
    <script>
        window.modal = function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            (function () {
                'use strict';
                // search applicants
                $('form#make-subscribe').submit(function (e) {
                    e.preventDefault();
                    $(".modal-content").css('opacity', '0.3');
                    var form = $(this);

                    $.post(form.attr('action'), {data: form.serialize()}, function (result) {
                        $(".modal-content").empty();
                        if (result == 'done') {
                            $(".modal-content").html('<h4>Thanks for subscription</h4>');
                        } else if (result == 'subscribed') {
                            $(".modal-content").html('<h4>You already subscribed</h4>');
                        } else if (result == 'registered') {
                            $(".modal-content").html('<h4>You already registered</h4>');
                        } else {
                            $(".modal-content").html('<h4>Error </h4>');
                        }
                        $(".modal-content").css('opacity', '1');
                    });
                });
            })();
        }



    </script>
@endsection
<div class="modal fade" id="atm-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <i class="iconic-button icon-cancel-circled" data-dismiss="modal" aria-hidden="true"></i>

        <div class="modal-content">
            <div class="header_modal form_group">
                <h3>Access daily reports directly from your email</h3>
                <i class="close_modal icon-cancel-circle icon-cancel-circle pull-right" data-dismiss="modal"
                   aria-hidden="true"></i>
            </div>
            <form id="make-subscribe" class="" method="post" action="/subscribe" name="">
                {!! csrf_field() !!}
                <div class="form_group">
                    <input type="text" name="name" placeholder="Name*" data-validation="notempty">
                </div>
                <div class="form_group">
                    <input type="text" name="email" placeholder="Email*" data-validation="notempty;isemail">
                </div>
                <div class="form_group">
                    <input type="tel" name="mobilePhone" value="" placeholder="Mobile" data-validation="isphone">
                </div>

                <input class="button-form" type="submit" value="Send">
                <input class="button-form" type="reset" value="Clear">

            </form>
        </div>
    </div>
</div>