@extends('layouts.landing')
@section('scripts')
    <!-- Google Code for Free Trial Membership Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 935833399;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "1LQ-CNWmtGIQt96evgM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/935833399/?label=1LQ-CNWmtGIQt96evgM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
@endsection
@section('title', 'become-member ')
@section('content')

    <div class="container-account-locked">
        <img src="/img/chart_up.jpg" width="100%" height="100%" alt="chart-up"/>
        <h2>Thanks for subscribing!</h2>
        <p class="account-locked-description">
           Please click the button and we'll redirect you to the member area.
        </p>

        <a href="/member?subscription={{$subscription}}" class="button " title="Click to go member area" >Go to Member Area</a>

    </div>

@endsection