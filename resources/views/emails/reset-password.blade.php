<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<body>
<table id="templateContainer" border="0" width="600" cellspacing="0" cellpadding="0"
       style="color: #505050; margin: 0 auto;">
    <tbody>
    <tr>
        <td align="center" valign="top">
            <table id="templateHeader" border="0" width="600" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="headerContainer"
                        style="font-family: Sanchez, 'Courier New', Courier, 'Lucida Sans Typewriter', 'Lucida Typewriter', monospace; font-size: 42px; text-align: left;"
                        valign="top">
                        <div style="text-align: left; padding-left: 12px; padding-top: 15px; display: block;"><a
                                    href="http://atmstrategy.com.au/" style="display: block;"><img
                                        src="http://atmstrategy.com.au/img/logo.png" alt="ATM" height="55" width="275"></a>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table id="templateBody" border="0" width="600" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="mcnTextContent"
                        style="font-family: 'Noto Sans'; font-size: 16px; padding: 10px 18px 9px 18px;" valign="top">
                        <p>Hi,</p>

                        <p>Someone requested a password reset from an account connected to your email. If it wasn't you
                            please ignore this email. In other case please navigate to this link and reset your
                            password</p><a href="{{ url('password/reset/'.$token) }}"
                                           style="color:#008FD5;">http://atmstrategy.com.au/password/reset/{{$token}}</a></p>
                        <p><br> Thanks,<br>ATM Team.</p>
                    </td>
                </tr>
                <tr>
                    <td class="mcnTextContent"
                        style="font-family: 'Noto Sans'; font-size: 14px; padding: 9px 18px 9px 18px;" valign="top"><em>Copyright
                            © 2015 Australasian Trading Management, All rights reserved.</em> <br><br>

                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>