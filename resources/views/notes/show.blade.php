@extends('layouts.landing')
@section('title', 'Note Page ')
@section('content')

    <!-- CRITICAL CSS -->
    <style>
        .navbar-toggle,a{background-color:transparent}*,:after,:before{box-sizing:border-box;outline:0}.clearfix:after,.navbar-collapse:after,.navbar-header:after,.row:after{clear:both}ul{list-style:none}@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans'),local('OpenSans'),url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')}[class*=" icon-"],[class^=icon-]{font-family:fontello;font-style:normal;font-weight:400;line-height:1em}.fontello-icon,[class*=" icon-"]:before,[class^=icon-]:before{font-family:fontello;font-style:normal;font-weight:400;text-decoration:inherit;font-variant:normal;line-height:1em;-webkit-font-smoothing:antialiased;display:inline-block;text-transform:none;text-align:center;-moz-osx-font-smoothing:grayscale}.nav_header a,a.button,button{text-transform:uppercase}@font-face{font-family:fontello;src:url(../fonts/fontello.eot);src:url(../fonts/fontello.eot?34375466) format('embedded-opentype'),url(../fonts/fontello.woff?34375466) format('woff'),url(../fonts/fontello.ttf?34375466) format('truetype'),url(../fonts/fontello.svg?34375466) format('svg');font-weight:400;font-style:normal}.fontello-icon{font-size:18px;color:#93a6b0}[class*=" icon-"]:before,[class^=icon-]:before{speak:none;width:1em;margin-right:.2em;margin-left:.2em}.icon-facebook:before{content:'\e802'}.icon-twitter:before{content:'\e804'}.icon-google:before{content:'\e805'}html{font-family:sans-serif;-webkit-tap-highlight-color:transparent;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}a{color:#008fd5;text-decoration:none;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}body{background-color:#fff}button{font:inherit;overflow:visible;-webkit-appearance:button;cursor:pointer;font-family:inherit;line-height:inherit}button::-moz-focus-inner{border:0;padding:0}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0}.row{margin-left:-15px;margin-right:-15px}.col-lg-12,.col-lg-3,.col-lg-4,.col-lg-6,.col-lg-9,.col-md-12,.col-md-3,.col-md-4,.col-md-6,.col-md-9,.col-sm-12,.col-sm-4,.col-sm-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px}@media (min-width:768px){.col-sm-12,.col-sm-4,.col-sm-6{float:left}.col-sm-12{width:100%}.col-sm-6{width:50%}.col-sm-4{width:33.33333333%}}@media (min-width:992px){.col-md-12,.col-md-3,.col-md-4,.col-md-6,.col-md-9{float:left}.col-md-12{width:100%}.col-md-9{width:75%}.col-md-6{width:50%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}}@media (min-width:1200px){.col-lg-12,.col-lg-3,.col-lg-4,.col-lg-6,.col-lg-9{float:left}.col-lg-12{width:100%}.col-lg-9{width:75%}.col-lg-6{width:50%}.col-lg-4{width:33.33333333%}.col-lg-3{width:25%}}.collapse{display:none}.navbar-collapse{overflow-x:visible;padding-right:15px;padding-left:15px;border-top:1px solid transparent;box-shadow:inset 0 1px 0 rgba(255,255,255,.1);-webkit-overflow-scrolling:touch}@media (min-width:768px){.navbar-header{float:left}.navbar-collapse{width:auto;border-top:0;box-shadow:none}.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}.navbar-toggle{position:relative;float:right;margin-right:15px;margin-top:8px;margin-bottom:8px;background-image:none;border-radius:4px}.navbar-toggle .icon-bar{display:block;height:2px;border-radius:1px}.navbar-toggle .icon-bar+.icon-bar{margin-top:4px}@media (min-width:768px){.navbar-toggle{display:none}}.clearfix:after,.clearfix:before,.navbar-collapse:after,.navbar-collapse:before,.navbar-header:after,.navbar-header:before,.row:after,.row:before{content:" ";display:table}.navbar-toggle .icon-bar{background-color:#008fd5;width:35px;margin:7px 0}@-ms-viewport{width:device-width}.row-inline{letter-spacing:-4px;font-size:0}.row-inline>[class*=col-]{float:none;display:inline-block;letter-spacing:0;font-size:14px;vertical-align:top}.pre_header .navbar-header,.pre_header a[title=ATM]{float:left}.row-inline.align-bottom>[class*=col-]{vertical-align:bottom}.navbar-toggle{border:1px solid #008fd5;padding:5px 10px}.navbar-header{margin-left:25px}.align-center{text-align:center}a,body,div,footer,h3,h4,header,html,i,img,li,nav,p,section,span,ul{margin:0;padding:0;border:0;font-size:100%;vertical-align:baseline}img{vertical-align:middle;border:0}footer,header,main,nav,section{display:block}body,html{height:100%;min-width:320px;color:#73848e}body{font-family:'Open Sans',sans-serif;font-size:14px;line-height:22px;font-weight:400;color:#73848e}.nav_header a,h3,h4{color:#23323a}.clearfix:after,.clearfix:before{display:table;content:" "}.section{max-width:1400px;margin:0 auto;padding:0 15px}.lower_header,.pre_header{padding:15px 0}.back-to-top{display:none}.back-to-top a{position:fixed;width:50px;height:50px;bottom:20px;right:20px;opacity:.8;z-index:1000;background:url(../img/back-to-top.png) center center no-repeat #0996d2;cursor:pointer;-webkit-transition:opacity .3s;transition:opacity .3s}.lower_header,.notes-page-links li>a,.notes-page-links li>a:before{position:relative}:active,:focus{outline:0}.pre_header_right_column{text-align:right}.pre_header_right_column>a{display:inline-block;vertical-align:middle}.navbar-header{display:none}.pre_header_right_column a i{font-size:1.4em;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}.lower_header{z-index:100;background-color:rgba(255,255,255,.67);border-bottom:1px solid rgba(245,245,245,.5);-webkit-transition:background-color ease-in-out .15s;transition:background-color ease-in-out .15s}.nav_header li{display:inline-block;margin-left:30px;text-align:left;-webkit-transition:background-color,padding ease-in-out .15s;transition:background-color,padding ease-in-out .15s}.nav_header li:first-child{margin-left:0}.wrapper{-webkit-transition:.3s filter linear;transition:.3s filter linear}a.button,button{background-color:rgba(0,143,213,.73);color:#fff;font-size:1em;padding:10px 20px;display:inline-block;border:none;-webkit-transition:all .3s;transition:all .3s}a.button.biggest{visibility:visible!important;padding:20px 60px;font-size:1em}.recent_posts_title{text-transform:capitalize;font-size:1.8em;font-weight:400;line-height:1.8em}.post_content_meta li,.post_date{text-transform:uppercase}.blog_post{margin-top:35px;overflow:hidden}.recent_post_meta{float:left;margin-right:10px}.post_date{color:#23323a;display:block;font-size:.9em;font-weight:300;text-align:center;max-width:5.08em!important}.post_content_meta{list-style:none;padding:10px;border-bottom:1px solid #E5E8EA;overflow:hidden}.post_content .post_content_meta{padding-left:0}.post_content_meta li{float:left;margin-right:5px;color:#23323A;font-size:.75em;background:url(../img/post-meta-square.png) left center no-repeat;padding-left:15px}.post_content .post_content_meta>li>h3{font-size:1.5em}a.button-member-login{margin-left:10px}.button-member-login-rubber{display:none!important}.container-account-locked{padding:30px;text-align:center}.post_content_text{padding-left:75px}.separate_post_content_text{margin-top:15px}.separate_post_content_text>img{margin-right:15px;margin-bottom:15px;float:left}.notes-page-container .recent_post_meta{width:auto}.post_content .post_content_text{margin-top:10px}.notes-page-container .post_content_text{padding-left:0;text-align:justify}.notes-page-container{padding:45px 0}.notes-page-container .blog_post{margin:0}.notes-page-container .recent_posts_title{color:#008fd5}.notes-page-links{padding-left:20px;padding-top:15px;padding-bottom:15px}.notes-page-links-container h3{text-align:center;color:#1e73be;font-size:1.5em!important;margin:0;font-weight:400;text-transform:uppercase}.notes-page-links li{margin-top:15px}.notes-page-links li:first-child{margin-top:0}.notes-page-links li>a{margin-top:5px;line-height:1.4em;font-size:1em}.notes-page-container .post_content>p{margin-top:15px}.notes-page-links li>a:before{content:'';display:inline-block;width:11px;height:11px;margin-right:5px;background:url(../img/read-more-arrow.png) no-repeat;top:-1px}.pre_header a.button,button{margin:5px 0}.note-free-trial-container>a{margin-top:20px;margin-bottom:0;font-size:2em}.note-free-trial-container>div{font-size:1.4em}.notes-page-container .recent_post_meta .post_date{max-width:100%!important}.footer{display:block}.upper-footer{background-color:#098ed1}.container-twetter{float:left;padding:25px;background-color:#1f9cda;position:relative}.container-twetter .icon-twitter{font-size:70px;color:#f5f5f5}.container-twetter:before{content:'';display:block;position:absolute;right:-25px;top:25px;width:10px;height:10px;border-left:25px solid #1f9cda;border-top:0 solid transparent;border-bottom:25px solid transparent}.main-footer{background:#19242a;color:#7ca1b3;padding:30px 0}.main-footer p{margin:10px 0;font-size:.9em}.main-footer h4{margin-bottom:30px;color:#7ca1b3;font-size:1.3em;line-height:28px}.widget-text-one>img{max-width:245px}.widget-text-one p{padding-left:50px}.footer_social_media a,.widget-text-four li{display:inline-block}.footer_social_media i{font-size:1.3em;color:#93a6b0}.widget-text-four li{font-size:.9em;text-align:left;position:relative;padding-left:35px;margin-bottom:10px;vertical-align:top;margin-left:20px}.widget-text-four li:first-child{margin-left:0}.footer-email{display:block}@media screen and (max-width:1200px){.notes-page-container a.button.biggest{text-align:center}}@media screen and (max-width:991px){.pre_header>.row-inline.align-bottom>[class*=col-]{vertical-align:top}.main-footer .align-center{text-align:left}.notes-page-links-container{margin-top:20px}.notes-page-links{border-bottom:1px solid rgba(0,0,0,.12)}.notes-page-container .recent_posts_title{margin-left:0}.notes-page-container a.button.biggest{margin-right:0}}@media screen and (max-width:900px){a.button.biggest{padding-left:45px;padding-right:45px}}@media screen and (max-width:767px){.header,.recent_posts_title{text-align:center}a.button.biggest{margin-top:30px}.nav_header{margin-bottom:35px}.nav_header li{display:block;cursor:pointer;padding:10px 0;border-bottom:1px solid #0182c4;margin-left:0!important}.pre_header_right_column{display:block;margin-top:15px}.main-footer h4{margin-bottom:15px}.main-footer [class*=col-]{margin-top:45px}.main-footer [class*=col-]:first-child{margin-top:0}.navbar-header{display:block}.blog_post .post_content_text{padding-left:0}}@media screen and (min-width:768px){.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}@media screen and (max-width:450px){.pre_header_right_column{display:none}.button-member-login-rubber{display:block!important}.pre_header a[title=ATM]>img{max-width:100%}.pre_header .navbar-header,.pre_header a[title=ATM]{float:none;margin-left:0}.navbar-toggle{float:none;margin-right:0}.container-account-locked{padding:25px}}
    </style>
    @if(!\Auth::check())
        @if (Cookie::get('conversion') !== null)
            <!-- Google Code for Subscription Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 935833399;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "2KlCCIX86GIQt96evgM"; var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/935833399/?label=2KlCCIX86GIQt96evgM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
            <script type="text/javascript">
                document.cookie = 'conversion =; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            </script>
        @endif
    @endif

    <main class="notes-page-container">
        <div class="section">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <div class="blog_post">
                        <div class="post_content clearfix">
                            <ul class="post_content_meta">
                                <li>
                                    <div class="recent_post_meta">
                                        <span class="post_date">{{$note->updated_at->diffForHumans()}}</span>
                                    </div>
                                </li>
                                <li>
                                    <h3><a href="/notes/{{$note->slug}}">{{$note->title}}</a></h3>
                                </li>
                            </ul>
                            <p class="post_content_text separate_post_content_text">
                                @if(\Auth::check())
                                    {!!$note->body!!}
                                @elseif (Cookie::get('subscriber') !== null)
                                    {!!$note->body!!}
                                @else
                                    <img src="/uploads-min{{Croppa::url($note->image, 200, 200, ['resize'])}}" alt="{{$note->title}}">
                                    {{str_limit(strip_tags($note->body), 1000)}}
                                @endif
                            </p>
                        </div>
                        @if(!\Auth::check())
                            @if (Cookie::get('subscriber') !== null)
                                <div class="container-account-locked note-free-trial-container">
                                    <div class="recent_posts_title">Free access to our top stock picks for 2016</div>
                                    <a href="/auth/register" class="button biggest note-button" >Free Trial</a>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-sm-6 col-sm-push-3">
                                        <div class="modal-content">
                                            <div class="header_modal form_group">
                                                <h3>To keep reading the full note </h3>
                                            </div>
                                            <form id="make-subscribe" class="notes-make-subscribe" method="post" action="/subscribe" name="">
                                                {!! csrf_field() !!}
                                                <div class="form_group">
                                                    <input type="text" name="email" placeholder="Enter your email here..." data-validation="notempty;isemail">
                                                </div>
                                                <p style="font-size: 12px; color: #ffffff; text-align: left; margin-bottom: 10px;
                                            line-height: 14px;">By clicking this button, you agree that we may contact you to keep you informed about services we think might interest you. You can unsubscribe at any time.</p>
                                                <input class="button-form" type="submit" value="Click Here, It's Free!">
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            @endif
                        @endif
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 notes-page-links-container">
                    <div class="post_content_meta">
                        <h3>Latest Notes</h3>
                    </div>
                    <ul class="notes-page-links">
                        @if($lastNotes)
                            @foreach($lastNotes as $note)
                                <li>
                                    <a href="/notes/{{$note->slug}}">{{$note->title}}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>

    </main>

@endsection

