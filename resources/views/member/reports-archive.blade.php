@extends('layouts.landing')
@section('title', 'Remind password ')
@section('content')


    <div class="reports-archive-container">
        <div class="section">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2 class="members_info_main_right_column_title">ATM Market Reports Archive</h2>


                    <div class="reports-archive-container-table">

                        <table class="member_home_table research-meta reports-archive-table tablesorter">
                            <thead class="">
                            <tr>
                                <th class="sorter-shortDate dateFormat-ddmmyyyy">Date<i class="fontello-icon icon-sort"></i></th>
                                <th class="sorter-shortDate">Document<i class="fontello-icon icon-sort"></i></th>
                            </tr>
                            </thead>
                            <tbody class="">
                            <tr>
                                <td class="research-news-date">
                                    1/11/2012 12:34 PM
                                </td>
                                <td><a href="">US Dailly: The Markets Have Already Done Much of the Fed's Dirty Word
                                        (Stehn)</a> by Jan Hatzius, Zach J Pandl
                                </td>
                            </tr>

                            <tr>
                                <td class="research-news-date">
                                    17/11/2014 12:34 AM
                                </td>
                                <td><a href="">Europe: Automobilies: Rationalising the risk: PSA up to CL-Buy. Daimler off
                                        CL but remains But</a> [PDF, 28, 521KB] by Stefan Burgstaller, Ashik Kurian
                                </td>
                            </tr>

                            <tr>
                                <td class="research-news-date">
                                    15/11/2015 12:34 PM
                                </td>
                                <td><a href="">Lindt &amp; Sprungli (LISN S): Near-term upside on the stock limitedl down to
                                        Neutral [PDF, 252KB]</a> by Fulvio Cazzol, Alexandra Walvis
                                </td>
                            </tr>

                            <tr>
                                <td class="research-news-date">
                                    5/12/2015 12:34 PM
                                </td>
                                <td><a href="">Lindt &amp; Sprungli (LISN S): Near-term upside on the stock limitedl down to
                                        Neutral [PDF, 252KB]</a> by Fulvio Cazzol, Alexandra Walvis
                                </td>
                            </tr>
                            </tbody>

                            <tfoot>

                            </tfoot>

                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>



@endsection