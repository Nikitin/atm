@extends('layouts.landing')
@section('title', 'PORTFOLIO Thematics ')
@section('content')

        <!-- CRITICAL CSS -->
<style>
    a, table {
        background-color: transparent;
    }

    .sub_menu, ul {
        list-style: none;
    }

    *, :after, :before {
        box-sizing: border-box;
        outline: 0;
    }

    .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
        clear: both;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
    }

    [class*=" icon-"], [class^=icon-] {
        font-family: fontello;
        font-style: normal;
        font-weight: 400;
        line-height: 1em;
    }

    .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
        font-family: fontello;
        font-style: normal;
        font-weight: 400;
        text-decoration: inherit;
        font-variant: normal;
        line-height: 1em;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        text-transform: none;
        text-align: center;
        -moz-osx-font-smoothing: grayscale;
    }

    .menu--link, .nav_header a, a.button, button {
        text-transform: uppercase;
    }

    @font-face {
        font-family: fontello;
        src: url(../../../public/fonts/fontello.eot);
        src: url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'), url(../../../public/fonts/fontello.woff?34375466) format('woff'), url(../../../public/fonts/fontello.ttf?34375466) format('truetype'), url(../../../public/fonts/fontello.svg?34375466) format('svg');
        font-weight: 400;
        font-style: normal;
    }

    .fontello-icon {
        font-size: 18px;
        color: #93a6b0;
    }

    [class*=" icon-"]:before, [class^=icon-]:before {
        speak: none;
        width: 1em;
        margin-right: .2em;
        margin-left: .2em;
    }

    .icon-facebook:before {
        content: '\e802';
    }

    .icon-twitter:before {
        content: '\e804';
    }

    .icon-google:before {
        content: '\e805';
    }

    html {
        font-family: sans-serif;
        -webkit-tap-highlight-color: transparent;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
    }

    a {
        color: #008fd5;
        text-decoration: none;
        -webkit-transition: color ease-in-out .15s;
        transition: color ease-in-out .15s;
    }

    body {
        background-color: #fff;
    }

    button {
        font: inherit;
        overflow: visible;
        -webkit-appearance: button;
        cursor: pointer;
        font-family: inherit;
        line-height: inherit;
    }

    label, strong {
        font-weight: 700;
    }

    button::-moz-focus-inner {
        border: 0;
        padding: 0;
    }

    .sr-only {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
    }

    .row {
        margin-left: -15px;
        margin-right: -15px;
    }

    .col-lg-12, .col-lg-3, .col-lg-6, .col-lg-9, .col-md-12, .col-md-3, .col-md-6, .col-md-9, .col-sm-12, .col-sm-3, .col-sm-6, .col-sm-9, .col-xs-12, .col-xs-3 {
        position: relative;
        min-height: 1px;
        padding-left: 15px;
        padding-right: 15px;
    }

    .col-xs-12, .col-xs-3 {
        float: left;
    }

    .col-xs-12 {
        width: 100%;
    }

    .col-xs-3 {
        width: 25%;
    }

    @media (min-width: 768px) {
        .col-sm-12, .col-sm-3, .col-sm-6, .col-sm-9 {
            float: left;
        }

        .col-sm-12 {
            width: 100%;
        }

        .col-sm-9 {
            width: 75%;
        }

        .col-sm-6 {
            width: 50%;
        }

        .col-sm-3 {
            width: 25%;
        }
    }

    @media (min-width: 992px) {
        .col-md-12, .col-md-3, .col-md-6, .col-md-9 {
            float: left;
        }

        .col-md-12 {
            width: 100%;
        }

        .col-md-9 {
            width: 75%;
        }

        .col-md-6 {
            width: 50%;
        }

        .col-md-3 {
            width: 25%;
        }
    }

    @media (min-width: 1200px) {
        .col-lg-12, .col-lg-3, .col-lg-6, .col-lg-9 {
            float: left;
        }

        .col-lg-12 {
            width: 100%;
        }

        .col-lg-9 {
            width: 75%;
        }

        .col-lg-6 {
            width: 50%;
        }

        .col-lg-3 {
            width: 25%;
        }
    }

    .table-responsive {
        overflow-x: auto;
        min-height: .01%;
    }

    @media screen and (max-width: 767px) {
        .table-responsive {
            width: 100%;
            margin-bottom: 15px;
            overflow-y: hidden;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            border: 1px solid #ddd;
        }
    }

    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
    }

    .collapse {
        display: none;
    }

    .navbar-collapse {
        overflow-x: visible;
        padding-right: 15px;
        padding-left: 15px;
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
        -webkit-overflow-scrolling: touch;
    }

    @media (min-width: 768px) {
        .navbar-header {
            float: left;
        }

        .navbar-collapse {
            width: auto;
            border-top: 0;
            box-shadow: none;
        }

        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important;
        }
    }

    .navbar-toggle {
        position: relative;
        float: right;
        margin-right: 15px;
        margin-top: 8px;
        margin-bottom: 8px;
        background-color: transparent;
        background-image: none;
        border-radius: 4px;
    }

    .navbar-toggle .icon-bar {
        display: block;
        height: 2px;
        border-radius: 1px;
    }

    .navbar-toggle .icon-bar + .icon-bar {
        margin-top: 4px;
    }

    @media (min-width: 768px) {
        .navbar-toggle {
            display: none;
        }
    }

    .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
        content: " ";
        display: table;
    }

    .navbar-toggle .icon-bar {
        background-color: #008fd5;
        width: 35px;
        margin: 7px 0;
    }

    @-ms-viewport {
        width: device-width;
    }

    .row-inline {
        letter-spacing: -4px;
        font-size: 0;
    }

    .row-inline > [class*=col-] {
        float: none;
        display: inline-block;
        letter-spacing: 0;
        font-size: 14px;
        vertical-align: top;
    }

    .pre_header .navbar-header, .pre_header a[title=ATM], .toggle_menu {
        float: left;
    }

    .row-inline.align-middle > [class*=col-] {
        vertical-align: middle;
    }

    .row-inline.align-bottom > [class*=col-] {
        vertical-align: bottom;
    }

    .navbar-toggle {
        border: 1px solid #008fd5;
        padding: 5px 10px;
    }

    .navbar-header {
        margin-left: 25px;
    }

    a, body, div, h2, h3, h4, header, html, i, img, label, li, nav, p, section, span, strong, table, tbody, td, tr, ul {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        vertical-align: baseline;
    }

    img {
        vertical-align: middle;
        border: 0;
    }

    header, main, nav, section {
        display: block;
    }

    body, html {
        height: 100%;
        min-width: 320px;
        color: #73848e;
    }

    body {
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        line-height: 22px;
        font-weight: 400;
        color: #73848e;
    }

    h2, h3, h4 {
        color: #23323a;
    }

    .clearfix:after, .clearfix:before {
        display: table;
        content: " ";
    }

    .section {
        max-width: 1400px;
        margin: 0 auto;
        padding: 0 15px;
    }

    .back-to-top {
        display: none;
    }

    .menu--label, .menu--link, .menu--subitens__opened .sub_menu, .toggle_menu {
        display: block;
    }

    .vertical_nav {
        -webkit-transition: left .3s ease-out;
        transition: left .3s ease-out;
        z-index: 100;
        left: -470px;
        top: 0;
        bottom: 0;
    }

    :active, :focus {
        outline: 0;
    }

    .sub_menu {
        background-color: #fff;
    }

    .sub_menu li {
        margin-top: 2px;
        background-color: #0191D3;
    }

    .menu-vertical {
        top: 0;
        bottom: 40px;
        overflow-y: auto;
        height: 100%;
        width: 100%;
        min-width: 285px;
        margin: 0;
        padding: 0;
        list-style-type: none;
        background: rgba(255, 255, 255, 1);
    }

    .menu--label {
        height: 40px;
        line-height: 40px;
    }

    .menu--item {
        position: relative;
        min-height: 40px;
        line-height: 40px;
    }

    .menu--item__has_sub_menu .menu--link:after {
        position: absolute;
        top: 0;
        font-size: 21px;
        color: #fff;
        right: 15px;
        height: 40px;
        line-height: 40px;
        font-family: fontello;
        content: "\e815";
        -webkit-transition: color .4s ease;
        transition: color .4s ease;
    }

    .menu--link, .sub_menu--link {
        font-size: .875rem;
        text-decoration: none;
    }

    .menu--subitens__opened .menu--link:after {
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .menu--subitens__opened {
        background: #e9e9e9;
    }

    .menu--subitens__opened .menu--link {
        color: #000;
    }

    .menu--link {
        overflow: hidden;
        color: #666;
        font-weight: 400;
        white-space: nowrap;
        cursor: pointer;
    }

    .sub_menu {
        overflow: hidden;
        padding-left: 0;
        display: none;
    }

    .sub_menu--link {
        display: block;
        padding-left: 50px;
        padding-right: 16px;
        color: #f5f5f5;
    }

    @media (min-width: 1200px) {
        .collapse_menu {
            height: 40px;
            line-height: 40px;
            display: block;
            position: absolute;
            bottom: 0;
            width: 100%;
            padding: 0;
            border: 0;
            border-top: 1px solid #e6e6e6;
            background: #f6f6f6;
            color: #666;
            font-size: .875rem;
            text-align: left;
            cursor: pointer;
        }

        .vertical_nav {
            top: 10px;
            left: 0;
        }

        .toggle_menu {
            display: none;
        }
    }

    .lower_header, .pre_header {
        padding: 15px 0;
    }

    .pre_header_right_column {
        text-align: right;
    }

    .pre_header_right_column > a {
        display: inline-block;
        vertical-align: middle;
    }

    .navbar-header {
        display: none;
    }

    .pre_header_right_column a i {
        font-size: 1.4em;
        -webkit-transition: color ease-in-out .15s;
        transition: color ease-in-out .15s;
    }

    .lower_header {
        z-index: 100;
        position: relative;
        background-color: rgba(255, 255, 255, .67);
        border-bottom: 1px solid rgba(245, 245, 245, .5);
        -webkit-transition: background-color ease-in-out .15s;
        transition: background-color ease-in-out .15s;
    }

    .nav_header li {
        display: inline-block;
        margin-left: 30px;
        text-align: left;
        -webkit-transition: background-color, padding ease-in-out .15s;
        transition: background-color, padding ease-in-out .15s;
    }

    .nav_header a {
        color: #23323a;
    }

    .nav_header li:first-child {
        margin-left: 0;
    }

    .toogle_container {
        position: relative;
        height: 50px;
        display: none;
    }

    .wrapper {
        -webkit-transition: .3s filter linear;
        transition: .3s filter linear;
    }

    a.button, button {
        background-color: rgba(0, 143, 213, .73);
        color: #fff;
        font-size: 1em;
        padding: 10px 20px;
        display: inline-block;
        border: none;
        -webkit-transition: all .3s;
        transition: all .3s;
    }

    a.button-member-login {
        margin-left: 10px;
    }

    .button-member-login-rubber {
        display: none !important;
    }

    .vertical_nav {
        margin-bottom: 15px;
        width: 100%;
        background: #fff;
    }

    .menu--item {
        background: #0191D3;
        margin-top: 5px;
    }

    .menu--label {
        color: #f5f5f5;
        padding: 0 20px;
        font-weight: 700;
        -webkit-transition: all .4s ease;
        transition: all .4s ease;
    }

    .menu--subitens__opened .menu--label {
        padding-left: 25px;
        padding-top: 1px;
    }

    .collapse_menu {
        display: none;
    }

    .menu--link {
        position: relative;
        -webkit-transition: all .4s ease;
        transition: all .4s ease;
    }

    .members_info {
        background: rgba(9, 142, 209, .83);
        padding-top: 10px;
        padding-bottom: 10px;
        color: #fff;
        font-size: 1.15em;
        font-weight: 100;
    }

    .members_info_main_right_column_title {
        color: #fff;
        text-transform: uppercase;
        font-size: 1em;
        margin-bottom: 20px;
        padding: 10px 10px 10px 20px;
        background-color: #3d4952;
    }

    .toggle_menu .icon-bar {
        width: 20px;
        -webkit-transform: rotate(-30deg);
        transform: rotate(-30deg);
        -webkit-transition: .35s all;
        transition: .35s all;
    }

    .toggle_menu .icon-bar:first-child {
        -webkit-transform: rotate(30deg);
        transform: rotate(30deg);
    }

    .portfolio {
        padding: 30px 0 45px 0;
    }

    .portfolio-center-column-container {
        padding: 20px 0;
    }

    .portfolio-center-column-container h3 {
        font-size: 17px;
        color: #000;
    }

    .portfolio-center-column-container h4 {
        color: #008fd5;
        margin-top: 20px;
    }

    .portfolio-center-column-container table {
        width: 100% !important;
        table-layout: auto;
        margin-top: 20px;
        border: 1px solid #000;
    }

    .portfolio-center-column-container td {
        text-align: center;
        padding: 5px;
        color: rgba(0, 0, 0, .7);
        border: 1px solid #000;
        background-color: rgba(0, 111, 165, .1);
    }

    .menu-vertical li:first-child {
        margin-top: 7px;
    }

    .menu-vertical > li:first-child {
        margin-top: 0;
    }

    .pre_header a.button, button {
        margin: 5px 0;
    }

    .table-responsive {
        border: none !important;
    }

    .portfolio-thematics > .portfolio {
        padding: 0;
    }

    .container-portfolio-thematics {
        padding: 30px 0 45px;
    }

    @media screen and (max-width: 1200px) {
        .menu--item {
            margin-left: 10px;
            margin-right: 10px;
        }

        .toogle_container {
            display: block;
        }

        .menu-vertical, .vertical_nav {
            position: absolute;
            min-width: 330px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 3px;
        }

        .container-portfolio-thematics .portfolio-thematics {
            width: 100%;
            margin-top: 20px;
        }

        .container-portfolio-thematics {
            padding-top: 0;
        }
    }

    @media screen and (max-width: 991px) {
        .pre_header > .row-inline.align-bottom > [class*=col-] {
            vertical-align: top;
        }

        .portfolio {
            padding-bottom: 20px;
        }
    }

    @media screen and (max-width: 767px) {
        .header {
            text-align: center;
        }

        .nav_header {
            margin-bottom: 35px;
        }

        .nav_header li {
            display: block;
            cursor: pointer;
            padding: 10px 0;
            border-bottom: 1px solid #0182c4;
            margin-left: 0 !important;
        }

        .pre_header_right_column {
            display: block;
            margin-top: 15px;
        }

        .navbar-header {
            display: block;
        }
    }

    @media screen and (min-width: 768px) {
        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important;
        }
    }

    @media screen and (max-width: 450px) {
        .pre_header_right_column {
            display: none;
        }

        .button-member-login-rubber {
            display: block !important;
        }

        .menu-vertical, .vertical_nav {
            width: 100%;
            margin-top: 1px;
            min-width: 300px;
        }

        .pre_header a[title=ATM] > img {
            max-width: 100%;
        }

        .pre_header .navbar-header, .pre_header a[title=ATM] {
            float: none;
            margin-left: 0;
        }

        .navbar-toggle {
            float: none;
            margin-right: 0;
        }
    }
</style>

@include('layouts.member-line-top')

<main class="container-portfolio container-portfolio-thematics menu--subitens__close_thematics">

    <section class="section">
        <div class="row clearfix">

            @include('layouts.member-left-menu')

            <div class="col-lg-9 col-md-9 col-sm-9 portfolio-thematics">
                <div class="portfolio">
                    <h2 class="members_info_main_right_column_title">PORTFOLIO Thematics</h2>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="portfolio-center-column-container">
                                @if(isset($text->body))
                                    {!!$text->body!!}
                                @else
                                    <p>No saved text for PORTFOLIO Thematics</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
</main>


@endsection