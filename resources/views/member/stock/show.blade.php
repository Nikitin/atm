@extends('layouts.landing')
@section('title', 'Stock specific ')
@section('content')

        <!-- CRITICAL CSS -->
    <style>
        a,table{background-color:transparent}*,:after,:before{box-sizing:border-box;outline:0}.clearfix:after,.navbar-collapse:after,.navbar-header:after,.row:after{clear:both}ul{list-style:none}table{border-collapse:collapse;border-spacing:0}@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans'),local('OpenSans'),url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')}[class*=" icon-"],[class^=icon-]{font-family:fontello;font-style:normal;font-weight:400;line-height:1em}.fontello-icon,[class*=" icon-"]:before,[class^=icon-]:before{font-family:fontello;font-style:normal;font-weight:400;text-decoration:inherit;font-variant:normal;line-height:1em;-webkit-font-smoothing:antialiased;display:inline-block;text-transform:none;text-align:center;-moz-osx-font-smoothing:grayscale}.nav_header a,a.button,button{text-transform:uppercase}@font-face{font-family:fontello;src:url(../../../public/fonts/fontello.eot);src:url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'),url(../../../public/fonts/fontello.woff?34375466) format('woff'),url(../../../public/fonts/fontello.ttf?34375466) format('truetype'),url(../../../public/fonts/fontello.svg?34375466) format('svg');font-weight:400;font-style:normal}.fontello-icon{font-size:18px;color:#93a6b0}[class*=" icon-"]:before,[class^=icon-]:before{speak:none;width:1em;margin-right:.2em;margin-left:.2em}.icon-facebook:before{content:'\e802'}.icon-twitter:before{content:'\e804'}.icon-google:before{content:'\e805'}html{font-family:sans-serif;-webkit-tap-highlight-color:transparent;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}a{color:#008fd5;text-decoration:none;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}body{background-color:#fff}button{font:inherit;overflow:visible;-webkit-appearance:button;cursor:pointer;font-family:inherit;line-height:inherit}button::-moz-focus-inner{border:0;padding:0}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0}.row{margin-left:-15px;margin-right:-15px}.col-lg-12,.col-lg-4,.col-lg-6,.col-lg-8,.col-md-12,.col-md-4,.col-md-6,.col-md-8,.col-sm-12,.col-sm-4,.col-sm-6,.col-sm-8{position:relative;min-height:1px;padding-left:15px;padding-right:15px}@media (min-width:768px){.col-sm-12,.col-sm-4,.col-sm-6,.col-sm-8{float:left}.col-sm-12{width:100%}.col-sm-8{width:66.66666667%}.col-sm-6{width:50%}.col-sm-4{width:33.33333333%}}@media (min-width:992px){.col-md-12,.col-md-4,.col-md-6,.col-md-8{float:left}.col-md-12{width:100%}.col-md-8{width:66.66666667%}.col-md-6{width:50%}.col-md-4{width:33.33333333%}}@media (min-width:1200px){.col-lg-12,.col-lg-4,.col-lg-6,.col-lg-8{float:left}.col-lg-12{width:100%}.col-lg-8{width:66.66666667%}.col-lg-6{width:50%}.col-lg-4{width:33.33333333%}}caption{text-align:left;padding-top:8px;padding-bottom:8px;color:#999}.collapse{display:none}.navbar-collapse{overflow-x:visible;padding-right:15px;padding-left:15px;border-top:1px solid transparent;box-shadow:inset 0 1px 0 rgba(255,255,255,.1);-webkit-overflow-scrolling:touch}@media (min-width:768px){.navbar-header{float:left}.navbar-collapse{width:auto;border-top:0;box-shadow:none}.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}@media (min-width:768px){}.clearfix:after,.clearfix:before,.navbar-collapse:after,.navbar-collapse:before,.navbar-header:after,.navbar-header:before,.row:after,.row:before{content:" ";display:table}.pull-right{float:right!important}@-ms-viewport{width:device-width}.row-inline{letter-spacing:-4px;font-size:0}.row-inline>[class*=col-]{float:none;display:inline-block;letter-spacing:0;font-size:14px;vertical-align:top}.pre_header .navbar-header,.pre_header a[title=ATM]{float:left}.row-inline.align-bottom>[class*=col-]{vertical-align:bottom}.navbar-header{margin-left:25px}a,body,caption,div,h2,h4,header,html,i,iframe,img,li,nav,section,span,table,tbody,td,tfoot,time,tr,ul{margin:0;padding:0;border:0;font-size:100%;vertical-align:baseline}img{vertical-align:middle;border:0}header,main,nav,section{display:block}body,html{height:100%;min-width:320px;color:#73848e}body{font-family:'Open Sans',sans-serif;font-size:14px;line-height:22px;font-weight:400;color:#73848e}.nav_header a,h2,h4{color:#23323a}.clearfix:after,.clearfix:before{display:table;content:" "}.section{max-width:1400px;margin:0 auto;padding:0 15px}.lower_header,.pre_header{padding:15px 0}.back-to-top{display:none}:active,:focus{outline:0}.pre_header_right_column{text-align:right}.pre_header_right_column>a{display:inline-block;vertical-align:middle}.navbar-header{display:none}.pre_header_right_column a i{font-size:1.4em;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}.lower_header{z-index:100;position:relative;background-color:rgba(255,255,255,.67);border-bottom:1px solid rgba(245,245,245,.5);-webkit-transition:background-color ease-in-out .15s;transition:background-color ease-in-out .15s}.nav_header li{display:inline-block;margin-left:30px;text-align:left;-webkit-transition:background-color,padding ease-in-out .15s;transition:background-color,padding ease-in-out .15s}.nav_header li:first-child{margin-left:0}.wrapper{-webkit-transition:.3s filter linear;transition:.3s filter linear}a.button,button{background-color:rgba(0,143,213,.73);color:#fff;font-size:1em;padding:10px 20px;display:inline-block;border:none;-webkit-transition:all .3s;transition:all .3s}a.button-member-login{margin-left:10px}.button-member-login-rubber{display:none!important}.member_home_table{table-layout:fixed;border-bottom:3px solid #3d4952;text-align:center;width:100%;font-size:1.15em}.member_home_table td{padding:7px 5px;color:#000}.portfolio-breakdown-table-container{margin-top:25px}.portfolio-breakdown-table-container>table>caption{text-align:left}.portfolio-breakdown-table-container>table>caption>span{padding:5px 20px;display:inline-block;color:#f5f5f5;font-size:1.1em;background-color:#3d4952;text-align:left}.portfolio-breakdown-table-container table{font-size:1em;border-top:3px solid #3d4952}.research-container{font-size:1.1em;margin-top:35px}.research-news-date{min-width:7.7em;vertical-align:top}.research-meta{margin:15px 0 5px}.research-meta tbody>tr>td{padding-left:20px}.research-meta tbody>tr>td:first-child{width:10%}.research-meta caption{background:#eee;margin-bottom:10px;padding:10px 10px 10px 15px}.research-meta caption h4{text-align:left;font-size:1.15em;text-transform:capitalize}.research-meta tr td{text-align:justify;padding:5px 0}.report-archive tbody>tr>td>a{margin-left:15px}.report-archive tbody>tr>td>a:first-child{margin-left:0}.stock-specific{padding:45px 0}.abc-nzx{text-align:right}.abc-nzx tbody tr td:first-child{text-align:left}.stock-specific .stock-specific-title{margin-top:0;margin-bottom:25px}.stock-specific-title span{border-right:1px solid #f5f5f5;height:100%;margin-left:10px;margin-right:10px}.stock-specific-title-rubber{display:none}.pre_header a.button,button{margin:5px 0}.research-container>table{width:100%}@media screen and (max-width:1200px){.box-view{height:550px!important}}@media screen and (max-width:991px){.member_home_table{margin-top:25px}.pre_header>.row-inline.align-bottom>[class*=col-]{vertical-align:top}.portfolio-breakdown-table-container{margin-bottom:35px}}@media screen and (max-width:767px){.header{text-align:center}.nav_header{margin-bottom:35px}.nav_header li{display:block;cursor:pointer;padding:10px 0;border-bottom:1px solid #0182c4;margin-left:0!important}.pre_header_right_column{display:block;margin-top:15px}.research-container{margin-top:0}.box-view{height:350px!important}.navbar-header{display:block}.stock-specific .stock-specific-title{display:none}.stock-specific .stock-specific-title-rubber{display:block}.au-portfolio-title{margin-top:25px}}@media screen and (min-width:768px){.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}@media screen and (max-width:450px){.pre_header_right_column{display:none}.button-member-login-rubber{display:block!important}.pre_header a[title=ATM]>img{max-width:100%}.pre_header .navbar-header,.pre_header a[title=ATM]{float:none;margin-left:0}.box-view{height:300px!important}}
    </style>

    <main class="stock-specific">
        <div class="section">
            <div class="row ">
                <div class="col-lg-8 col-md-8 col-sm-8">

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6"><h2 class="au-portfolio-title stock-specific-title">{{$stock->name}} {{$stock->code}}</h2></div>
                        <div class="col-lg-6 col-md-6 col-sm-6"><h2 class="au-portfolio-title stock-specific-title">
                                Recommendation: {{$stock->recommendation}}</h2></div>
                        <div class="col-sm-12">
                            <h2 class="au-portfolio-title stock-specific-title stock-specific-title-rubber">{{$stock->name}} {{$stock->code}}<span></span>Recommendation: {{$stock->recommendation}}</h2>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">

                            @if($doc)
                                <iframe class="box-view"
                                        src="{!!$doc->viewUrl!!}"
                                        width="100%" height="650px" frameborder="0" allowfullscreen webkitallowfullscreen
                                        msallowfullscreen></iframe>
                                <div class="box-view-container"></div>
                            @else
                                <h2>Document not uploaded</h2>
                            @endif

                        </div>
                    </div>

                </div>


                <div class="col-lg-4 col-md-4 col-sm-4">
                    <h2 class="au-portfolio-title">Key Metrics</h2>

                    <div class="portfolio-breakdown-table-container">


                        <table class="member_home_table abc-nzx">
                            <caption><span>{{$stock->code}}</span></caption>


                            <tbody class="table_sort_tbody">

                            <tr>
                                <td>Current Share Price</td>
                                <td>{{$stock->sharePrice}}</td>
                            </tr>

                            <tr>
                                <td>Recommendation</td>
                                <td>{{$stock->recommendation}}</td>
                            </tr>

                            <tr>
                                <td>Initially Recommended to Buy</td>
                                <td>at {{$stock->price}}</td>
                            </tr>

                            <tr>

                                <td>Dividend Yield</td>
                                <td>{{$stock->dividend}}</td>
                            </tr>

                            <tr>
                                <td>Market Cap</td>
                                <td>{{$stock->marketCap}}</td>
                            </tr>

                            <tr>
                                <td>Last Updated</td>
                                <td>{{ date('F d, Y', strtotime($stock->updated_at)) }}</td>
                            </tr>

                            </tbody>

                        </table>
                    </div>

                    <div class="research-container clearfix">

                        <table class="research-meta report-archive">
                            <caption><h4>Latest Equity Research</h4></caption>
                            <tbody>
                            @if($documents)
                                @foreach($documents as $document)
                                    <tr>
                                        <td class="research-news-date">
                                            <time datetime="{{$document->created_at}}">{{$document->created_at->diffForHumans()}}</time>
                                        </td>
                                        <td><a href="/member/stock/{{$stock->code}}/{{$document->id}}">{{$document->name}}</a> by {{$document->author}}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>

                            <tfoot>


                            <tr>
                                <td colspan="2">
                                    <a class="research-more-link pull-right" href="">More</a>
                                </td>
                            </tr>
                            </tfoot>

                        </table>

                    </div>


                </div>

            </div>
        </div>


        <div class="section">
            <div class="row clearfix ">

                <div class="col-lg-8 col-md-12 col-sm-12">
                </div>

            </div>
        </div>



    </main>

@endsection