@extends('layouts.landing')
@section('title', 'AUPortfolio ')

@section('scripts')
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/modules/data.js"></script>
    @endsection

    @section('content')
            <!-- CRITICAL CSS -->
    <style>
        .portfolio-center-column-container .table-responsive:first-child {
            margin-top: 20px;
        }

        .portfolio-center-column-container thead td, .portfolio-center-column-container thead th {
            border-bottom: none;
            font-size: 13px;
            font-weight: 700;
            text-transform: capitalize;
            padding: 10px 5px;
            color: #f5f5f5;
        }

        .portfolio-center-column-container td {
            text-align: center;
            padding: 5px;
            color: rgba(0, 0, 0, .7);
            border: 1px solid #000;
            background-color: rgba(0, 111, 165, .1);
        }

        .portfolio-center-column-container td, .portfolio-center-column-container th {
            text-align: center;
            padding: 5px;
            color: rgba(0, 0, 0, .7);
            border: 1px solid #000;
            background-color: rgba(0, 111, 165, .1);
        }

        a, table {
            background-color: transparent
        }

        *, :after, :before {
            box-sizing: border-box;
            outline: 0
        }

        .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
            clear: both
        }

        ul {
            list-style: none
        }

        table {
            border-collapse: collapse;
            border-spacing: 0
        }

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
        }

        [class*=" icon-"], [class^=icon-] {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            line-height: 1em
        }

        .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            text-decoration: inherit;
            font-variant: normal;
            line-height: 1em;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            text-transform: none;
            text-align: center;
            -moz-osx-font-smoothing: grayscale
        }

        .nav_header a, a.button, button {
            text-transform: uppercase
        }

        @font-face {
            font-family: fontello;
            src: url(../../../public/fonts/fontello.eot);
            src: url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'), url(../../../public/fonts/fontello.woff?34375466) format('woff'), url(../../../public/fonts/fontello.ttf?34375466) format('truetype'), url(../../../public/fonts/fontello.svg?34375466) format('svg');
            font-weight: 400;
            font-style: normal
        }

        .fontello-icon {
            font-size: 18px;
            color: #93a6b0
        }

        caption, th {
            text-align: left
        }

        [class*=" icon-"]:before, [class^=icon-]:before {
            speak: none;
            width: 1em;
            margin-right: .2em;
            margin-left: .2em
        }

        .icon-facebook:before {
            content: '\e802'
        }

        .icon-twitter:before {
            content: '\e804'
        }

        .icon-google:before {
            content: '\e805'
        }

        .icon-sort:before {
            content: '\e817'
        }

        html {
            font-family: sans-serif;
            -webkit-tap-highlight-color: transparent;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        a {
            color: #008fd5;
            text-decoration: none;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        body {
            background-color: #fff
        }

        button {
            font: inherit;
            overflow: visible;
            -webkit-appearance: button;
            cursor: pointer;
            font-family: inherit;
            line-height: inherit
        }

        button::-moz-focus-inner {
            border: 0;
            padding: 0
        }

        .sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            margin: -1px;
            padding: 0;
            overflow: hidden;
            clip: rect(0, 0, 0, 0);
            border: 0
        }

        .row {
            margin-left: -15px;
            margin-right: -15px
        }

        .col-lg-12, .col-lg-3, .col-lg-6, .col-lg-9, .col-md-12, .col-md-3, .col-md-6, .col-md-9, .col-sm-12, .col-sm-3, .col-sm-6, .col-xs-12 {
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px
        }

        .col-xs-12 {
            float: left;
            width: 100%
        }

        @media (min-width: 768px) {
            .col-sm-12, .col-sm-3, .col-sm-6 {
                float: left
            }

            .col-sm-12 {
                width: 100%
            }

            .col-sm-6 {
                width: 50%
            }

            .col-sm-3 {
                width: 25%
            }

            .col-sm-push-3 {
                left: 25%
            }
        }

        @media (min-width: 992px) {
            .col-md-12, .col-md-3, .col-md-6, .col-md-9 {
                float: left
            }

            .col-md-12 {
                width: 100%
            }

            .col-md-9 {
                width: 75%
            }

            .col-md-6 {
                width: 50%
            }

            .col-md-3 {
                width: 25%
            }

            .col-md-push-0 {
                left: auto
            }
        }

        @media (min-width: 1200px) {
            .col-lg-12, .col-lg-3, .col-lg-6, .col-lg-9 {
                float: left
            }

            .col-lg-12 {
                width: 100%
            }

            .col-lg-9 {
                width: 75%
            }

            .col-lg-6 {
                width: 50%
            }

            .col-lg-3 {
                width: 25%
            }

            .col-lg-push-0 {
                left: auto
            }
        }

        caption {
            padding-top: 8px;
            padding-bottom: 8px;
            color: #999
        }

        .table-responsive {
            overflow-x: auto;
            min-height: .01%
        }

        @media screen and (max-width: 767px) {
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                overflow-y: hidden;
                -ms-overflow-style: -ms-autohiding-scrollbar;
                border: 1px solid #ddd
            }
        }

        .collapse {
            display: none
        }

        .navbar-collapse {
            overflow-x: visible;
            padding-right: 15px;
            padding-left: 15px;
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
            -webkit-overflow-scrolling: touch
        }

        @media (min-width: 768px) {
            .navbar-header {
                float: left
            }

            .navbar-collapse {
                width: auto;
                border-top: 0;
                box-shadow: none
            }

            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        @media (min-width: 768px) {
        }

        .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
            content: " ";
            display: table
        }

        @-ms-viewport {
            width: device-width
        }

        .row-inline {
            letter-spacing: -4px;
            font-size: 0
        }

        .row-inline > [class*=col-] {
            float: none;
            display: inline-block;
            letter-spacing: 0;
            font-size: 14px;
            vertical-align: top
        }

        .pre_header .navbar-header, .pre_header a[title=ATM], .toggle_menu {
            float: left
        }

        .row-inline.align-middle > [class*=col-] {
            vertical-align: middle
        }

        .row-inline.align-bottom > [class*=col-] {
            vertical-align: bottom
        }

        .navbar-header {
            margin-left: 25px
        }

        a, body, caption, div, h2, h3, h4, header, html, i, img, li, nav, p, section, span, table, tbody, td, tfoot, th, thead, tr, ul {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            vertical-align: baseline
        }

        img {
            vertical-align: middle;
            border: 0
        }

        header, main, nav, section {
            display: block
        }

        body, html {
            height: 100%;
            min-width: 320px;
            color: #73848e
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
            line-height: 22px;
            font-weight: 400;
            color: #73848e
        }

        .nav_header a, h2, h3, h4 {
            color: #23323a
        }

        .clearfix:after, .clearfix:before {
            display: table;
            content: " "
        }

        .section {
            max-width: 1400px;
            margin: 0 auto;
            padding: 0 15px
        }

        .lower_header, .pre_header {
            padding: 15px 0
        }

        .back-to-top {
            display: none
        }

        .toggle_menu {
            display: block
        }

        :active, :focus {
            outline: 0
        }

        @media (min-width: 1200px) {
            .toggle_menu {
                display: none
            }
        }

        .pre_header_right_column {
            text-align: right
        }

        .pre_header_right_column > a {
            display: inline-block;
            vertical-align: middle
        }

        .navbar-header {
            display: none
        }

        .pre_header_right_column a i {
            font-size: 1.4em;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        .lower_header {
            z-index: 100;
            position: relative;
            background-color: rgba(255, 255, 255, .67);
            border-bottom: 1px solid rgba(245, 245, 245, .5);
            -webkit-transition: background-color ease-in-out .15s;
            transition: background-color ease-in-out .15s
        }

        .nav_header li {
            display: inline-block;
            margin-left: 30px;
            text-align: left;
            -webkit-transition: background-color, padding ease-in-out .15s;
            transition: background-color, padding ease-in-out .15s
        }

        .nav_header li:first-child {
            margin-left: 0
        }

        .toogle_container {
            position: relative;
            height: 50px;
            display: none
        }

        .wrapper {
            -webkit-transition: .3s filter linear;
            transition: .3s filter linear
        }

        a.button, button {
            background-color: rgba(0, 143, 213, .73);
            color: #fff;
            font-size: 1em;
            padding: 10px 20px;
            display: inline-block;
            border: none;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        a.button-member-login {
            margin-left: 10px
        }

        .button-member-login-rubber {
            display: none !important
        }

        .members_info {
            background: rgba(9, 142, 209, .83);
            padding-top: 10px;
            padding-bottom: 10px;
            color: #fff;
            font-size: 1.15em;
            font-weight: 100
        }

        .member_home_table, .member_home_table thead th {
            border-bottom: 3px solid #3d4952;
            text-align: center
        }

        .member_home_table {
            table-layout: fixed;
            width: 100%;
            font-size: 1.15em
        }

        .member_home_table thead th {
            text-transform: capitalize;
            color: #006fa5;
            cursor: pointer;
            font-weight: 100;
            padding: 7px 5px
        }

        .false-sort-first-column thead th {
            padding-left: 20px
        }

        .sector-breakdown.tablesorter thead th:first-child, .top-ten-holdings.tablesorter thead th:first-child {
            background: 0 0;
            cursor: default
        }

        .member_home_table td {
            padding: 7px 5px;
            color: #000
        }

        .members_info_main_right_column_title {
            color: #fff;
            text-transform: uppercase;
            font-size: 1em;
            margin-bottom: 20px;
            padding: 10px 10px 10px 20px;
            background-color: #3d4952
        }

        svg .highcharts-tooltip + text {
            display: none
        }

        .toggle_menu .icon-bar {
            width: 20px;
            -webkit-transform: rotate(-30deg);
            transform: rotate(-30deg);
            -webkit-transition: .35s all;
            transition: .35s all
        }

        .toggle_menu .icon-bar:first-child {
            -webkit-transform: rotate(30deg);
            transform: rotate(30deg)
        }

        .portfolio {
            padding: 30px 0 45px 0;
        }

        .portfolio-left-column {
            margin-top: 10px
        }

        .portfolio-left-column h4 {
            font-weight: 700
        }

        .portfolio-left-column:first-child {
            margin-top: 0
        }

        .portfolio-left-column-container {
            padding: 20px 0 20px 15px
        }

        .portfolio-right-column-container {
            padding-bottom: 20px
        }

        .portfolio-breakdown-table-container {
            margin-top: 25px
        }

        .portfolio-right-column-container table {
            text-align: right
        }

        .portfolio-breakdown-table-container:first-child {
            margin-top: 0
        }

        .portfolio-right-column-container .portfolio-breakdown-table-container tr td {
            text-align: center
        }

        .portfolio-breakdown-table-container tfoot {
            border-top: 3px solid #3d4952
        }

        .portfolio-breakdown-table-container > table > caption {
            text-align: left
        }

        .portfolio-breakdown-table-container > table > caption > span {
            padding: 5px 20px;
            display: inline-block;
            color: #f5f5f5;
            font-size: 1.1em;
            background-color: #3d4952;
            text-align: left
        }

        .portfolio-breakdown-table-container table {
            font-size: 1em;
            border-top: 3px solid #3d4952
        }

        .portfolio-center-column-container {
            padding: 20px 0
        }

        .portfolio-center-column-container h3 {
            font-size: 17px;
            color: #000
        }

        .portfolio-center-column-container h4 {
            color: #008fd5;
            margin-top: 20px
        }

        .portfolio-center-column-container table {
            width: 100% !important;
            table-layout: auto;
            border: 1px solid #000
        }

        .portfolio-center-column-container td {
            text-align: center;
            padding: 5px;
            color: rgba(0, 0, 0, .7);
            border: 1px solid #000;
            background-color: rgba(0, 111, 165, .1)
        }

        .portfolio-center-column-container thead {
            text-transform: capitalize;
            background-color: rgba(9, 142, 209, .83);
            color: #f5f5f5
        }

        .portfolio-center-column-container thead td {
            border-bottom: none;
            font-size: 13px;
            font-weight: 700;
            text-transform: capitalize;
            padding: 10px 5px;
            color: #f5f5f5
        }

        .pre_header a.button, button {
            margin: 5px 0
        }

        .tablesorter .icon-sort {
            font-size: 16px;
            color: #006fa5
        }

        .container-portfolio .toogle_container {
            display: none
        }

        .table-responsive {
            border: none !important
        }

        @media screen and (max-width: 1200px) {
            .toogle_container {
                display: block
            }
        }

        @media screen and (max-width: 991px) {
            .member_home_table {
                margin-top: 25px
            }

            .pre_header > .row-inline.align-bottom > [class*=col-] {
                vertical-align: top
            }

            .portfolio-breakdown-table-container {
                margin-bottom: 35px
            }

            .portfolio-right-column-container {
                padding: 0
            }

            .portfolio {
                padding-bottom: 20px
            }
        }

        @media screen and (max-width: 767px) {
            .header {
                text-align: center
            }

            .nav_header {
                margin-bottom: 35px
            }

            .nav_header li {
                display: block;
                cursor: pointer;
                padding: 10px 0;
                border-bottom: 1px solid #0182c4;
                margin-left: 0 !important
            }

            .pre_header_right_column {
                display: block;
                margin-top: 15px
            }

            .navbar-header {
                display: block
            }
        }

        @media screen and (min-width: 768px) {
            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        @media screen and (max-width: 450px) {
            .pre_header_right_column {
                display: none;
            }

            .button-member-login-rubber {
                display: block !important;
            }

            .pre_header a[title=ATM] > img {
                max-width: 100%;
            }

            .pre_header .navbar-header, .pre_header a[title=ATM] {
                float: none;
                margin-left: 0;
            }
        }
    </style>


    <main class="container-portfolio">

        @include('layouts.member-line-top')

        <div class="portfolio">

            <div class="portfolio-perfomance-container ">

                <section class="section">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12"><h3>Performance</h3></div>

                    </div>
                </section>
                <section class="section">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 -col-sm-6">


                            <div class="portfolio-breakdown-table-container">
                                <h2 class="members_info_main_right_column_title">ATM Australia Model Portfolio
                                    Performance</h2>

                                <table class="member_home_table  perfomance-table tablesorter">
                                    <thead class="table_sort_thead">
                                    <tr>
                                        <th>Perfor&shy;mance<i class="fontello-icon icon-sort"></i></th>
                                        <th>ATM AU Port&shy;folio<i class="fontello-icon icon-sort"></i></th>
                                        <th>Bench&shy;mark ASX200<i class="fontello-icon icon-sort"></i></th>
                                        <th>Relative Perfor&shy;mance<i class="fontello-icon icon-sort"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody class="">
                                    @if($csvs['newZelandPortfolioPerformance'])
                                        @foreach($csvs['newZelandPortfolioPerformance'] as $key => $newZelandPortfolioPerformance)
                                            <?php if (!$newZelandPortfolioPerformance[0]) break  ?>
                                            <tr>
                                                <td>{{isset($newZelandPortfolioPerformance[0]) ? $newZelandPortfolioPerformance[0] : null}}</td>
                                                <td>{{isset($newZelandPortfolioPerformance[1]) ? $newZelandPortfolioPerformance[1] : null}}</td>
                                                <td>{{isset($newZelandPortfolioPerformance[2]) ? $newZelandPortfolioPerformance[2] : null}}</td>
                                                <td>{{isset($newZelandPortfolioPerformance[3]) ? $newZelandPortfolioPerformance[3] : null}}</td>
                                            </tr>

                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 -col-sm-6">
                            <div class="chart-container-auportfolio">
                                <h2 class="members_info_main_right_column_title">Chart of the Moment</h2>

                                <div class="" id="container-auportfolio"></div>
                                <div id="auPortfolioChart" style="display: none" data-csv="{{json_encode($chart)}}">
                                </div>
                            </div>
                        </div>


                    </div>
                </section>
            </div>
            <section class="section">
                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <h2 class="members_info_main_right_column_title">ATM MODEL AUSTRALIAN EQUITY PORTFOLIO</h2>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="portfolio-left-column-container">

                                    <div class="portfolio-left-column">
                                        <h4>Who should invest?</h4>

                                        <p>The fund is designed to provide a guide for our members with an ideal
                                            portfolio of Australian stocks, in which ATM sees medium term potential</p>
                                    </div>

                                    <div class="portfolio-left-column">
                                        <h4>Investments held</h4>

                                        <p>The portfolio will predominantly invest in Australian shares, but has the
                                            ability to leave money on the sidelines by holding cash. Generally the
                                            portfolio will hold between 10-15 stocks, and can invest in companies of any
                                            size across all sectors</p>
                                    </div>

                                    <div class="portfolio-left-column">
                                        <h4>Benchmark</h4>

                                        <p>ASX 200 Index</p>
                                    </div>

                                    <div class="portfolio-left-column">
                                        <h4>Date of inception</h4>

                                        <p>1 September 2015</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="portfolio-center-column-container" data-trial="{{$trial}}">
                                    @if(isset($text->body))
                                        {!!$text->body!!}
                                    @else
                                        <p>No saved text for AU Portfolio</p>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-lg-push-0 col-md-push-0 col-sm-6 col-sm-push-3">
                        <h2 class="members_info_main_right_column_title">Chart of the Moment</h2>

                        <div class="portfolio-right-column-container">
                            <div id="au-thematic-expsoure" data-csv="{{json_encode($thematicExpsoure)}}"></div>
                        </div>

                        <div class="portfolio-right-column-container">
                            <div id="au-industry-weightings" data-csv="{{json_encode($industryWeightings)}}"></div>
                        </div>
                    </div>


                </div>
            </section>

        </div>


    </main>


@endsection