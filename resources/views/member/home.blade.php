@extends('layouts.landing')
@section('title', 'Member home ')

@section('scripts')
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/modules/data.js"></script>
@endsection

@section('content')

    <!-- CRITICAL CSS -->
    <style>
        a, table {
            background-color: transparent
        }

        .sub_menu, ul {
            list-style: none
        }

        *, :after, :before {
            box-sizing: border-box;
            outline: 0
        }

        .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
            clear: both
        }

        table {
            border-collapse: collapse;
            border-spacing: 0
        }

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
        }

        [class*=" icon-"], [class^=icon-] {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            line-height: 1em
        }

        .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            text-decoration: inherit;
            font-variant: normal;
            line-height: 1em;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            text-transform: none;
            text-align: center;
            -moz-osx-font-smoothing: grayscale
        }

        .menu--link, .nav_header a, a.button, button {
            text-transform: uppercase
        }

        @font-face {
            font-family: fontello;
            src: url(../../../public/fonts/fontello.eot);
            src: url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'), url(../../../public/fonts/fontello.woff?34375466) format('woff'), url(../../../public/fonts/fontello.ttf?34375466) format('truetype'), url(../../../public/fonts/fontello.svg?34375466) format('svg');
            font-weight: 400;
            font-style: normal
        }

        .fontello-icon {
            font-size: 18px;
            color: #93a6b0
        }

        th {
            text-align: left
        }

        [class*=" icon-"]:before, [class^=icon-]:before {
            speak: none;
            width: 1em;
            margin-right: .2em;
            margin-left: .2em
        }

        .icon-facebook:before {
            content: '\e802'
        }

        .icon-twitter:before {
            content: '\e804'
        }

        .icon-google:before {
            content: '\e805'
        }

        .icon-sort:before {
            content: '\e817'
        }

        html {
            font-family: sans-serif;
            -webkit-tap-highlight-color: transparent;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        a {
            color: #008fd5;
            text-decoration: none;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        body {
            background-color: #fff
        }

        svg:not(:root) {
            overflow: hidden
        }

        button {
            font: inherit;
            overflow: visible;
            -webkit-appearance: button;
            cursor: pointer;
            font-family: inherit;
            line-height: inherit
        }

        button::-moz-focus-inner {
            border: 0;
            padding: 0
        }

        .sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            margin: -1px;
            padding: 0;
            overflow: hidden;
            clip: rect(0, 0, 0, 0);
            border: 0
        }

        .row {
            margin-left: -15px;
            margin-right: -15px
        }

        .col-lg-12, .col-lg-3, .col-lg-6, .col-md-12, .col-md-3, .col-md-6, .col-sm-12, .col-sm-3, .col-sm-6, .col-xs-12, .col-xs-3 {
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px
        }

        .col-xs-12, .col-xs-3 {
            float: left
        }

        .col-xs-12 {
            width: 100%
        }

        .col-xs-3 {
            width: 25%
        }

        @media (min-width: 768px) {
            .col-sm-12, .col-sm-3, .col-sm-6 {
                float: left
            }

            .col-sm-12 {
                width: 100%
            }

            .col-sm-6 {
                width: 50%
            }

            .col-sm-3 {
                width: 25%
            }
        }

        @media (min-width: 992px) {
            .col-md-12, .col-md-3, .col-md-6 {
                float: left
            }

            .col-md-12 {
                width: 100%
            }

            .col-md-6 {
                width: 50%
            }

            .col-md-3 {
                width: 25%
            }

            .col-md-push-3 {
                left: 25%
            }
        }

        @media (min-width: 1200px) {
            .col-lg-12, .col-lg-3, .col-lg-6 {
                float: left
            }

            .col-lg-12 {
                width: 100%
            }

            .col-lg-6 {
                width: 50%
            }

            .col-lg-3 {
                width: 25%
            }

            .col-lg-push-0 {
                left: auto
            }
        }

        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700
        }

        .collapse {
            display: none
        }

        .navbar-collapse {
            overflow-x: visible;
            padding-right: 15px;
            padding-left: 15px;
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
            -webkit-overflow-scrolling: touch
        }

        @media (min-width: 768px) {
            .navbar-header {
                float: left
            }

            .navbar-collapse {
                width: auto;
                border-top: 0;
                box-shadow: none
            }

            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
            content: " ";
            display: table
        }

        @-ms-viewport {
            width: device-width
        }

        .row-inline {
            letter-spacing: -4px;
            font-size: 0
        }

        .row-inline > [class*=col-] {
            float: none;
            display: inline-block;
            letter-spacing: 0;
            font-size: 14px;
            vertical-align: top
        }

        .pre_header .navbar-header, .pre_header a[title=ATM] {
            float: left
        }

        .row-inline.align-bottom > [class*=col-] {
            vertical-align: bottom
        }

        .navbar-header {
            margin-left: 25px
        }

        a, body, div, h2, header, html, i, iframe, img, label, li, nav, section, span, table, tbody, td, th, thead, tr, ul {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            vertical-align: baseline
        }

        img {
            vertical-align: middle;
            border: 0
        }

        header, nav, section {
            display: block
        }

        body, html {
            height: 100%;
            min-width: 320px;
            color: #73848e
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
            line-height: 22px;
            font-weight: 400;
            color: #73848e
        }

        h2 {
            color: #23323a
        }

        .clearfix:after, .clearfix:before {
            display: table;
            content: " "
        }

        .section {
            max-width: 1400px;
            margin: 0 auto;
            padding: 0 15px
        }

        .back-to-top {
            display: none
        }

        .back-to-top a {
            position: fixed;
            width: 50px;
            height: 50px;
            bottom: 20px;
            right: 20px;
            opacity: .8;
            z-index: 1000;
            background: url(img/back-to-top.png) center center no-repeat #0996d2;
            cursor: pointer;
            -webkit-transition: opacity .3s;
            transition: opacity .3s
        }

        .menu--label, .menu--link, .menu--subitens__opened .sub_menu {
            display: block
        }

        .vertical_nav {
            -webkit-transition: left .3s ease-out;
            transition: left .3s ease-out;
            z-index: 100;
            left: -470px;
            top: 0;
            bottom: 0
        }

        :active, :focus {
            outline: 0
        }

        .sub_menu {
            background-color: #fff
        }

        .sub_menu li {
            margin-top: 2px;
            background-color: #0191D3
        }

        .menu-vertical {
            top: 0;
            bottom: 40px;
            overflow-y: auto;
            height: 100%;
            width: 100%;
            min-width: 285px;
            margin: 0;
            padding: 0;
            list-style-type: none;
            background: rgba(255, 255, 255, 1)
        }

        .menu--item {
            position: relative;
            min-height: 40px;
            line-height: 40px
        }

        .menu--item__has_sub_menu .menu--link:after {
            position: absolute;
            top: 0;
            font-size: 21px;
            color: #fff;
            right: 15px;
            height: 40px;
            line-height: 40px;
            font-family: fontello;
            content: "\e815";
            -webkit-transition: color .4s ease;
            transition: color .4s ease
        }

        .menu--link, .sub_menu--link {
            font-size: .875rem;
            text-decoration: none
        }

        .menu--subitens__opened .menu--link:after {
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        .menu--subitens__opened {
            background: #e9e9e9
        }

        .menu--subitens__opened .menu--link {
            color: #000
        }

        .menu--link {
            overflow: hidden;
            color: #666;
            font-weight: 400;
            white-space: nowrap;
            cursor: pointer
        }

        .sub_menu {
            overflow: hidden;
            padding-left: 0;
            display: none
        }

        .sub_menu--link {
            display: block;
            padding-left: 50px;
            padding-right: 16px;
            color: #f5f5f5
        }

        @media (min-width: 1200px) {
            .collapse_menu {
                height: 40px;
                line-height: 40px;
                display: block;
                position: absolute;
                bottom: 0;
                width: 100%;
                padding: 0;
                border: 0;
                border-top: 1px solid #e6e6e6;
                background: #f6f6f6;
                color: #666;
                font-size: .875rem;
                text-align: left;
                cursor: pointer
            }

            .vertical_nav {
                top: 10px;
                left: 0
            }
        }

        .lower_header, .pre_header {
            padding: 15px 0
        }

        .pre_header_right_column {
            text-align: right
        }

        .pre_header_right_column > a {
            display: inline-block;
            vertical-align: middle
        }

        .navbar-header {
            display: none
        }

        .pre_header_right_column a i {
            font-size: 1.4em;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        .lower_header {
            z-index: 100;
            position: relative;
            background-color: rgba(255, 255, 255, .67);
            border-bottom: 1px solid rgba(245, 245, 245, .5);
            -webkit-transition: background-color ease-in-out .15s;
            transition: background-color ease-in-out .15s
        }

        .nav_header li {
            display: inline-block;
            margin-left: 30px;
            text-align: left;
            -webkit-transition: background-color, padding ease-in-out .15s;
            transition: background-color, padding ease-in-out .15s
        }

        .nav_header a {
            color: #23323a
        }

        .nav_header li:first-child {
            margin-left: 0
        }

        .wrapper {
            -webkit-transition: .3s filter linear;
            transition: .3s filter linear
        }

        a.button, button {
            background-color: rgba(0, 143, 213, .73);
            color: #fff;
            font-size: 1em;
            padding: 10px 20px;
            display: inline-block;
            border: none;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        a.button-member-login {
            margin-left: 10px
        }

        .button-member-login-rubber {
            display: none !important
        }

        .vertical_nav {
            margin-bottom: 15px;
            width: 100%;
            background: #fff
        }

        .menu--item {
            background: #0191D3;
            margin-top: 5px
        }

        .menu--label {
            height: 40px;
            line-height: 40px;
            color: #f5f5f5;
            padding: 0 20px;
            font-weight: 700;
            -webkit-transition: all .4s ease;
            transition: all .4s ease
        }

        .menu--subitens__opened .menu--label {
            padding-left: 25px;
            padding-top: 1px
        }

        .collapse_menu {
            display: none
        }

        .menu--link {
            position: relative;
            -webkit-transition: all .4s ease;
            transition: all .4s ease
        }

        .member_home_table, .member_home_table thead th {
            border-bottom: 3px solid #3d4952;
            text-align: center
        }

        .member_home_table {
            table-layout: fixed;
            width: 100%;
            font-size: 1.15em
        }

        .member_home_table thead th {
            text-transform: capitalize;
            color: #006fa5;
            cursor: pointer;
            font-weight: 100;
            padding: 7px 5px
        }

        .member_home_table td {
            padding: 7px 5px;
            color: #000
        }

        .member_home_table .positive-value {
            color: #00a300
        }

        .members_info_container {
            position: relative;
            padding-top: 30px;
            padding-bottom: 45px;
            -webkit-transition: all .3s ease-in-out;
            transition: all .3s ease-in-out
        }

        .container-right-column {
            margin-top: 35px
        }

        .members_info_container .container-right-column:first-child {
            margin-top: 0
        }

        .members_info_main_right_column_title {
            color: #fff;
            text-transform: uppercase;
            font-size: 1em;
            margin-bottom: 20px;
            padding: 10px 10px 10px 20px;
            background-color: #3d4952
        }

        .top_perfoming_title {
            padding: 15px;
            color: #fff;
            font-size: 1.15em;
            margin-top: 5px;
            background: #0191D3;
            -webkit-transition: all .3s ease-in-out;
            transition: all .3s ease-in-out
        }

        .top_perfoming_name {
            float: left;
            text-transform: capitalize
        }

        .top_perfoming_percent {
            float: right
        }

        .top-perfoming-container img {
            width: 100%;
            height: auto;
            margin-top: 3px
        }

        .top-perfoming-container a {
            display: block;
            margin-top: 20px
        }

        .top-perfoming-container a:first-child {
            margin-top: 0
        }

        .chart-container {
            margin: 35px 0
        }

        .member_home_page_chart_one {
            width: 100%;
            height: 350px
        }

        svg .highcharts-tooltip + text {
            display: none
        }

        .menu-vertical li:first-child {
            margin-top: 7px
        }

        .menu-vertical > li:first-child {
            margin-top: 0
        }

        .highcharts-title {
            font-size: 1.4em !important
        }

        .pre_header a.button, button {
            margin: 5px 0
        }

        .tablesorter .icon-sort {
            font-size: 16px;
            color: #006fa5
        }

        @media screen and (max-width: 1200px) {
            .menu--item {
                margin-left: 10px;
                margin-right: 10px
            }

            .box-view {
                height: 550px !important
            }

            .members_info_container {
                padding-top: 0
            }

            .menu-vertical, .vertical_nav {
                position: absolute;
                height: 725px;
                min-width: 330px;
                padding-top: 10px;
                padding-bottom: 10px;
                margin-top: 3px
            }
        }

        @media screen and (max-width: 991px) {
            .member_home_table {
                margin-top: 25px
            }

            .pre_header > .row-inline.align-bottom > [class*=col-] {
                vertical-align: top
            }
        }

        @media screen and (max-width: 767px) {
            .header {
                text-align: center
            }

            .nav_header {
                margin-bottom: 35px
            }

            .nav_header li {
                display: block;
                cursor: pointer;
                padding: 10px 0;
                border-bottom: 1px solid #0182c4;
                margin-left: 0 !important
            }

            .pre_header_right_column {
                display: block;
                margin-top: 15px
            }

            .box-view {
                height: 350px !important
            }

            .navbar-header {
                display: block
            }
        }

        @media screen and (min-width: 768px) {
            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        @media screen and (max-width: 450px) {
            .pre_header_right_column {
                display: none
            }

            .button-member-login-rubber {
                display: block !important
            }

            .menu-vertical, .vertical_nav {
                width: 100%;
                margin-top: 1px;
                min-width: 300px
            }

            .members_info_container > .row:first-child > .col-xs-3:first-child {
                width: 100%
            }

            .pre_header a[title=ATM] > img {
                max-width: 100%
            }

            .pre_header .navbar-header, .pre_header a[title=ATM] {
                float: none;
                margin-left: 0
            }

            .box-view {
                height: 300px !important
            }
        }
        .video-container {
            position:relative;
            padding-bottom:56.25%;
            padding-top:5px;
            height:0;
            overflow:hidden;
            margin-top: 30px;
        }

        .video-container iframe, .video-container object, .video-container embed {
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
        }
    </style>

    @include('layouts.member-line-top')


    <div class="members_info_container ">
        <div class="row section ">

            @include('layouts.member-left-menu')

            <div class="col-lg-6 col-md-12 col-sm-12">
                @if($document)
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pros-atm container_form_login_img">
                            <a href="/member/AUPortfolio">
                                <h2 class="members_info_main_right_column_title">ATM AUSTRALIAN PORTFOLIO</h2>
                                <img class="img-responsive" src="/img/australia.jpg" alt="Australian Equity Portfolio"></a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pros-atm container_form_login_img">
                            <h2 class="members_info_main_right_column_title">ATM NEW ZEALAND PORTFOLIO</h2>
                            <a href="/member/NZPortfolio"><img class="img-responsive" src="/img/new_zealand.jpg" alt="New Zealand Equity Portfolio"></a>
                        </div>
                    </div>
                    <iframe class="box-view"
                            src="{!!$document->viewUrl!!}"
                            width="100%" height="800px" frameborder="0" allowfullscreen webkitallowfullscreen
                            msallowfullscreen></iframe>
                    <div class="box-view-container"></div>
                @else
                    <h2>Document not uploaded</h2>
                @endif
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/ts5uwWMfNn8"
                            frameborder="0" allowfullscreen webkitallowfullscreen
                            msallowfullscreen></iframe>
                </div>
                <div class="chart-container" style="display: none;;">
                    <h2 class="members_info_main_right_column_title">Chart of the Moment</h2>

                    <div class="member_home_page_chart_one" id="container_gtd_prices_recovering"></div>

                    <div id="homePageChart" style="display: none" data-csv="{{json_encode($chart)}}">
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-lg-push-0 col-md-push-3 col-md-6 col-sm-6 col-xs-12 col-sm-push-3">

                @include('partials.topTradesBlock')

                <div class="container-right-column">
                    <h2 class="members_info_main_right_column_title">RECENT PICKS</h2>

                    <div class="top-perfoming-container">
                        @if($ytdNotes)
                            @foreach($ytdNotes as $note)
                                <a title="{{$note->title}}" href="{{$note->stockLink}}">
                                    <div class="top_perfoming_title clearfix">
                                        <span class="top_perfoming_name">{{$note->title}}</span>
                                        <span class="top_perfoming_percent">+{{$note->ytd}}% Last 12 Months</span>
                                    </div>
                                    <div class="project-image wow">
                                        <img src="/uploads-min{{Croppa::url($note->image, 260, 260, ['resize'])}}" alt="{{$note->title}}"/>
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>


    </div>



@endsection