@extends('layouts.landing')
@section('title', 'become-member ')
@section('content')
    <div class="become-member-container">
        <div class="products-container">
            <div class="section">
                <div class="row products-row " style="margin-bottom: 0px;">
                    <div class="products-column" style="font-size: 1.8em;">
                        <h3>{!! $message !!}</h3>
                        <p class="account-locked-description" style="font-size: 20px;">
                            If you have any questions you are welcome to call <a title="+64278783600" href="tel:+64 27 878 3600">+64 27 878 3600</a> (New Zealand) and <a title="+1800026778" href="tel:+1800-026-778">1800-026-778</a> (Toll Free Australia) or send an email to <a class="" target="_top" title="Write email to ATM" href="mailto:info@atmstrategy.com.au">info@atmstrategy.com.au</a>                    </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="payment-plans-container">
            <div class="section">
                <div class="row ">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="row option-container">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="option-six-monthly option-block-container">
                                    <div class="option-block">
                                        <h2>6 months subscription</h2>

                                        <div class="payment-optional">
                                            <h3><strong>A$100</strong> / month</h3>

                                            <p class="optional-time"><br><br>
                                                *equates to $600 over 6 months period</p>

                                            <div><a href="">
                                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                        <input type="hidden" name="cmd" value="_s-xclick">
                                                        <input type="hidden" name="hosted_button_id" value="RHCMNYGSULED6">
                                                        <input class="button" type="submit" name="submit" value="Join Now"
                                                               alt="PayPal - The safer, easier way to pay online!" onClick="ga('send', 'event', 'join_six_month', 'Join');">
                                                    </form>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="option-twelve-monthly option-block-container">
                                    <div class="option-block">
                                        <h2>12 months subscription</h2>

                                        <div class="payment-optional">
                                            <h3><strong>A$83</strong> / month</h3>

                                            <p class="optional-time">Save 20% p.a<br><br>
                                                *equates to $995 over 12 months period</p>

                                            <div>
                                                <a href="" target="_blank">
                                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                        <input type="hidden" name="cmd" value="_s-xclick">
                                                        <input type="hidden" name="hosted_button_id" value="Y6T7VA5LH3GWE">
                                                        <input class="button" type="submit" name="submit" value="Join Now"
                                                               alt="PayPal - The safer, easier way to pay online!" onClick="ga('send', 'event', 'join_year', 'Join');">
                                                    </form>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @include('partials.becomeMember')
    </div>
@endsection