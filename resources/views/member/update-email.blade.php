@extends('layouts.landing')
@section('title', 'Update email ')
@section('content')

        <!-- CRITICAL CSS -->
<style>
    .pre_header_right_column a i, a {
        -webkit-transition: color ease-in-out .15s
    }

    .sub_menu, ul {
        list-style: none
    }

    *, :after, :before {
        box-sizing: border-box;
        outline: 0
    }

    .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
        clear: both
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
    }

    [class*=" icon-"], [class^=icon-] {
        font-family: fontello;
        font-style: normal;
        font-weight: 400;
        line-height: 1em
    }

    .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
        font-family: fontello;
        font-style: normal;
        font-weight: 400;
        text-decoration: inherit;
        font-variant: normal;
        line-height: 1em;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        text-transform: none;
        text-align: center;
        -moz-osx-font-smoothing: grayscale
    }

    @font-face {
        font-family: fontello;
        src: url(../../../public/fonts/fontello.eot);
        src: url(../../../public/fonts/fontello.eot?34375466) format('embedded-opentype'), url(../../../public/fonts/fontello.woff?34375466) format('woff'), url(../../../public/fonts/fontello.ttf?34375466) format('truetype'), url(../../../public/fonts/fontello.svg?34375466) format('svg');
        font-weight: 400;
        font-style: normal
    }

    .fontello-icon {
        font-size: 18px;
        color: #93a6b0
    }

    [class*=" icon-"]:before, [class^=icon-]:before {
        speak: none;
        width: 1em;
        margin-right: .2em;
        margin-left: .2em
    }

    .icon-facebook:before {
        content: '\e802'
    }

    .icon-twitter:before {
        content: '\e804'
    }

    .icon-google:before {
        content: '\e805'
    }

    html {
        font-family: sans-serif;
        -webkit-tap-highlight-color: transparent;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%
    }

    a {
        background-color: transparent;
        color: #008fd5;
        text-decoration: none;
        transition: color ease-in-out .15s
    }

    body {
        background-color: #fff
    }

    button, input {
        color: inherit;
        font: inherit;
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit
    }

    button {
        overflow: visible
    }

    button, input[type=submit] {
        -webkit-appearance: button;
        cursor: pointer
    }

    button::-moz-focus-inner, input::-moz-focus-inner {
        border: 0;
        padding: 0
    }

    .sr-only {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0
    }

    .row {
        margin-left: -15px;
        margin-right: -15px
    }

    .col-lg-10, .col-lg-12, .col-lg-3, .col-lg-4, .col-lg-6, .col-lg-8, .col-lg-9, .col-md-10, .col-md-12, .col-md-3, .col-md-4, .col-md-6, .col-md-8, .col-md-9, .col-sm-10, .col-sm-12, .col-sm-3, .col-sm-4, .col-sm-6, .col-sm-8, .col-sm-9, .col-xs-12, .col-xs-3, .col-xs-6 {
        position: relative;
        min-height: 1px;
        padding-left: 15px;
        padding-right: 15px
    }

    .col-xs-12, .col-xs-3, .col-xs-6 {
        float: left
    }

    .col-xs-12 {
        width: 100%
    }

    .col-xs-6 {
        width: 50%
    }

    .col-xs-3 {
        width: 25%
    }

    @media (min-width: 768px) {
        .col-sm-10, .col-sm-12, .col-sm-3, .col-sm-4, .col-sm-6, .col-sm-8, .col-sm-9 {
            float: left
        }

        .col-sm-12 {
            width: 100%
        }

        .col-sm-10 {
            width: 83.33333333%
        }

        .col-sm-9 {
            width: 75%
        }

        .col-sm-8 {
            width: 66.66666667%
        }

        .col-sm-6 {
            width: 50%
        }

        .col-sm-4 {
            width: 33.33333333%
        }

        .col-sm-3 {
            width: 25%
        }

        .col-sm-push-1 {
            left: 8.33333333%
        }

        .col-sm-offset-3 {
            margin-left: 25%
        }
    }

    @media (min-width: 992px) {
        .col-md-10, .col-md-12, .col-md-3, .col-md-4, .col-md-6, .col-md-8, .col-md-9 {
            float: left
        }

        .col-md-12 {
            width: 100%
        }

        .col-md-10 {
            width: 83.33333333%
        }

        .col-md-9 {
            width: 75%
        }

        .col-md-8 {
            width: 66.66666667%
        }

        .col-md-6 {
            width: 50%
        }

        .col-md-4 {
            width: 33.33333333%
        }

        .col-md-3 {
            width: 25%
        }

        .col-md-push-1 {
            left: 8.33333333%
        }

        .col-md-offset-3 {
            margin-left: 25%
        }
    }

    @media (min-width: 1200px) {
        .col-lg-10, .col-lg-12, .col-lg-3, .col-lg-4, .col-lg-6, .col-lg-8, .col-lg-9 {
            float: left
        }

        .col-lg-12 {
            width: 100%
        }

        .col-lg-10 {
            width: 83.33333333%
        }

        .col-lg-9 {
            width: 75%
        }

        .col-lg-8 {
            width: 66.66666667%
        }

        .col-lg-6 {
            width: 50%
        }

        .col-lg-4 {
            width: 33.33333333%
        }

        .col-lg-3 {
            width: 25%
        }

        .col-lg-push-1 {
            left: 8.33333333%
        }

        .col-lg-offset-3 {
            margin-left: 25%
        }
    }

    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700
    }

    .collapse {
        display: none
    }

    .navbar-collapse {
        overflow-x: visible;
        padding-right: 15px;
        padding-left: 15px;
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
        -webkit-overflow-scrolling: touch
    }

    @media (min-width: 768px) {
        .navbar-header {
            float: left
        }

        .navbar-collapse {
            width: auto;
            border-top: 0;
            box-shadow: none
        }

        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important
        }
    }

    .navbar-toggle {
        position: relative;
        float: right;
        margin-right: 15px;
        margin-top: 8px;
        margin-bottom: 8px;
        background-color: transparent;
        background-image: none;
        border-radius: 4px
    }

    .navbar-toggle .icon-bar {
        display: block;
        height: 2px;
        border-radius: 1px
    }

    .navbar-toggle .icon-bar + .icon-bar {
        margin-top: 4px
    }

    @media (min-width: 768px) {
        .navbar-toggle {
            display: none
        }
    }

    .menu--link, .nav_header a {
        text-transform: uppercase
    }

    .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
        content: " ";
        display: table
    }

    .navbar-toggle .icon-bar {
        background-color: #008fd5;
        width: 35px;
        margin: 7px 0
    }

    @-ms-viewport {
        width: device-width
    }

    .row-inline {
        letter-spacing: -4px;
        font-size: 0
    }

    .row-inline > [class*=col-] {
        float: none;
        display: inline-block;
        letter-spacing: 0;
        font-size: 14px;
        vertical-align: top
    }

    .pre_header .navbar-header, .pre_header a[title=ATM], .toggle_menu {
        float: left
    }

    .row-inline.align-middle > [class*=col-] {
        vertical-align: middle
    }

    .row-inline.align-bottom > [class*=col-] {
        vertical-align: bottom
    }

    .navbar-toggle {
        border: 1px solid #008fd5;
        padding: 5px 10px
    }

    .navbar-header {
        margin-left: 25px
    }

    .align-center {
        text-align: center
    }

    .align-right {
        text-align: right
    }

    a, body, div, footer, form, h2, h4, header, html, i, img, label, li, nav, p, section, span, ul {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        vertical-align: baseline
    }

    img {
        vertical-align: middle;
        border: 0
    }

    footer, header, main, nav, section {
        display: block
    }

    body, html {
        height: 100%;
        min-width: 320px;
        color: #73848e
    }

    body {
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        line-height: 22px;
        font-weight: 400;
        color: #73848e
    }

    h2, h4 {
        color: #23323a
    }

    .clearfix:after, .clearfix:before {
        display: table;
        content: " "
    }

    .section {
        max-width: 1400px;
        margin: 0 auto;
        padding: 0 15px
    }

    .back-to-top {
        display: none
    }

    .menu--label, .menu--link, .menu--subitens__opened .sub_menu, .toggle_menu {
        display: block
    }

    .vertical_nav {
        -webkit-transition: left .3s ease-out;
        transition: left .3s ease-out;
        z-index: 100;
        left: -470px;
        top: 0;
        bottom: 0
    }

    :active, :focus {
        outline: 0
    }

    .sub_menu {
        background-color: #fff
    }

    .sub_menu li {
        margin-top: 2px;
        background-color: #0191D3
    }

    .menu-vertical {
        top: 0;
        bottom: 40px;
        overflow-y: auto;
        height: 100%;
        width: 100%;
        min-width: 285px;
        margin: 0;
        padding: 0;
        list-style-type: none;
        background: rgba(255, 255, 255, 1)
    }

    .menu--label {
        height: 40px;
        line-height: 40px
    }

    .menu--item {
        position: relative;
        min-height: 40px;
        line-height: 40px
    }

    .menu--item__has_sub_menu .menu--link:after {
        position: absolute;
        top: 0;
        font-size: 21px;
        color: #fff;
        right: 15px;
        height: 40px;
        line-height: 40px;
        font-family: fontello;
        content: "\e815";
        -webkit-transition: color .4s ease;
        transition: color .4s ease
    }

    .menu--link, .sub_menu--link {
        font-size: .875rem;
        text-decoration: none
    }

    .menu--subitens__close .sub_menu {
        -webkit-transition: color .4s ease;
        transition: color .4s ease
    }

    .menu--subitens__opened .menu--link:after {
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg)
    }

    .menu--subitens__opened {
        background: #e9e9e9
    }

    .menu--subitens__opened .menu--link {
        color: #000
    }

    .menu--link {
        overflow: hidden;
        color: #666;
        font-weight: 400;
        white-space: nowrap;
        cursor: pointer
    }

    .sub_menu {
        overflow: hidden;
        padding-left: 0;
        display: none
    }

    .sub_menu--link {
        display: block;
        padding-left: 50px;
        padding-right: 16px;
        color: #f5f5f5
    }

    @media (min-width: 1200px) {
        .collapse_menu {
            height: 40px;
            line-height: 40px;
            display: block;
            position: absolute;
            bottom: 0;
            width: 100%;
            padding: 0;
            border: 0;
            border-top: 1px solid #e6e6e6;
            background: #f6f6f6;
            color: #666;
            font-size: .875rem;
            text-align: left;
            cursor: pointer
        }

        .vertical_nav {
            top: 10px;
            left: 0
        }

        .toggle_menu {
            display: none
        }
    }

    .button-form, .form_group, .lower_header, .menu--link, .toogle_container {
        position: relative
    }

    .lower_header, .pre_header {
        padding: 15px 0
    }

    .pre_header_right_column {
        text-align: right
    }

    .pre_header_right_column > a {
        display: inline-block;
        vertical-align: middle
    }

    .navbar-header {
        display: none
    }

    .pre_header_right_column a i {
        font-size: 1.4em;
        transition: color ease-in-out .15s
    }

    .lower_header {
        z-index: 100;
        background-color: rgba(255, 255, 255, .67);
        border-bottom: 1px solid rgba(245, 245, 245, .5);
        -webkit-transition: background-color ease-in-out .15s;
        transition: background-color ease-in-out .15s
    }

    .nav_header li {
        display: inline-block;
        margin-left: 30px;
        text-align: left;
        -webkit-transition: background-color, padding ease-in-out .15s;
        transition: background-color, padding ease-in-out .15s
    }

    .nav_header a {
        color: #23323a
    }

    .nav_header li:first-child {
        margin-left: 0
    }

    .toogle_container {
        height: 50px;
        display: none
    }

    .button-form, a.button, button {
        display: inline-block;
        font-size: 1em;
        text-transform: uppercase
    }

    .wrapper {
        -webkit-transition: .3s filter linear;
        transition: .3s filter linear
    }

    a.button, button {
        background-color: rgba(0, 143, 213, .73);
        color: #fff;
        padding: 10px 20px;
        border: none;
        -webkit-transition: all .3s;
        transition: all .3s
    }

    .form_group input {
        background: rgba(255, 255, 255, .6);
        color: #19242a;
        width: 100%;
        border: 1px solid transparent;
        padding: 10px 15px;
        -webkit-transition: background-color .3s;
        transition: background-color .3s
    }

    .form_group {
        margin-bottom: 20px
    }

    .form_group input::-webkit-input-placeholder {
        color: #19242a
    }

    .button-form {
        text-align: center;
        background: rgba(255, 255, 255, .15);
        border: 1px solid #707e89;
        color: #fff;
        padding: 12px 25px;
        -webkit-transition: background .3s;
        transition: background .3s
    }

    a.button-member-login {
        margin-left: 10px
    }

    .button-member-login-rubber {
        display: none !important
    }

    .vertical_nav {
        margin-bottom: 15px;
        width: 100%;
        background: #fff
    }

    .menu--item {
        background: #0191D3;
        margin-top: 5px
    }

    .menu--label {
        color: #f5f5f5;
        padding: 0 20px;
        font-weight: 700;
        -webkit-transition: all .4s ease;
        transition: all .4s ease
    }

    .menu--subitens__opened .menu--label {
        padding-left: 25px;
        padding-top: 1px
    }

    .collapse_menu {
        display: none
    }

    .menu--link {
        -webkit-transition: all .4s ease;
        transition: all .4s ease
    }

    .members_info {
        background: rgba(9, 142, 209, .83);
        padding-top: 10px;
        padding-bottom: 10px;
        color: #fff;
        font-size: 1.15em;
        font-weight: 100
    }

    .members_info_main_right_column_title {
        color: #fff;
        text-transform: uppercase;
        font-size: 1em;
        margin-bottom: 20px;
        padding: 10px 10px 10px 20px;
        background-color: #3d4952
    }

    .toggle_menu .icon-bar {
        width: 20px;
        -webkit-transform: rotate(-30deg);
        transform: rotate(-30deg);
        -webkit-transition: .35s all;
        transition: .35s all
    }

    .toggle_menu .icon-bar:first-child {
        -webkit-transform: rotate(30deg);
        transform: rotate(30deg)
    }

    .menu-vertical li:first-child {
        margin-top: 7px
    }

    .menu-vertical > li:first-child {
        margin-top: 0
    }

    .profile-text p {
        margin: 20px 0;
        text-align: justify
    }

    .profile-form {
        padding-top: 20px
    }

    .profile-form .form_group input {
        color: #73848e;
        border: 1px solid #e1e1e1;
        -webkit-transition: all .15s;
        transition: all .15s
    }

    .profile-form-submit {
        background: #269BD6
    }

    .profile-form .form_group label {
        line-height: 3.4em
    }

    .profile-update-email-container {
        padding: 30px 0 45px
    }

    .current-email-text {
        line-height: 100% !important
    }

    .current-email-address {
        font-size: 1.1em
    }

    .profile-update-email-container p {
        padding-left: 20px
    }

    .profile-update-email-container .profile-form {
        padding-bottom: 20px
    }

    .profile-update-email-container .members_info_main_right_column_title {
        margin-bottom: 0
    }

    .pre_header a.button, button {
        margin: 5px 0
    }

    .footer {
        display: block
    }

    .upper-footer {
        background-color: #098ed1
    }

    .container-twetter {
        float: left;
        padding: 25px;
        background-color: #1f9cda;
        position: relative
    }

    .container-twetter .icon-twitter {
        font-size: 70px;
        color: #f5f5f5
    }

    .container-twetter:before {
        content: '';
        display: block;
        position: absolute;
        right: -25px;
        top: 25px;
        width: 10px;
        height: 10px;
        border-left: 25px solid #1f9cda;
        border-top: 0 solid transparent;
        border-bottom: 25px solid transparent
    }

    .main-footer {
        background: #19242a;
        color: #7ca1b3;
        padding: 30px 0
    }

    .main-footer p {
        margin: 10px 0;
        font-size: .9em
    }

    .main-footer h4 {
        margin-bottom: 30px;
        color: #7ca1b3;
        font-size: 1.3em;
        line-height: 28px
    }

    .widget-text-one > img {
        max-width: 245px
    }

    .widget-text-one p {
        padding-left: 50px
    }

    .footer_social_media a, .widget-text-four li {
        display: inline-block
    }

    .footer_social_media i {
        font-size: 1.3em;
        color: #93a6b0
    }

    .widget-text-four li {
        font-size: .9em;
        text-align: left;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        vertical-align: top;
        margin-left: 20px
    }

    .widget-text-four li:first-child {
        margin-left: 0
    }

    .footer-email {
        display: block
    }

    @media screen and (max-width: 1200px) {
        .menu--item {
            margin-left: 10px;
            margin-right: 10px
        }

        .toogle_container {
            display: block
        }

        .menu-vertical, .vertical_nav {
            position: absolute;
            height: 725px;
            min-width: 330px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 3px
        }

        .profile-update-email-container .menu-vertical, .profile-update-email-container .vertical_nav {
            height: 290px
        }

        .profile-update-email-container .profile-update-email {
            width: 100%;
            margin-top: 20px
        }

        .profile-update-email-container {
            padding-top: 0
        }
    }

    @media screen and (max-width: 991px) {
        .pre_header > .row-inline.align-bottom > [class*=col-] {
            vertical-align: top
        }

        .main-footer .align-center {
            text-align: left
        }
    }

    @media screen and (max-width: 767px) {
        .header {
            text-align: center
        }

        .nav_header {
            margin-bottom: 35px
        }

        .nav_header li {
            display: block;
            cursor: pointer;
            padding: 10px 0;
            border-bottom: 1px solid #0182c4;
            margin-left: 0 !important
        }

        .pre_header_right_column {
            display: block;
            margin-top: 15px
        }

        .main-footer h4 {
            margin-bottom: 15px
        }

        .main-footer [class*=col-] {
            margin-top: 45px
        }

        .main-footer [class*=col-]:first-child {
            margin-top: 0
        }

        .navbar-header {
            display: block
        }

        .profile-form form .form_group .new-email {
            text-align: left
        }
    }

    @media screen and (min-width: 768px) {
        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important
        }
    }

    @media screen and (max-width: 450px) {
        .pre_header_right_column {
            display: none
        }

        .button-member-login-rubber {
            display: block !important
        }

        .menu-vertical, .vertical_nav {
            width: 100%;
            margin-top: 1px;
            min-width: 300px
        }

        .pre_header a[title=ATM] > img {
            max-width: 100%
        }

        .pre_header .navbar-header, .pre_header a[title=ATM] {
            float: none;
            margin-left: 0
        }

        .navbar-toggle {
            float: none;
            margin-right: 0
        }
    }
</style>

@include('layouts.member-line-top')

<main class="profile-update-email-container menu--subitens__close">
    <div class="section">
        <div class="row clearfix">

            @include('layouts.member-left-menu')

            <div class="col-lg-9 col-md-9 col-sm-9 profile-update-email">
                <h2 class="members_info_main_right_column_title">Update My Email Address</h2>

                <div class="row profile-text">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <p>To receive receive our daily and weekly email reminders, please ensure your correct email
                            adress is below.</p>

                        <p>To update your email adress, simply enter your new address in the box below and click
                            Update.</p>
                    </div>
                </div>
                @if(isset($success) && $success == 'true')
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <p style="text-align: center"><a href="#"><a href="#">EMAIL ADDRESS UPDATED SUCCESSFULLY</a></p>
                    </div>
                @endif
                <div class="row profile-form">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-push-1 col-md-push-1 col-sm-push-1">
                        <form method="POST" action="/member/update-email" id="profile-form-update">
                            {!! csrf_field() !!}
                            <div class="form_group row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 align-right">
                                    <label class="current-email-text">Current Email Address:</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 align-left">
                                    <a href="#">{{me()->email}}</a>
                                </div>
                            </div>
                            <div class="form_group row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 align-right new-email">
                                    <label for="newEmail">New Email Address:</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 align-left">
                                    <input type="text" name="newEmail" id="newEmail"
                                           placeholder="" value="" data-validation="notempty;isemail">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 align-center col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                                    <input class="senddat_ button-form profile-form-submit" title="Update email"
                                           type="submit"
                                           value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--<p>Once your new email address is submitted, a verification email will be sent your new email address.
                    Please follow the instruction in the verification email and activate your new email address.</p> -->

            </div>
        </div>
    </div>


</main>


@endsection