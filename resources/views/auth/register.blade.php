@extends('layouts.landing')
@section('title', 'Registration ')
@section('modal')
    @include('index._modal')
@endsection
@section('content')

        <!-- CRITICAL CSS -->
        <style>
            .navbar-toggle,a{background-color:transparent}*,:after,:before{box-sizing:border-box;outline:0}.clearfix:after,.navbar-collapse:after,.navbar-header:after,.row:after{clear:both}ul{list-style:none}@font-face{font-family:'Open Sans';font-style:normal;font-weight:400;src:local('Open Sans'),local('OpenSans'),url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')}[class*=" icon-"],[class^=icon-]{font-family:fontello;font-style:normal;font-weight:400;line-height:1em}.fontello-icon,[class*=" icon-"]:before,[class^=icon-]:before{font-family:fontello;font-style:normal;font-weight:400;text-decoration:inherit;font-variant:normal;line-height:1em;-webkit-font-smoothing:antialiased;display:inline-block;text-transform:none;text-align:center;-moz-osx-font-smoothing:grayscale}.button-form,.container_form_register_login>.row>div h3,.nav_header a,a.button,button{text-transform:uppercase}@font-face{font-family:fontello;src:url(../fonts/fontello.eot);src:url(../fonts/fontello.eot?34375466) format('embedded-opentype'),url(../fonts/fontello.woff?34375466) format('woff'),url(../fonts/fontello.ttf?34375466) format('truetype'),url(../fonts/fontello.svg?34375466) format('svg');font-weight:400;font-style:normal}.fontello-icon{font-size:18px;color:#93a6b0}[class*=" icon-"]:before,[class^=icon-]:before{speak:none;width:1em;margin-right:.2em;margin-left:.2em}.icon-facebook:before{content:'\e802'}.icon-twitter:before{content:'\e804'}.icon-google:before{content:'\e805'}html{font-family:sans-serif;-webkit-tap-highlight-color:transparent;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}a{color:#008fd5;text-decoration:none;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}body{background-color:#fff}textarea{overflow:auto}button,input,textarea{color:inherit;font:inherit;margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button{overflow:visible}button,input[type=submit]{-webkit-appearance:button;cursor:pointer}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);border:0}.row{margin-left:-15px;margin-right:-15px}.col-lg-12,.col-lg-6,.col-md-12,.col-md-6,.col-sm-12,.col-sm-6{position:relative;min-height:1px;padding-left:15px;padding-right:15px}@media (min-width:768px){.col-sm-12,.col-sm-6{float:left}.col-sm-12{width:100%}.col-sm-6{width:50%}}@media (min-width:992px){.col-md-12,.col-md-6{float:left}.col-md-12{width:100%}.col-md-6{width:50%}}@media (min-width:1200px){.col-lg-12,.col-lg-6{float:left}.col-lg-12{width:100%}.col-lg-6{width:50%}}.collapse{display:none}.navbar-collapse{overflow-x:visible;padding-right:15px;padding-left:15px;border-top:1px solid transparent;box-shadow:inset 0 1px 0 rgba(255,255,255,.1);-webkit-overflow-scrolling:touch}@media (min-width:768px){.navbar-header{float:left}.navbar-collapse{width:auto;border-top:0;box-shadow:none}.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}.navbar-toggle{position:relative;float:right;margin-right:15px;margin-top:8px;margin-bottom:8px;background-image:none;border-radius:4px}.navbar-toggle .icon-bar{display:block;height:2px;border-radius:1px}.navbar-toggle .icon-bar+.icon-bar{margin-top:4px}@media (min-width:768px){.navbar-toggle{display:none}}.clearfix:after,.clearfix:before,.navbar-collapse:after,.navbar-collapse:before,.navbar-header:after,.navbar-header:before,.row:after,.row:before{content:" ";display:table}.navbar-toggle .icon-bar{background-color:#008fd5;width:35px;margin:7px 0}@-ms-viewport{width:device-width}.row-inline{letter-spacing:-4px;font-size:0}.row-inline>[class*=col-]{float:none;display:inline-block;letter-spacing:0;font-size:14px;vertical-align:top}.pre_header .navbar-header,.pre_header a[title=ATM]{float:left}.row-inline.align-bottom>[class*=col-]{vertical-align:bottom}.navbar-toggle{border:1px solid #008fd5;padding:5px 10px}.navbar-header{margin-left:25px}a,body,div,footer,form,h3,header,html,i,img,li,nav,section,span,ul{margin:0;padding:0;border:0;font-size:100%;vertical-align:baseline}img{vertical-align:middle;border:0}footer,header,nav,section{display:block}body,html{height:100%;min-width:320px;color:#73848e}body{font-family:'Open Sans',sans-serif;font-size:14px;line-height:22px;font-weight:400;color:#73848e}.nav_header a,h3{color:#23323a}.clearfix:after,.clearfix:before{display:table;content:" "}.section{max-width:1400px;margin:0 auto;padding:0 15px}.lower_header,.pre_header{padding:15px 0}.back-to-top{display:none}.back-to-top a{position:fixed;width:50px;height:50px;bottom:20px;right:20px;opacity:.8;z-index:1000;background:url(../img/back-to-top.png) center center no-repeat #0996d2;cursor:pointer;-webkit-transition:opacity .3s;transition:opacity .3s}:active,:focus{outline:0}.pre_header_right_column{text-align:right}.pre_header_right_column>a{display:inline-block;vertical-align:middle}.navbar-header{display:none}.pre_header_right_column a i{font-size:1.4em;-webkit-transition:color ease-in-out .15s;transition:color ease-in-out .15s}.lower_header{z-index:100;position:relative;background-color:rgba(255,255,255,.67);border-bottom:1px solid rgba(245,245,245,.5);-webkit-transition:background-color ease-in-out .15s;transition:background-color ease-in-out .15s}.nav_header li{display:inline-block;margin-left:30px;text-align:left;-webkit-transition:background-color,padding ease-in-out .15s;transition:background-color,padding ease-in-out .15s}.nav_header li:first-child{margin-left:0}.wrapper{-webkit-transition:.3s filter linear;transition:.3s filter linear}a.button,button{background-color:rgba(0,143,213,.73);color:#fff;font-size:1em;padding:10px 20px;display:inline-block;border:none;-webkit-transition:all .3s;transition:all .3s}.button-form,.container_form,.container_form h3{text-align:center}.container_form_register_login{background:url(../img/section_background_one.jpg) center center no-repeat;background-size:cover}.container_form_register_login .button-form{width:35%}.container_form{padding:45px}.container_form h3{color:#fff;font-size:2.3em;line-height:110%;font-weight:300;margin-bottom:40px}.form_group{margin-bottom:20px;position:relative}.form_group input{color:#19242a;width:100%;border:1px solid transparent;padding:10px 15px;-webkit-transition:background-color .3s;transition:background-color .3s}.container_form form textarea::-webkit-input-placeholder{color:#19242a}.form_group input::-webkit-input-placeholder{color:#19242a}.container_form form textarea{color:#19242a;height:100px;width:100%;border:none;padding:10px 15px;margin-bottom:20px;-webkit-transition:background .3s;transition:background .3s}.button-form{display:inline-block;background:rgba(255,255,255,.15);border:1px solid #707e89;color:#fff;padding:12px 25px;font-size:1em;position:relative;-webkit-transition:background .3s;transition:background .3s}a.button-member-login{margin-left:10px}.button-member-login-rubber{display:none!important}.pros-atm ul{text-align:left;font-size:1.3em;color:#fff;padding-left:30px}.pros-atm ul li{margin-top:35px;padding-left:50px}.pros-atm ul li:first-child{margin-top:0}.pros-atm ul li:before{float:left;margin-top:2px;margin-left:-40px;content:'\e800';margin-right:15px;text-shadow:0 0 3px rgba(0,0,0,.7);font-family:fontello;font-size:1.4em;color:#fff;font-style:normal;font-weight:400;display:inline-block;text-decoration:inherit;text-align:center;font-variant:normal;text-transform:none;line-height:1em;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.pre_header a.button,button{margin:5px 0}.footer{display:block}.upper-footer{background-color:#098ed1}.container-twetter{float:left;padding:25px;background-color:#1f9cda;position:relative}.container-twetter:before{content:'';display:block;position:absolute;right:-25px;top:25px;width:10px;height:10px;border-left:25px solid #1f9cda;border-top:0 solid transparent;border-bottom:25px solid transparent}@media screen and (max-width:991px){.pre_header>.row-inline.align-bottom>[class*=col-]{vertical-align:top}}@media screen and (max-width:767px){.header{text-align:center}.nav_header{margin-bottom:35px}.nav_header li{display:block;cursor:pointer;padding:10px 0;border-bottom:1px solid #0182c4;margin-left:0!important}.pre_header_right_column{display:block;margin-top:15px}.container_form{padding:45px 0}.navbar-header{display:block}.pros-atm{margin-top:45px}}@media screen and (min-width:768px){.navbar-collapse.collapse{display:block!important;height:auto!important;padding-bottom:0;overflow:visible!important}}@media screen and (max-width:450px){.pre_header_right_column{display:none}.button-member-login-rubber{display:block!important}.pre_header a[title=ATM]>img{max-width:100%}.pre_header .navbar-header,.pre_header a[title=ATM]{float:none;margin-left:0}.navbar-toggle{float:none;margin-right:0}}
        </style>


    <div class="container_form container_form_register_login clearfix">

        <div class="row section clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h3>SIGN UP FOR A 15-DAY<br>FREE TRIAL MEMBERSHIP</h3>
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-server">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <div class="form-login-info">
                            <a href="/auth/login">Log in to your account</a>
                        </div>
                    </div>
                @endif
                <form method="POST" action="/auth/register" id="payment-form" novalidate>
                    {!! csrf_field() !!}
                    <div class="form_group">
                        <input type="text" name="firstName" placeholder="Full Name*" value="{{ old('firstName') }}" data-validation="notempty">
                    </div>
                    <!--<div class="form_group">
                        <input type="text" name="lastName" placeholder="Last Name*" value="empty" >
                    </div> -->
                    <div class="form_group">
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email*"
                               data-validation="notempty;isemail">
                    </div>
                    <div class="form_group" style="display: none">
                        <input type="tel" name="dayTimePhone" value="{{ old('dayTimePhone') }}" placeholder="Daytime Phone" data-validation="isnumeric">
                    </div>
                    <div class="form_group">
                        <!--<p style="text-align: left; font-size: 12px; color: white; padding-left: 15px;">Please use +61XXXXXXXXX for AU and +64XXXXXXXXX for NZ-based phones.<br>We will send a verification code to the provided phone.</p> -->
                        <input type="tel" name="mobilePhone" value="{{ old('mobilePhone') }}" placeholder="Mobile*" data-validation="notempty;isphone">
                    </div>
                    <div class="form_group">
                        <input type="password" name="password" value="{{ old('password') }}" placeholder="Password*"
                               data-validation="notempty;minlength:6">
                    </div>
                    <textarea name="comment" value="{{ old('comment') }}" placeholder="Message" style="display: none"></textarea>
                    {{--@include('partials.stripeForm')--}}
                    <input class="button-form" type="submit" id="register-form-submit-button" title="Send" value="Send">

                </form>
                <form method="POST" action="/auth/register" id="register-form" novalidate style="display: none">
                    {!! csrf_field() !!}
                    <input type="hidden" name="firstName" value="{{ old('firstName') }}">
                    <input type="hidden" name="email" value="{{ old('email') }}">
                    <input type="hidden" name="mobilePhone" value="{{ old('mobilePhone') }}">
                    <input type="hidden" name="password" value="{{ old('password') }}">
                    <input class="button-form" type="submit" title="Send" value="Send" style="display: none">

                </form>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 pros-atm">
                <h3>Your 15-day FREE membership to Australian Trading Management unlocks:</h3>
                <ul>
                    <li>All our NZ and AU buy, sell, and hold stock recommendations, with the research and analysis behind every decision</li>
                    <li>Exclusive access to our Australian & New Zealand model equity portfolios</li>
                    <li>Access to our proprietary top trade ideas</li>
                    <li>Timely notifications for all our recommendation changes</li>
                    <li>Full subscription to our daily newsletter providing at the minute insights into our investment views – At the Moment News</li>
                    <li>In depth but simple to understand analysis across a wide range of investments</li>
                    <li>Access to a database of investor education articles</li>
                </ul>
            </div>
        </div>
    </div>

@endsection