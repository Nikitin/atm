@extends('layouts.landing')
@section('title', 'Remind password ')
@section('content')
    <section class="section">
        <div class="container-password-recovery">
            <div class="row row-inline align-middle">
                <div class="col-lg-6 col-md-6 col-sm-6 align-center">
                    <img src="/img/password_recovery.jpg" alt=""/>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-server">
                            <strong>Whoops!</strong> There were some problems with your input.
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="clearfix" method="POST" action="/password/email" novalidate>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <h3>password recovery</h3>

                        <p>You can use this form to recover your password if you have forgotten it. Because your
                            password is
                            securely database, it is impossible actually recover you password, but we will email you a
                            link
                            that will enable you to reset it.
                        </p>
                        <p>
                            Enter your email address below to get started.
                        </p>
                        <div class="form_group">
                            <input type="email" name="email" placeholder="Email" data-validation="notempty;isemail">
                        </div>

                        <input class="button-form" type="submit" value="Send">

                    </form>
                </div>
            </div>

        </div>
    </section>
@endsection