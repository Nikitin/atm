<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') | Australian trading management</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:title" content="ATM"/>
    <meta property="og:description" content="Australian trading management"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://"/>
    <meta property="og:image" content="http:// "/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!-- CRITICAL CSS -->
    <style>

        @charset "UTF-8";

        .description_main_image{
            padding: 30px;
            position: relative;
            background: #2a3b45;
            text-align: left;
            -webkit-transition: background-color .4s ease;
            transition: background-color .4s ease;
        }

        .portfolio-center-column-container[data-trial=true], a.button.biggest {
            visibility: hidden
        }

        .pre_header_right_column a .icon-phone-circled {
            font-size: 1.7em;
            vertical-align: middle
        }

        .pre_header_right_column a .icon-phone-circled + span {
            font-size: 1.2em;
            vertical-align: middle;
            color: #93A6B0
        }

        .char-negative, .mobile-button-auth {
            display: none
        }

        .navbar-toggle, a {
            background-color: transparent
        }

        *, :after, :before {
            box-sizing: border-box;
            outline: 0
        }

        .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
            clear: both
        }

        ul {
            list-style: none
        }

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
        }

        [class*=" icon-"], [class^=icon-] {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            line-height: 1em
        }

        .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            text-decoration: inherit;
            font-variant: normal;
            line-height: 1em;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            text-transform: none;
            text-align: center;
            -moz-osx-font-smoothing: grayscale
        }

        .fontello-icon {
            font-size: 18px;
            color: #93a6b0
        }

        [class*=" icon-"]:before, [class^=icon-]:before {
            speak: none;
            width: 1em;
            margin-right: .2em;
            margin-left: .2em
        }

        .icon-facebook:before {
            content: '\e802'
        }

        .icon-twitter:before {
            content: '\e804'
        }

        .icon-google:before {
            content: '\e805'
        }

        .icon-cancel-circled:before {
            content: '\e80b'
        }

        html {
            font-family: sans-serif;
            -webkit-tap-highlight-color: transparent;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        body {
            background-color: #fff
        }

        button, input {
            color: inherit;
            font: inherit;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit
        }

        button {
            overflow: visible
        }

        .modal, .sr-only {
            overflow: hidden
        }

        button, input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer
        }

        button::-moz-focus-inner, input::-moz-focus-inner {
            border: 0;
            padding: 0
        }

        .sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            margin: -1px;
            padding: 0;
            clip: rect(0, 0, 0, 0);
            border: 0
        }

        .row {
            margin-left: -15px;
            margin-right: -15px
        }

        .col-lg-12, .col-lg-3, .col-lg-6, .col-md-12, .col-md-6, .col-sm-12, .col-sm-6 {
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px
        }

        @media (min-width: 768px) {
            .col-sm-12, .col-sm-6 {
                float: left
            }

            .col-sm-12 {
                width: 100%
            }

            .col-sm-6 {
                width: 50%
            }
        }

        @media (min-width: 992px) {
            .col-md-12, .col-md-6 {
                float: left
            }

            .col-md-12 {
                width: 100%
            }

            .col-md-6 {
                width: 50%
            }
        }

        @media (min-width: 1200px) {
            .col-lg-12, .col-lg-3, .col-lg-6 {
                float: left
            }

            .col-lg-12 {
                width: 100%
            }

            .col-lg-6 {
                width: 50%
            }

            .col-lg-3 {
                width: 25%
            }
        }

        .fade {
            opacity: 0;
            -webkit-transition: opacity .15s linear;
            transition: opacity .15s linear
        }

        .collapse {
            display: none
        }

        .navbar-collapse {
            overflow-x: visible;
            padding-right: 15px;
            padding-left: 15px;
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
            -webkit-overflow-scrolling: touch
        }

        @media (min-width: 768px) {
            .navbar-header {
                float: left
            }

            .navbar-collapse {
                width: auto;
                border-top: 0;
                box-shadow: none
            }

            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        .navbar-toggle {
            position: relative;
            float: right;
            margin-right: 15px;
            margin-top: 8px;
            margin-bottom: 8px;
            background-image: none;
            border-radius: 4px
        }

        .navbar-toggle .icon-bar {
            display: block;
            height: 2px;
            border-radius: 1px
        }

        .navbar-toggle .icon-bar + .icon-bar {
            margin-top: 4px
        }

        .modal {
            display: none;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            -webkit-overflow-scrolling: touch;
            outline: 0
        }

        .modal.fade .modal-dialog {
            -webkit-transform: translate(0, -25%);
            transform: translate(0, -25%);
            opacity: 0;
            -webkit-transition: all .3s ease-out;
            transition: all .3s ease-out
        }

        .modal-dialog {
            width: auto;
            margin: 10px
        }

        .modal-content {
            position: relative;
            background: url(img/section_background_one.jpg) center center no-repeat;
            background-size: cover;
            border: 1px solid #999;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: 6px;
            box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
            background-clip: padding-box;
            outline: 0
        }

        @media (min-width: 768px) {
            .navbar-toggle {
                display: none
            }

            .modal-dialog {
                width: 600px;
                margin: 160px auto
            }

            .modal-content {
                box-shadow: 0 5px 15px rgba(0, 0, 0, .5)
            }
        }

        .nav_header a, .slide_title {
            text-transform: uppercase
        }

        .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
            content: " ";
            display: table
        }

        .pull-right {
            float: right !important
        }

        @-ms-viewport {
            width: device-width
        }

        .row-inline {
            letter-spacing: -4px;
            font-size: 0
        }

        .row-inline > [class*=col-] {
            float: none;
            display: inline-block;
            letter-spacing: 0;
            font-size: 14px;
            vertical-align: top
        }

        .pre_header .navbar-header, .pre_header a[title=ATM] {
            float: left
        }

        .row-inline.align-bottom > [class*=col-] {
            vertical-align: bottom
        }

        .full_width > [class*=col-] {
            padding-left: 0;
            padding-right: 0
        }

        .row.full_width {
            margin-left: 0;
            margin-right: 0
        }

        .navbar-toggle {
            border: 1px solid #008fd5;
            padding: 5px 10px
        }

        .navbar-toggle .icon-bar {
            background-color: #008fd5;
            width: 35px;
            margin: 7px 0
        }

        .navbar-header {
            margin-left: 25px
        }

        a, body, div, form, h3, header, html, i, img, li, nav, p, section, span, ul {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            vertical-align: baseline
        }

        img {
            vertical-align: middle;
            border: 0
        }

        header, main, nav, section {
            display: block
        }

        body, html {
            height: 100%;
            min-width: 320px;
            color: #73848e
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
            line-height: 22px;
            font-weight: 400;
            color: #73848e
        }

        a {
            color: #008fd5;
            text-decoration: none;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        .nav_header a, h3 {
            color: #23323a
        }

        .clearfix:after, .clearfix:before {
            display: table;
            content: " "
        }

        .back-to-top, .ls-slide {
            display: none
        }

        .section {
            max-width: 1400px;
            margin: 0 auto
        }

        .lower_header, .pre_header {
            padding: 15px 0
        }

        .fadeInLeft-one {
            -webkit-animation-name: fadeInLeft-one;
            animation-name: fadeInLeft-one
        }

        .ls-slide {
            background-position: center center;
            overflow: hidden;
            width: 100%;
            height: 100%;
            position: absolute
        }

        .ls-slide > * {
            position: absolute;
            line-height: normal;
            margin: 0;
            left: 0;
            top: 0
        }

        .lower_header, .modal-dialog {
            position: relative
        }

        body * .ls-slide > *, html * .ls-slide > * {
            margin: 0
        }

        body * .ls-slide > *, body * .ls-slide > a, body * .ls-slide > h3, body * .ls-slide > p, html * .ls-slide > *, html * .ls-slide > a, html * .ls-slide > h3, html * .ls-slide > p {
            transition: none;
            -o-transition: none;
            -ms-transition: none;
            -moz-transition: none;
            -webkit-transition: none
        }

        :active, :focus {
            outline: 0
        }

        .pre_header_right_column {
            text-align: right
        }

        .pre_header_right_column > a {
            display: inline-block;
            vertical-align: middle
        }

        .navbar-header {
            display: none
        }

        .button-form, .iconic-button, .nav_header li, a.button, button {
            display: inline-block
        }

        .pre_header_right_column a i {
            font-size: 1.4em;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        .lower_header {
            z-index: 100;
            background-color: rgba(255, 255, 255, .67);
            border-bottom: 1px solid rgba(245, 245, 245, .5);
            -webkit-transition: background-color ease-in-out .15s;
            transition: background-color ease-in-out .15s
        }

        .nav_header li {
            margin-left: 30px;
            text-align: left;
            -webkit-transition: background-color, padding ease-in-out .15s;
            transition: background-color, padding ease-in-out .15s
        }

        .nav_header li:first-child {
            margin-left: 0
        }

        .modal-content {
            padding: 20px 50px;
            text-align: center
        }

        .modal-content .form_group input {
            border: 1px solid #bdbdbd
        }

        .modal-content .button-form {
            padding: 12px 40px;
            font-size: 1em
        }

        .modal-content h3 {
            color: #fff;
            font-size: 1.3em
        }

        .wrapper {
            -webkit-transition: .3s filter linear;
            transition: .3s filter linear
        }

        .modal-dialog > .iconic-button {
            position: absolute;
            z-index: 100
        }

        .modal-dialog > .iconic-button.icon-cancel-circled {
            position: absolute;
            top: 16px;
            right: 15px;
            cursor: pointer;
            font-size: 25px;
            color: #fff
        }

        .form_group, .layerslider, .main {
            position: relative
        }

        .main {
            z-index: 0;
            margin-top: -53px
        }

        .slide_title {
            color: #fff;
            font-size: 40px;
            line-height: 100%
        }

        .slide_text {
            color: #fff;
            font-size: 16px;
            max-width: 700px;
            padding-right: 30px;
            line-height: 1.2
        }

        .slider-button {
            padding: 12px;
            font-size: 1.5em;
            width: 100%;
            max-width: 205px;
            line-height: 24px;
            background: #0074A2;
            border-radius: 5px;
            color: #FFF;
            border: 1px solid #707e89;
            -webkit-transition: background-color, color .4s ease;
            transition: background-color, color .4s ease
        }

        .container_who_we_are {
            background-color: #23323a;
            color: #c4d3de;
            text-align: center;
            overflow: hidden
        }

        .who_we_are_title {
            font-size: 2.1em;
            text-transform: capitalize
        }

        .container_who_we_are > section {
            padding: 20px 15px
        }

        .who_we_are_text {
            font-size: 1.5em;
            line-height: 26px;
            margin: 30px 0;
            color: #fff
        }

        .main_section_image {
            margin-top: 20px
        }

        .main_section_image > [class*=col-] {
            overflow: hidden;
            border-right: 1px solid transparent
        }

        .main_section_image img {
            z-index: 100;
            width: 100%;
            height: auto;
            -webkit-transition: -webkit-transform .4s ease;
            transition: -webkit-transform .4s ease;
            transition: transform .4s ease;
            transition: transform .4s ease, -webkit-transform .4s ease
        }

        .layerslider {
            background: url(../../img/background_layerslider_two.jpg) no-repeat;
            background-size: cover
        }

        a.button, button {
            background-color: rgba(0, 143, 213, .73);
            color: #fff;
            font-size: 1em;
            padding: 10px 20px;
            text-transform: uppercase;
            border: none;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        .form_group input, .form_group input[name=email] {
            background: rgba(255, 255, 255, .6)
        }

        .form_group {
            margin-bottom: 20px
        }

        .form_group input {
            color: #19242a;
            width: 100%;
            border: 1px solid transparent;
            padding: 10px 15px;
            -webkit-transition: background-color .3s;
            transition: background-color .3s
        }

        .modal .form_group input {
            padding-right: 35px
        }

        .form_group input::-webkit-input-placeholder {
            color: #19242a
        }

        .form_group i {
            position: absolute;
            top: 35%;
            right: 10px;
            color: #008fd5;
            font-size: 18px
        }

        .button-form, .iconic-button {
            position: relative
        }

        .button-form {
            text-transform: uppercase;
            text-align: center;
            background: rgba(255, 255, 255, .15);
            border: 1px solid #707e89;
            color: #fff;
            padding: 12px 25px;
            font-size: 1em;
            -webkit-transition: background .3s;
            transition: background .3s
        }

        a.button-member-login {
            margin-left: 10px
        }

        .button-member-login-rubber {
            display: none !important
        }

        .pre_header a.button, button {
            margin: 5px 0
        }

        @media screen and (max-width: 991px) {
            .pre_header > .row-inline.align-bottom > [class*=col-] {
                vertical-align: top
            }
        }

        @media screen and (max-width: 767px) {
            .header {
                text-align: center
            }

            .nav_header {
                margin-bottom: 35px
            }

            .nav_header li {
                display: block;
                cursor: pointer;
                padding: 10px 0;
                border-bottom: 1px solid #0182c4;
                margin-left: 0 !important
            }

            .pre_header_right_column {
                display: block;
                margin-top: 15px
            }

            .navbar-header {
                display: block
            }

            .main {
                margin-top: -32px
            }
        }

        @media screen and (min-width: 768px) {
            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        @media screen and (max-width: 450px) {
            .pre_header_right_column {
                display: none
            }

            .button-member-login-rubber {
                display: block !important
            }

            .pre_header a[title=ATM] > img {
                max-width: 100%
            }

            .pre_header .navbar-header, .pre_header a[title=ATM] {
                float: none;
                margin-left: 0
            }

            .navbar-toggle {
                float: none;
                margin-right: 0
            }

            .layerslider {
                display: none
            }
        }

        .footer, .widget-text-one > img {
            display: none
        }

        @media screen and (min-width: 1900px) {
            .section {
                max-width: 85% !important
            }

            .row-inline > [class*=col-], body {
                font-size: 16px !important
            }

            .container_freetrial a.button.biggest {
                padding: 25px 100px
            }

            .pre_header .section {
                padding: 0 15px
            }

            .mobile-button-auth {
                display: none
            }
        }

        #navbar-collapse {
            padding: 0
        }

        .pre_header_right_column a .icon-phone-circled {
            font-size: 1.7em;
            vertical-align: middle;
        }

        .pre_header_right_column a .icon-phone-circled + span {
            font-size: 1.2em;
            vertical-align: middle;
            color: #93A6B0;
        }

        @charset "UTF-8";
        .char-negative, .mobile-button-auth {
            display: none;
        }

        .navbar-toggle, a {
            background-color: transparent
        }

        *, :after, :before {
            box-sizing: border-box;
            outline: 0
        }

        .clearfix:after, .navbar-collapse:after, .navbar-header:after, .row:after {
            clear: both
        }

        ul {
            list-style: none
        }

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype')
        }

        [class*=" icon-"], [class^=icon-] {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            line-height: 1em
        }

        .fontello-icon, [class*=" icon-"]:before, [class^=icon-]:before {
            font-family: fontello;
            font-style: normal;
            font-weight: 400;
            text-decoration: inherit;
            font-variant: normal;
            line-height: 1em;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            text-transform: none;
            text-align: center;
        }

        .fontello-icon {
            font-size: 18px;
            color: transparent
        }

        [class*=" icon-"]:before, [class^=icon-]:before {
            speak: none;
            width: 1em;
            margin-right: .2em;
            margin-left: .2em
        }

        .icon-facebook:before {
            content: '\e802'
        }

        .icon-twitter:before {
            content: '\e804'
        }

        .icon-google:before {
            content: '\e805'
        }

        .icon-cancel-circled:before {
            content: '\e80b'
        }

        html {
            font-family: sans-serif;
            -webkit-tap-highlight-color: transparent;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%
        }

        body {
            background-color: #fff
        }

        button, input {
            color: inherit;
            font: inherit;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit
        }

        button {
            overflow: visible
        }

        .modal, .sr-only {
            overflow: hidden
        }

        button, input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer
        }

        button::-moz-focus-inner, input::-moz-focus-inner {
            border: 0;
            padding: 0
        }

        .sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            margin: -1px;
            padding: 0;
            clip: rect(0, 0, 0, 0);
            border: 0
        }

        .row {
            margin-left: -15px;
            margin-right: -15px
        }

        .col-lg-12, .col-lg-3, .col-lg-6, .col-md-12, .col-md-6, .col-sm-12, .col-sm-6 {
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px
        }

        @media (min-width: 768px) {
            .col-sm-12, .col-sm-6 {
                float: left
            }

            .col-sm-12 {
                width: 100%
            }

            .col-sm-6 {
                width: 50%
            }
        }

        @media (min-width: 992px) {
            .col-md-12, .col-md-6 {
                float: left
            }

            .col-md-12 {
                width: 100%
            }

            .col-md-6 {
                width: 50%
            }
        }

        @media (min-width: 1200px) {
            .col-lg-12, .col-lg-3, .col-lg-6 {
                float: left
            }

            .col-lg-12 {
                width: 100%
            }

            .col-lg-6 {
                width: 50%
            }

            .col-lg-3 {
                width: 25%
            }
        }

        .fade {
            opacity: 0;
            -webkit-transition: opacity .15s linear;
            transition: opacity .15s linear
        }

        .collapse {
            display: none
        }

        .navbar-collapse {
            overflow-x: visible;
            padding-right: 15px;
            padding-left: 15px;
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
            -webkit-overflow-scrolling: touch
        }

        @media (min-width: 768px) {
            .navbar-header {
                float: left
            }

            .navbar-collapse {
                width: auto;
                border-top: 0;
                box-shadow: none
            }

            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        .navbar-toggle {
            position: relative;
            float: right;
            margin-right: 15px;
            margin-top: 8px;
            margin-bottom: 8px;
            background-image: none;
            border-radius: 4px
        }

        .navbar-toggle .icon-bar {
            display: block;
            height: 2px;
            border-radius: 1px
        }

        .navbar-toggle .icon-bar + .icon-bar {
            margin-top: 4px
        }

        .modal {
            display: none;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            -webkit-overflow-scrolling: touch;
            outline: 0
        }

        .modal.fade .modal-dialog {
            -webkit-transform: translate(0, -25%);
            transform: translate(0, -25%);
            opacity: 0;
            -webkit-transition: all .3s ease-out;
            transition: all .3s ease-out
        }

        .modal-dialog {
            width: auto;
            margin: 10px
        }

        .modal-content {
            position: relative;
            background: url(img/section_background_one.jpg) center center no-repeat;
            background-size: cover;
            border: 1px solid #999;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: 6px;
            box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
            background-clip: padding-box;
            outline: 0
        }

        @media (min-width: 768px) {
            .navbar-toggle {
                display: none
            }

            .modal-dialog {
                width: 600px;
                margin: 160px auto
            }

            .modal-content {
                box-shadow: 0 5px 15px rgba(0, 0, 0, .5)
            }
        }

        .nav_header a, .slide_title {
            text-transform: uppercase
        }

        .clearfix:after, .clearfix:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .row:after, .row:before {
            content: " ";
            display: table
        }

        .pull-right {
            float: right !important
        }

        @-ms-viewport {
            width: device-width
        }

        .row-inline {
            letter-spacing: -4px;
            font-size: 0
        }

        .row-inline > [class*=col-] {
            float: none;
            display: inline-block;
            letter-spacing: 0;
            font-size: 14px;
            vertical-align: top
        }

        .pre_header .navbar-header, .pre_header a[title=ATM] {
            float: left
        }

        .row-inline.align-bottom > [class*=col-] {
            vertical-align: bottom
        }

        .full_width > [class*=col-] {
            padding-left: 0;
            padding-right: 0
        }

        .row.full_width {
            margin-left: 0;
            margin-right: 0
        }

        .navbar-toggle {
            border: 1px solid #008fd5;
            padding: 5px 10px
        }

        .navbar-toggle .icon-bar {
            background-color: #008fd5;
            width: 35px;
            margin: 7px 0
        }

        .navbar-header {
            margin-left: 25px
        }

        a, body, div, form, h3, header, html, i, img, li, nav, p, section, span, ul {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            vertical-align: baseline
        }

        img {
            vertical-align: middle;
            border: 0
        }

        header, main, nav, section {
            display: block
        }

        body, html {
            height: 100%;
            min-width: 320px;
            color: #73848e
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
            line-height: 22px;
            font-weight: 400;
            color: #73848e
        }

        a {
            color: #008fd5;
            text-decoration: none;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        .nav_header a, h3 {
            color: #23323a
        }

        .clearfix:after, .clearfix:before {
            display: table;
            content: " "
        }

        .back-to-top, .ls-slide {
            display: none
        }

        .section {
            max-width: 1400px;
            margin: 0 auto;
        }

        .lower_header, .pre_header {
            padding: 15px 0
        }

        .fadeInLeft-one {
            -webkit-animation-name: fadeInLeft-one;
            animation-name: fadeInLeft-one
        }

        .ls-slide {
            background-position: center center;
            overflow: hidden;
            width: 100%;
            height: 100%;
            position: absolute
        }

        .ls-slide > * {
            position: absolute;
            line-height: normal;
            margin: 0;
            left: 0;
            top: 0
        }

        .lower_header, .modal-dialog {
            position: relative
        }

        body * .ls-slide > *, html * .ls-slide > * {
            margin: 0
        }

        body * .ls-slide > *, body * .ls-slide > a, body * .ls-slide > h3, body * .ls-slide > p, html * .ls-slide > *, html * .ls-slide > a, html * .ls-slide > h3, html * .ls-slide > p {
            transition: none;
            -o-transition: none;
            -ms-transition: none;
            -moz-transition: none;
            -webkit-transition: none
        }

        :active, :focus {
            outline: 0
        }

        .pre_header_right_column {
            text-align: right
        }

        .pre_header_right_column > a {
            display: inline-block;
            vertical-align: middle
        }

        .navbar-header {
            display: none
        }

        .button-form, .iconic-button, .nav_header li, a.button, button {
            display: inline-block
        }

        .pre_header_right_column a i {
            font-size: 1.4em;
            -webkit-transition: color ease-in-out .15s;
            transition: color ease-in-out .15s
        }

        .lower_header {
            z-index: 100;
            background-color: rgba(255, 255, 255, .67);
            border-bottom: 1px solid rgba(245, 245, 245, .5);
            -webkit-transition: background-color ease-in-out .15s;
            transition: background-color ease-in-out .15s
        }

        .nav_header li {
            margin-left: 30px;
            text-align: left;
            -webkit-transition: background-color, padding ease-in-out .15s;
            transition: background-color, padding ease-in-out .15s
        }

        .nav_header li:first-child {
            margin-left: 0
        }

        .modal-content {
            padding: 20px 50px;
            text-align: center
        }

        .modal-content .form_group input {
            border: 1px solid #bdbdbd
        }

        .modal-content .button-form {
            padding: 12px 40px;
            font-size: 1em
        }

        .modal-content h3 {
            color: #fff;
            font-size: 1.3em
        }

        .wrapper {
            -webkit-transition: .3s filter linear;
            transition: .3s filter linear
        }

        .modal-dialog > .iconic-button {
            position: absolute;
            z-index: 100
        }

        .modal-dialog > .iconic-button.icon-cancel-circled {
            position: absolute;
            top: 16px;
            right: 15px;
            cursor: pointer;
            font-size: 25px;
            color: #fff
        }

        .form_group, .layerslider, .main {
            position: relative
        }

        .main {
            z-index: 0;
            margin-top: -53px
        }

        .slide_title {
            color: #fff;
            font-size: 40px;
            line-height: 100%
        }

        .slide_text {
            color: #fff;
            font-size: 16px;
            max-width: 700px;
            padding-right: 30px;
            line-height: 1.2
        }

        .slider-button {
            padding: 12px;
            font-size: 1.5em;
            width: 100%;
            max-width: 205px;
            line-height: 24px;
            background: #0074A2;
            border-radius: 5px;
            color: #FFF;
            border: 1px solid #707e89;
            -webkit-transition: background-color, color .4s ease;
            transition: background-color, color .4s ease
        }

        .container_who_we_are {
            background-color: #23323a;
            color: #c4d3de;
            text-align: center;
            overflow: hidden
        }

        .who_we_are_title {
            font-size: 2.1em;
            text-transform: capitalize
        }

        .container_who_we_are > section {
            padding: 20px 15px
        }

        .who_we_are_text {
            font-size: 1.5em;
            line-height: 26px;
            margin: 30px 0;
            color: #fff
        }

        .main_section_image {
            margin-top: 20px
        }

        .main_section_image > [class*=col-] {
            overflow: hidden;
            border-right: 1px solid transparent
        }

        .main_section_image img {
            z-index: 100;
            width: 100%;
            height: auto;
            -webkit-transition: -webkit-transform .4s ease;
            transition: -webkit-transform .4s ease;
            transition: transform .4s ease;
            transition: transform .4s ease, -webkit-transform .4s ease
        }

        .layerslider {
            background: url(../../img/background_layerslider_two.jpg) no-repeat;
            background-size: cover
        }

        a.button, button {
            background-color: rgba(0, 143, 213, .73);
            color: #fff;
            font-size: 1em;
            padding: 10px 20px;
            text-transform: uppercase;
            border: none;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        .form_group input, .form_group input[name=email] {
            background: rgba(255, 255, 255, .6)
        }

        .form_group {
            margin-bottom: 20px
        }

        .form_group input {
            color: #19242a;
            width: 100%;
            border: 1px solid transparent;
            padding: 10px 15px;
            -webkit-transition: background-color .3s;
            transition: background-color .3s
        }

        .modal .form_group input {
            padding-right: 35px
        }

        .form_group input::-webkit-input-placeholder {
            color: #19242a
        }

        .form_group i {
            position: absolute;
            top: 35%;
            right: 10px;
            color: #008fd5;
            font-size: 18px
        }

        .button-form, .iconic-button {
            position: relative
        }

        .button-form {
            text-transform: uppercase;
            text-align: center;
            background: rgba(255, 255, 255, .15);
            border: 1px solid #707e89;
            color: #fff;
            padding: 12px 25px;
            font-size: 1em;
            -webkit-transition: background .3s;
            transition: background .3s
        }

        a.button-member-login {
            margin-left: 10px
        }

        .button-member-login-rubber {
            display: none !important
        }

        .pre_header a.button, button {
            margin: 5px 0
        }

        @media screen and (max-width: 991px) {
            .pre_header > .row-inline.align-bottom > [class*=col-] {
                vertical-align: top
            }
        }

        @media screen and (max-width: 767px) {
            .header {
                text-align: center
            }

            .nav_header {
                margin-bottom: 35px
            }

            .nav_header li {
                display: block;
                cursor: pointer;
                padding: 10px 0;
                border-bottom: 1px solid #0182c4;
                margin-left: 0 !important
            }

            .pre_header_right_column {
                display: block;
                margin-top: 15px
            }

            .navbar-header {
                display: block
            }

            .main {
                margin-top: -32px
            }
        }

        @media screen and (min-width: 768px) {
            .navbar-collapse.collapse {
                display: block !important;
                height: auto !important;
                padding-bottom: 0;
                overflow: visible !important
            }
        }

        @media screen and (max-width: 450px) {
            .pre_header_right_column {
                display: none
            }

            .button-member-login-rubber {
                display: block !important
            }

            .pre_header a[title=ATM] > img {
                max-width: 100%
            }

            .pre_header .navbar-header, .pre_header a[title=ATM] {
                float: none;
                margin-left: 0
            }

            .navbar-toggle {
                float: none;
                margin-right: 0
            }

            .layerslider {
                display: none
            }
        }

        @media screen and (min-width: 1900px) {
            .section {
                max-width: 85%;
            }

            body, .row-inline > *[class*="col-"] {
                font-size: 16px;
            }

            .container_freetrial a.button.biggest {
                padding: 25px 100px
            }
        }

        .widget-text-one > img {
            display: none;
        }

        .footer {
            display: none;
        }

        a.button.biggest {
            visibility: hidden;
        }

        @media screen and (min-width: 1900px) {
            .section {
                max-width: 85% !important;
            }

            body, .row-inline > *[class*="col-"] {
                font-size: 16px !important;
            }

            .container_freetrial a.button.biggest {
                padding: 25px 100px
            }

            .pre_header .section {
                padding: 0 15px;
            }

            .mobile-button-auth {
                display: none;
            }
        }

        .col-lg-15 {
            position: relative;
            min-height: 1px;
            padding-right: 10px;
            padding-left: 10px;
        }


        @media (min-width: 1200px) {
            .col-lg-15 {
                width: 20%;
                float: left;
            }
        }


    </style>

    @yield('style')

            <!-- FAVICONS -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('ico/atm144x144.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('ico/atm114x114.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('atm72x72.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('ico/atm57x57.png')}}">
    <link rel="shortcut icon" href="{{asset('ico/favicon.png')}}">
</head>

<body gram_dict="true">
@yield('modal')
        <!-- WRAPPER -->
<div class="wrapper">
    <!-- NAVBAR -->
    <header class="header">
        @include('layouts.landing-header')
    </header>
    <!-- PAGE CONTENT -->

    @yield('content')

            <!-- END PAGE CONTENT -->

    <!-- FOOTER -->
    <footer class="footer clearfix">
        @include('layouts.landing-footer')
    </footer>
    <div class="back-to-top">
        <a href="#anchor-landmark" id="go-top-anchor"></a>
    </div>

    <!-- END FOOTER -->
</div>


<!-- END WRAPPER -->

<!-- JAVASCRIPTS -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
    /**
     * Visualize an HTML table using Highcharts. The top (horizontal) header
     * is used for series names, and the left (vertical) header is used
     * for category names. This function is based on jQuery.
     * @param {Object} table The reference to the HTML table to visualize
     * @param {Object} options Highcharts options
     */


    $(document).ready(function () {
        $("head").append("<link rel='stylesheet' type='text/css' href='{{asset('/css/frontend.min.css')}}' />");
        $("head").append("<link rel='stylesheet' type='text/css' href='{{asset('/css/ie.css')}}' />");


        <!-- If optimize img not found, then replace src for old img -->
        $("img").error(function () {
            if ($(this).unbind("error").attr('src').indexOf('uploads-min') != -1) {
                var src = $(this).attr('src').replace('/uploads-min', '');
                $(this).unbind("error").attr("src", src);
            }
        });


    });

    <!-- If optimize img not found, then replace src for old img -->
    $("img").error(function () {
        if ($(this).unbind("error").attr('src').indexOf('uploads-min') != -1) {
            var src = $(this).attr('src').replace('/uploads-min', '');
            $(this).unbind("error").attr("src", src);
        }
    });






    <!-- Google Tag Manager -->
    (function () {
        var url = "atmstrategy.com.au/js/gtm.js";
        var gtm = document.createElement('script');
        gtm.type = 'text/javascript';
        gtm.async = true;
        gtm.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + url;
        var s = document.getElementsByTagName('body')[0];
        s.parentNode.insertBefore(gtm, s);
    })();
    <!-- End Google Tag Manager -->


</script>

{{--<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/modules/data.js"></script>--}}
<script src="{{asset('js/frontend.min.js')}}"></script>

@yield('scripts')


<noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-PS8H84"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>

</body>

</html>

