<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

    <nav class="vertical_nav">

        <ul class="menu-vertical js-menu">

            <li class="menu--item  menu--item__has_sub_menu menu--subitens__opened">

                <label class="menu--link" style="background-color: #3d4952" title="Model portfolios">
                    <span class="menu--label">MODEL PORTFOLIOS</span>
                </label>

                <ul class="sub_menu">
                    <li class="sub_menu--item">
                        <a href="/member/AUPortfolio" title="ATM model Australian equity portfolio" class="sub_menu--link">ATM AU portfolio</a>
                    </li>
                    <li class="sub_menu--item">
                        <a href="/member/NZPortfolio" title="ATM model New Zealand equity portfolio"  class="sub_menu--link">ATM NZ Portfolio</a>
                    </li>
                    <li class="sub_menu--item">
                        <a href="/member/ConcentratedPortfolio" title="Concentrated Portfolio"  class="sub_menu--link">Concentrated Portfolio</a>
                    </li>
                    <li class="sub_menu--item">
                        <a href="/member/Thematics" title="Portfolio Thematics"  class="sub_menu--link">Portfolio Thematics</a>
                    </li>
                </ul>
            </li>

            <li class="menu--item  menu--item__has_sub_menu menu--subitens__opened">

                <label class="menu--link" style="background-color: #3d4952" title="ATM stocks reports">
                    <a href="#" title="ATM stocks reports">
                        <span class="menu--label">ATM Stock Reports</span>
                    </a>
                </label>

                <ul class="sub_menu">
                    <li class="sub_menu--item">
                        <a href="/member/stocks/au" title="Recommendations for Australian Stocks" class="sub_menu--link">Australian Stocks</a>
                    </li>
                    <li class="sub_menu--item">
                        <a href="/member/stocks/nz" title="Recommendations for New Zealand Stocks" class="sub_menu--link">New Zealand Stocks</a>
                    </li>
                </ul>

            </li>

            <li class="menu--item  menu--item__has_sub_menu menu--subitens__opened">

                <label class="menu--link" style="background-color: #3d4952" title="Research reports">
                    <a href="#" title="Research reports">
                        <span class="menu--label">Research Reports</span>
                    </a>
                </label>

                <ul class="sub_menu">
                    <li class="sub_menu--item">
                        <a href="/member/report" title="All reports" class="sub_menu--link">All Reports</a>
                    </li>
                    <li class="sub_menu--item">
                        <a href="/member/report/topTrades" title="ATM top trades" class="sub_menu--link">ATM Top Trades & Market Movers</a>
                    </li>
                    <li class="sub_menu--item">
                        <a href="/member/report/monthly" title="Latest ATM monthly newsletter" class="sub_menu--link">Latest ATM Monthly Newsletter</a>
                    </li>
                    <!--
                    <li class="sub_menu--item">
                        <a href="/member/report/daily" title="&#34At the moment news&#34 archive" class="sub_menu--link">"At the Moment News" Archive</a>
                    </li>
                    -->
                    <li class="sub_menu--item">
                        <a href="/member/report/investor" title="Investor education" class="sub_menu--link">Investor Education</a>
                    </li>
                </ul>

            </li>

            <li class="menu--item menu--item__has_sub_menu menu--subitens__opened" style="background-color: #3d4952">

                <label class="menu--link" title="My details">
                    <span class="menu--label">My Details</span>
                </label>

                <ul class="sub_menu">
                    <li class="sub_menu--item">
                        <a href="/member/contact-details" title="Change your contact details" class="sub_menu--link">My Contact Details</a>
                    </li>
                    <!--<li class="sub_menu--item">
                        <a href="/member/update-email" title="Update your email address"  class="sub_menu--link">My Email Address</a>
                    </li>-->
                    <li class="sub_menu--item">
                        <a href="/member/subscription" title="View your subscription"  class="sub_menu--link">My Subscription</a>
                    </li>
                </ul>
            </li>

        </ul>

        <button id="collapse_menu" class="collapse_menu"></button>

    </nav>

</div>