<div class="upper-footer clearfix">
    <section class="section">
        <div class="container-twetter">
            <a href="https://twitter.com/AUSTRALASIANATM"><i class="fontello-icon icon-twitter"></i></a>
        </div>
    </section>
</div>
<div class="main-footer clearfix">
    <section class="section">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 ">
                <div class="widget-text-one">
                    <img src="/img/logo.png" alt="logo" title="ATM" width="100%" height="100%" />

                    <p>ATM’s investment philosophy – at ATM we believe a true measure of success is
                        making positive returns for our members and that we should practice what we
                        preach
                    </p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 align-center">
                <div class="widget-text-three">
                    <h4>Follow Us</h4>

                    <div class="footer_social_media">
                        <a href="https://www.facebook.com/atmstrategy/" target="_blank" data-toggle="tooltip" title="Facebook"><i
                                    class="fontello-icon icon-facebook"></i></a>
                        <a href="https://twitter.com/AUSTRALASIANATM" target="_blank" data-toggle="tooltip" title="Twitter"><i
                                    class="fontello-icon icon-twitter"></i></a>
                        <a href="https://plus.google.com/102413346868706753580" target="_blank" data-toggle="tooltip" title="Google"><i
                                    class="fontello-icon icon-linkedin"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 align-center">

                <div>
                    <h4>Contact</h4>
                    <ul>
                        <li style="margin-bottom: 10px">
                            <a class="footer-email" title="Contact to ATM" target="_top" href="mailto:info@atmstrategy.com.au">info@atmstrategy.com.au</a>
                        </li>
                        <li style="margin-bottom: 10px">
                            <a class="footer-email" title="+64-27-878-3600" href="tel:+64278783600">+64-27-878-3600</a> - New Zealand
                        </li>
                        <li>
                            <a class="footer-email" title="+1800-026-778 " href="tel:1800026778">1800-026-778</a> - Toll Free Australia
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="lower-footer">
    <section class="section">
                <p>© 2015 Australasian Trading Management. All Rights Reserved. The site contains investment reports, views,
                    opinions, conclusions, estimates, recommendations and other information (Information). However,
                    such Information comprises general securities information only, and has not been prepared taking into account the particular investment objectives,
                    financial situation and needs of any particular person. Accordingly, you should assess whether it is appropriate in light of your individual
                    circumstances or contact your financial planner or advisor.</p>
    </section>
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 935833399;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/935833399/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
</div>