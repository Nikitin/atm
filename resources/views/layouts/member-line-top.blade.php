<div class="members_info">
    <div class="row row-inline align-middle section">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 align-left">
            <div class="members_info_left_column">
                <span>{{me()->firstName}} {{me()->lastName}}</span>
            </div>
        </div>
    </div>
</div>

<div class="clearfix toogle_container section">
    <button type="button" class="navbar-toggle toggle_menu" id="toggleMenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>