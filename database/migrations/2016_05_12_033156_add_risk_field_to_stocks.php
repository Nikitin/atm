<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRiskFieldToStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->string('risk')->nullable();
            $table->string('capitalReturn')->nullable();
            $table->string('totalReturn')->nullable();
            $table->timestamp('recommendation_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->dropColumn('risk');
            $table->dropColumn('capitalReturn');
            $table->dropColumn('totalReturn');
            $table->dropColumn('recommendation_date');
        });
    }
}
