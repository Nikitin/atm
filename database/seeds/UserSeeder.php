<?php

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $faker = (new FakerGenerator())->generate();
        $users = [
            [
                'email' => 'admin@example.com',
                'password' => bcrypt('admin'),
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'dayTimePhone' => $faker->phoneNumber,
                'mobilePhone' => $faker->phoneNumber,
                'postcode' => $faker->postcode,
                'comment' => $faker->sentence(5),
                'status' => 'admin',
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ],
            [
                'email' => 'trial@example.com',
                'password' => bcrypt('trial'),
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'dayTimePhone' => $faker->phoneNumber,
                'mobilePhone' => $faker->phoneNumber,
                'postcode' => $faker->postcode,
                'comment' => $faker->sentence(5),
                'status' => 'trial',
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ],
            [
                'email' => 'full@example.com',
                'password' => bcrypt('full'),
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'dayTimePhone' => $faker->phoneNumber,
                'mobilePhone' => $faker->phoneNumber,
                'postcode' => $faker->postcode,
                'comment' => $faker->sentence(5),
                'status' => 'full',
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ],
            [
                'email' => 'blocked@example.com',
                'password' => bcrypt('blocked'),
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'dayTimePhone' => $faker->phoneNumber,
                'mobilePhone' => $faker->phoneNumber,
                'postcode' => $faker->postcode,
                'comment' => $faker->sentence(5),
                'status' => 'blocked',
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ],
            [
                'email' => 'mail@example.com',
                'password' => bcrypt('mail'),
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'dayTimePhone' => $faker->phoneNumber,
                'mobilePhone' => $faker->phoneNumber,
                'postcode' => $faker->postcode,
                'comment' => $faker->sentence(5),
                'status' => 'notActive',
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ]
        ];
        foreach ($users as $key => $user) {
            User::create($user);
        }
    }

}
