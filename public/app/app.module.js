(function() {
    'use strict';

    angular.module('app', [
        /*
         * Order is not important. Angular makes a
         * pass to register all of the modules listed
         * and then when app.dashboard tries to use app.data,
         * it's components are available.
         */

        /*
         * Everybody has access to these.
         * We could place these under every feature area,
         * but this is easier to maintain.
         */
        'app.core',
        'app.data',

        /*
         * Feature areas
         */
        'app.config', 'app.directives', 'app.layout', 'app.dashboard', 'app.users',
        'app.home', 'app.auportfolio', 'app.nzportfolio', 'app.concentrated', 'app.stocks', 'app.notes',
        'app.reports', 'app.thematics'
    ]).filter("YesNoField", function() {
        return function(fieldValueUnused, item) {
            if (item) {
                return 'Yes';
            } else {
                return 'No';
            }
        };
    });
})();
