(function() {
    'use strict';

    var app = angular.module('app.core', [
        /*
         * Angular modules
         */
        'ngAnimate', 'ngSanitize', 'ui.router',
        /*
         * Our reusable cross app code modules
         */
        'blocks.exception', 'blocks.logger', 'blocks.router',
        /*
         * 3rd Party modules
         */
        'angular-loading-bar', 'oc.lazyLoad', 'trNgGrid', 'formFor'
    ]);
})();
