// =========================================================================
// CONFIGURATION ROUTE
// =========================================================================
(function() {
    'use strict';
    angular.module('app.config', [])

    // Setup global settings
    .factory('settings', ['$rootScope', function($rootScope) {
        var baseURL = '',
            settings = {
                pluginPath: baseURL + '/vendor',
                pluginCommercialPath: baseURL + '/js/plugins',
                globalImagePath: baseURL + '/img',
                adminImagePath: baseURL + '/img/admin',
                cssPath: baseURL + '/css',
                dataPath: baseURL + '/app/data'
            };
        $rootScope.settings = settings;
        return settings;
    }])

    // Configuration angular loading bar
    .config(function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
    })

    // Configuration event, debug and cache
    .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            events: true,
            debug: false,
            cache: false,
            cssFilesInsertBefore: 'ng_load_plugins_before',
            /*modules: [{
                            name: 'app.core.demo',
                            files: ['js/modules/core/demo.js']
                        }]*/

        });
    }])

    // Init app run
    .run(["$rootScope", "settings", "$state", '$location',
        function($rootScope, settings, $state, $location) {
            $rootScope.$state = $state; // state to be accessed from view
            $rootScope.$location = $location; // location to be accessed from view
            $rootScope.settings = settings; // global settings to be accessed from view
        }
    ]);
})();
