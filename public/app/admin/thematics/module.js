(function() {
    'use strict';

    angular
        .module('app.thematics', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'thematics': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'thematics.index': {
                url: '/thematics',
                parent: 'thematics',
                templateUrl: '/app/admin/thematics/html/thematics.html',
                controller: 'Thematics as vm',
                title: 'Portfolio Thematics page',
                data: {
                    nav: 1,
                    pageTitle: 'Portfolio Thematics',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'Portfolio Thematics page',
                        subtitle: 'Portfolio Thematics page'
                    },
                    breadcrumbs: [{
                        title: 'Portfolio'
                    }, {
                        title: 'Portfolio Thematics page'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    '/js/plugins/ckeditor/contents.css',
                                ]
                            }, {
                                name: 'ngCkeditor',
                                files: [
                                    '/js/plugins/ckeditor/ckeditor.js',
                                    pluginPath + '/ng-ckeditor/ng-ckeditor.min.js',
                                ]
                            }, {
                                name: 'app.thematics',
                                files: [
                                    '/app/admin/thematics/ctrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
