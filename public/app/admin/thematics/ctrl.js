(function() {
    'use strict';
    angular.module('app.thematics').controller('Thematics', Thematics);

    Thematics.$inject = ['logger', '$timeout', '$http', '$q'];

    function Thematics(logger, $timeout, $http, $q) {

        var vm = this;
        vm.getText = getText;
        vm.saveText = saveText;
        vm.text = {};
        vm.editorOptions = {
            language: 'en',
            uiColor: '#000000'
        };

        activate();

        function activate() {
            getText();
        }

        function getText() {
            var path = '/api/v1/portfolio/text/Thematics';
            $http.get(path)
                .success(function(data, status, headers, config) {
                    vm.text.textBody = data.body;
                })
                .error(function(data, status, headers, config) {
                    logger.error('text data not received');
                });
        }

        function saveText() {
            var path = '/api/v1/portfolio/text';
            $http.post(path, {
                    title: 'Thematics',
                    text: vm.text.textBody,
                    type: 'Thematics'
                })
                .success(function(data, status, headers, config) {
                    logger.success(data.message);
                })
                .error(function(data, status, headers, config) {
                    logger.error(data.message);
                });
        }

    }
})();
