(function() {
    'use strict';
    angular.module('app.dashboard').controller('DashboardCtrl', DashboardCtrl);
    DashboardCtrl.$inject = ['logger', 'dashboard.model'];

    function DashboardCtrl(logger, model) {
        var vm = this;
        vm.getDashboardData = getDashboardData;
        vm.data = {};

        activate();

        function activate() {
            getDashboardData();
        }

        function getDashboardData() {
            vm.data = model.getDashboardData();
            return logger.info('Activated Dashboard View');
        }
    }
})();
