(function() {
    'use strict';

    angular
        .module('app.dashboard', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'dashboard': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'dashboard.index': {
                url: '/dashboard',
                parent: 'dashboard',
                templateUrl: '/app/admin/dashboard/dashboard.html',
                controller: 'DashboardCtrl as vm',
                title: 'Dashboard',
                data: {
                    nav: 1,
                    pageTitle: 'DASHBOARD',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'Dashboard',
                        subtitle: 'dashboard & statistics'
                    }
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/jquery.gritter/css/jquery.gritter.css'
                                ]
                            }, {
                                name: 'app.dashboard',
                                files: [
                                    pluginPath + '/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
                                    pluginPath + '/jquery.gritter/js/jquery.gritter.min.js',
                                    pluginPath + '/skycons-html5/skycons.js',
                                    '/app/admin/dashboard/dashboard.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/dashboard/model.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
