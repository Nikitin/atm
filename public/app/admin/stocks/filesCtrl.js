(function() {
    'use strict';

    angular
        .module('app.stocks')
        .controller('StockFilesCtrl', StockFilesCtrl);

    StockFilesCtrl.$inject = ['Upload', 'logger', '$stateParams', '$timeout', '$state', '$http'];

    function StockFilesCtrl(Upload, logger, $stateParams, $timeout, $state, $http) {

        var vm = this;
        vm.uploadFiles = uploadFiles;
        vm.getAll = getAll;
        vm.del = del;
        vm.modalMessage = 'are you sure?';

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.title = 'Files for stock';
                getAll($stateParams.id);
            } else {
                logger.error('there is no such path');
                $state.go('stocks.index');
            }
        }

        function getAll(id) {
            var path = '/api/v1/documents/all/stocks/' + id;
            $http.get(path)
                .success(function(data, status, headers, config) {
                    vm.documents = data;
                })
                .error(function(data, status, headers, config) {
                    logger.error('files for this stock not loaded');
                });
        }

        function uploadFiles(files, errFiles) {
            vm.files = files;
            vm.errFiles = errFiles;
            angular.forEach(files, function(file) {
                file.upload = Upload.upload({
                    url: '/api/v1/documents/store',
                    data: {
                        file: file,
                        author: vm.author,
                        name: vm.description,
                        category: 'stocks',
                        categoryId: $stateParams.id
                    }
                });

                file.upload.then(function(response) {
                    $timeout(function() {
                        vm.author = null;
                        vm.description = null;
                        file.result = response.data;
                        logger.info(response.data.message);
                        getAll($stateParams.id);
                        vm.disableButton = false;
                    });
                }, function(response) {
                    if (response.status > 0)
                        vm.errorMsg = response.status + ': ' + response.data.message;
                }, function(evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            });
        }

        function del(id) {
            if (window.confirm(vm.modalMessage)) {
                var path = '/api/v1/documents/' + id;
                $http.delete(path)
                    .success(function(data, status) {
                        logger.info('file deleted');
                        getAll($stateParams.id);
                    })
                    .error(function(data, status) {
                        logger.error('file not deleted');
                    });
            }
        }
    }
})();
