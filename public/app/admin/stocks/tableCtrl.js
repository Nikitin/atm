(function() {
    'use strict';

    angular
        .module('app.stocks')
        .controller('StocksTableCtrl', StocksTableCtrl);

    StocksTableCtrl.$inject = ['stocks.model', 'logger'];

    function StocksTableCtrl(model, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.fields = ['code', 'name', 'sharePrice', 'price', 'dividend', 'lastYearReturn', 'marketCap', 'region'];
        vm.del = del;
        vm.tableActions = tableActions;
        vm.getAll = getAll;
        vm.modalMessage = "Are you sure?";

        activate();

        function activate() {
            vm.title = 'All stocks';
            getAll();
        }

        function getAll() {
            vm.tableItems = model.all();
        }

        function tableActions(current, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {

            vm.tableItems = Stocks.by(current, pageItems, filterBy, filterByFields, orderBy, orderByReverse);

            vm.tableItems.$then(function(_collection) {
                vm.totalItems = _collection.$metadata.totalItems;
                vm.pageItems = _collection.$metadata.limit;
                vm.currentPage = _collection.$metadata.pageNumber ? _collection.$metadata.pageNumber : 0;
            });
            logger.info('Stocks loaded');
        }

        function del(gridItem) {
            if (window.confirm(vm.modalMessage)) {
                model.del(gridItem.id);
                vm.tableItems.splice(vm.tableItems.indexOf(gridItem), 1);
            }
        }

    }
})();
