(function() {
    'use strict';

    angular.module('app.data')
        .factory('stocks.form', FormService);

    FormService.$inject = ['common', 'logger', '$state'];

    function FormService(common, logger, $state) {
        var service = {
            getValidationRules: getValidationRules
        };

        return service;

        function getValidationRules() {
            return {
                rules: {
                    code: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    name: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    recommendation: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    returnSince: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    sharePrice: {
                        required: {
                            rule: true,
                            message: 'Field phone is required'
                        }
                    },
                    price: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    dividind: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    lastYearReturn: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    marketCap: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    }
                },
                submit: function(data) {
                    var deferred = common.$q.defer();
                    common.$timeout(function() {
                        data.$save().$then(function() {
                                logger.success('form has been successfully saved');
                                $state.go('stocks.index');
                            },
                            function() {
                                return deferred.reject('Form validation failed');
                            });
                    }, 100);
                    return deferred.promise;
                }
            };
        }
    }
}());
