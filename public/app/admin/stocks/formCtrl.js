(function() {
    'use strict';

    angular
        .module('app.stocks')
        .controller('StocksFormCtrl', StocksFormCtrl);

    StocksFormCtrl.$inject = ['stocks.model', 'logger', 'stocks.form', '$stateParams'];

    function StocksFormCtrl(model, logger, FormService, $stateParams) {

        var vm = this;
        var stock = null;
        vm.formData = {};
        vm.submitFailed = submitFailed;
        vm.validationFailed = validationFailed;
        vm.regions = [{
            label: 'AU stock',
            value: 'au'
        }, {
            label: 'NZ stock',
            value: 'nz'
        }];

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.formData = model.edit($stateParams.id);
                vm.title = 'Edit stock';
            } else {
                vm.formData = model.create();
                vm.title = 'Create stock';
            }
            vm.validationRules = FormService.getValidationRules();
            vm.submit = vm.validationRules.submit;
        }

        function validationFailed() {
            return logger.error("validaton faliled");
        }

        function submitFailed(error) {
            logger.error(error);
        }
    }
})();
