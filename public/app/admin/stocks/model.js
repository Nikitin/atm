(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('stocks.model', StocksModel);

    StocksModel.$inject = ['restmod', 'logger', '$state'];

    function StocksModel(restmod, logger, $state) {
        var entity = restmod.model('/api/v1/stocks').mix({

            $config: {
                name: 'stock',
                plural: 'stocks'
            }
        });
        //var collection = stocks.$collection();

        var service = {
            all: all,
            by: by,
            del: del,
            edit: edit,
            create: create
        };

        return service;

        function all() {
            return entity.$search().$then(function(data) {
                logger.success('Resource loaded');
                return data;
            }, function() {
                logger.error('there is no stocks');
            });
        }

        function by(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
            var order = 'asc';
            if (!orderByReverse) {
                order = 'desc';
            }
            return entity.$search({
                page: currentPage,
                limit: pageItems,
                orderBy: orderBy,
                filterByFields: filterByFields,
                order: order
            });
        }

        function edit(id) {
            return entity.$find(id).$then(function(_data) {
                logger.success("Resource loaded");
                return _data;
            }, function(reason) {
                logger.error('Resource not found');
                $state.go('stocks.index');
            });
        }

        function create() {
            return entity.$build({
                'name': '',
                'code': '',
                'region': 'au',
                'recommendation': '',
                'returnSince': '',
                'sharePrice': '',
                'price': '',
                'dividend': '',
                'lastYearReturn': '',
                'marketCap': ''
            });
        }

        function del(id) {
            var model = entity.$find(id);
            model.$destroy().$then(function() {
                logger.info('Resource destroyed');
            }, function() {
                logger.error('Something went wrong');
            });
        }
    }
})();
