(function() {
    'use strict';

    angular
        .module('app.stocks', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];
    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'stocks': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'stocks.index': {
                url: '/stocks',
                templateUrl: '/app/admin/stocks/html/table.html',
                controller: 'StocksTableCtrl as vm',
                title: 'Stocks',
                parent: 'stocks',
                data: {
                    nav: 4,
                    pageTitle: 'Stocks',
                    pageHeader: {
                        icon: 'fa fa-circle',
                        title: 'Stocks',
                        subtitle: 'all stocks'
                    },
                    breadcrumbs: [{
                        title: 'Stocks'
                    }, {
                        title: 'All stocks'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/datatables/media/css/dataTables.bootstrap.min.css',
                                    pluginPath + '/datatables-responsive-helper/files/1.10/css/datatables.responsive.css',
                                    pluginPath + '/fuelux/dist/css/fuelux.min.css',
                                    pluginPath + '/v-accordion/dist/v-accordion.min.css'

                                ]
                            }, {
                                name: 'app.stocks',
                                files: [
                                    '/app/admin/stocks/tableCtrl.js'
                                ]
                            }, {
                                name: 'vAccordion',
                                files: [
                                    pluginPath + '/v-accordion/dist/v-accordion.min.js'
                                ]
                            }, {
                                name: 'ngclipboard',
                                files: [
                                    pluginPath + '/clipboard/dist/clipboard.min.js',
                                    pluginPath + '/ngclipboard/dist/ngclipboard.min.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/stocks/model.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'stocks.create': {
                url: '/stocks/create',
                templateUrl: '/app/admin/stocks/html/form.html',
                controller: 'StocksFormCtrl as vm',
                title: 'Stocks.Create',
                parent: 'stocks',
                data: {
                    nav: 0,
                    pageTitle: 'Stocks',
                    pageHeader: {
                        icon: 'fa fa-circle',
                        title: 'Stocks',
                        subtitle: 'create stock'
                    },
                    breadcrumbs: [{
                        title: 'Create'
                    }, {
                        title: 'Create new stock'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'app.stocks',
                                files: [
                                    '/app/admin/stocks/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/stocks/model.js',
                                    '/app/admin/stocks/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'stocks.edit': {
                url: '/stocks/:id/edit',
                templateUrl: '/app/admin/stocks/html/form.html',
                controller: 'StocksFormCtrl as vm',
                title: 'Edit stock',
                parent: 'stocks',
                data: {
                    nav: 0,
                    pageTitle: 'Stocks',
                    pageHeader: {
                        icon: 'fa fa-circle',
                        title: 'Stocks',
                        subtitle: 'edit stock'
                    },
                    breadcrumbs: [{
                        title: 'Edit'
                    }, {
                        title: 'Edit stock'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'app.stocks',
                                files: [
                                    '/app/admin/stocks/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/stocks/model.js',
                                    '/app/admin/stocks/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'stocks.files': {
                url: '/stocks/:id/files',
                templateUrl: '/app/admin/stocks/html/files.html',
                controller: 'StockFilesCtrl as vm',
                title: 'Stock files',
                parent: 'stocks',
                data: {
                    nav: 0,
                    pageTitle: 'Stock files',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Stocks',
                        subtitle: 'files stock'
                    },
                    breadcrumbs: [{
                        title: 'Files'
                    }, {
                        title: 'Stock files'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'app.stocks',
                                files: [
                                    '/app/admin/stocks/filesCtrl.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'stocks.image': {
                url: '/stocks/:id/image',
                templateUrl: '/app/admin/stocks/html/image.html',
                controller: 'StockImageCtrl as vm',
                title: 'Stock files',
                parent: 'stocks',
                data: {
                    nav: 0,
                    pageTitle: 'Stock files',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Stocks',
                        subtitle: 'files note'
                    },
                    breadcrumbs: [{
                        title: 'File'
                    }, {
                        title: 'Stock file'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/ng-img-crop/compile/minified/ng-img-crop.css'
                                ]
                            }, {
                                name: 'ngImgCrop',
                                files: [
                                    pluginPath + '/ng-img-crop/compile/minified/ng-img-crop.js'
                                ]
                            }, {
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'app.stocks',
                                files: [
                                    '/app/admin/stocks/imageCtrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
