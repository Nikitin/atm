(function() {
    'use strict';

    angular.module('app.data')
        .factory('users.form', FormService);

    FormService.$inject = ['common', 'logger', '$state'];

    function FormService(common, logger, $state) {
        var service = {
            getValidationRules: getValidationRules
        };

        return service;

        function getValidationRules() {
            return {
                rules: {
                    firstName: {
                        required: {
                            rule: true,
                            message: 'Name is required'
                        }
                    },
                    lastName: {
                        required: {
                            rule: true,
                            message: 'Name is required'
                        }
                    },
                    email: {
                        required: {
                            rule: true,
                            message: 'Email is required'
                        },
                        pattern: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    }
                },
                submit: function(data) {
                    var deferred = common.$q.defer();
                    common.$timeout(function() {
                        data.$save().$then(function() {
                                logger.success('form has been successfully saved');
                                return $state.go('users.index');
                            },
                            function() {
                                return deferred.reject('Form validation failed');
                            });
                    }, 100);
                    return deferred.promise;
                }
            };
        }
    }
}());
