(function() {
    'use strict';

    angular
        .module('app.users')
        .controller('UsersFormCtrl', UsersFormCtrl);

    UsersFormCtrl.$inject = ['users.model', 'logger', 'users.form', '$stateParams'];

    function UsersFormCtrl(model, logger, FormService, $stateParams) {
        var vm = this;
        var user = null;
        vm.listenFromOptions = [{
            label: 'Internet',
            value: 'internet'
        }, {
            label: 'Family',
            value: 'family'
        }, {
            label: 'Friends',
            value: 'friends'
        }];
        vm.statusOptions = [{
            label: 'Trial',
            value: 'trial'
        }, {
            label: 'Full 6 Months',
            value: 'full6'
        }, {
            label: 'Full 12 Months',
            value: 'full12'
        }, {
            label: 'Subscriber',
            value: 'subscriber'
        }, {
            label: 'Blocked',
            value: 'blocked'
        }, {
            label: 'Administrator',
            value: 'admin'
        }];
        vm.formData = {};
        vm.submitFailed = submitFailed;
        vm.validationFailed = validationFailed;

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.formData = model.edit($stateParams.id);
                vm.title = 'Edit user';
            } else {
                vm.formData = model.create();
                vm.title = 'Create user';
            }
            vm.validationRules = FormService.getValidationRules();
            vm.submit = vm.validationRules.submit;
        }

        function validationFailed() {
            return logger.error("validaton faliled");
        }

        function submitFailed(error) {
            logger.error(error);
        }
    }
})();
