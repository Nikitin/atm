(function() {
    'use strict';

    angular
        .module('app.users')
        .controller('UsersTableCtrl', UsersTableCtrl);

    UsersTableCtrl.$inject = ['users.model', 'logger'];

    function UsersTableCtrl(model, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.fields = ['id', 'firstName', 'lastName', 'email', 'mobilePhone', 'status', 'ends', 'isOnline', 'comment'];

        vm.del = del;
        vm.tableActions = tableActions;
        vm.getAll = getAll;
        vm.modalMessage = "Are you sure you want to DELETE this user?";

        activate();

        function activate() {
            vm.title = 'All users';
            getAll();
        }

        function getAll() {
            vm.tableItems = model.all();
        }

        function tableActions(currentUser, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
            vm.tableItems = Users.byUser(currentUser, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
            vm.tableItems.$then(function(_collection) {
                vm.totalItems = _collection.$metadata.totalItems;
                vm.pageItems = _collection.$metadata.limit;
                vm.currentPage = _collection.$metadata.pageNumber ? _collection.$metadata.pageNumber : 0;
            });
            logger.info('Users loaded');
        }

        function del(gridItem) {
            vm.modalMessage = "Are you sure you want to DELETE the user with ID " + gridItem.id + ": " + gridItem.firstName + " " + gridItem.lastName + "( "+ gridItem.email +" ) ? THIS CANNOT BE UNDONE!";

            if (window.confirm(vm.modalMessage)) {
                model.del(gridItem.id);
                vm.tableItems.splice(vm.tableItems.indexOf(gridItem), 1);
            }
        }
    }
})();
