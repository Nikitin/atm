(function() {
    'use strict';

    angular
        .module('app.users', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];
    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'users': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'users.index': {
                url: '/users',
                templateUrl: '/app/admin/users/html/table.html',
                controller: 'UsersTableCtrl as vm',
                title: 'Users',
                parent: 'users',
                data: {
                    nav: 11,
                    pageTitle: 'Users',
                    pageHeader: {
                        icon: 'fa fa-user',
                        title: 'Users',
                        subtitle: 'all users'
                    },
                    breadcrumbs: [{
                        title: 'Users'
                    }, {
                        title: 'All users'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/datatables/media/css/dataTables.bootstrap.min.css',
                                    pluginPath + '/datatables-responsive-helper/files/1.10/css/datatables.responsive.css',
                                    pluginPath + '/fuelux/dist/css/fuelux.min.css'
                                ]
                            }, {
                                name: 'app.users',
                                files: [
                                    '/app/admin/users/tableCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/users/model.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'users.create': {
                url: '/users/create',
                templateUrl: '/app/admin/users/html/form.html',
                controller: 'UsersFormCtrl as vm',
                title: 'Users.Create',
                parent: 'users',
                data: {
                    nav: 0,
                    pageTitle: 'Users',
                    pageHeader: {
                        icon: 'fa fa-user',
                        title: 'Users',
                        subtitle: 'create user'
                    },
                    breadcrumbs: [{
                        title: 'Create'
                    }, {
                        title: 'Create new user'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'app.users',
                                files: [
                                    '/app/admin/users/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/users/model.js',
                                    '/app/admin/users/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'users.edit': {
                url: '/users/:id/edit',
                templateUrl: '/app/admin/users/html/form.html',
                controller: 'UsersFormCtrl as vm',
                title: 'Edit user',
                parent: 'users',
                data: {
                    nav: 0,
                    pageTitle: 'Users',
                    pageHeader: {
                        icon: 'fa fa-user',
                        title: 'Users',
                        subtitle: 'edit user'
                    },
                    breadcrumbs: [{
                        title: 'Edit'
                    }, {
                        title: 'Edit user'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'app.users',
                                files: [
                                    '/app/admin/users/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/users/model.js',
                                    '/app/admin/users/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
