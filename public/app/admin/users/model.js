(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('users.model', UsersModel);

    UsersModel.$inject = ['restmod', 'logger', '$state'];

    function UsersModel(restmod, logger, $state) {
        var entity = restmod.model('/api/v1/users').mix({
            $config: {
                name: 'user',
                plural: 'users'
            }
        });
        //var collection = users.$collection();

        var service = {
            all: all,
            by: by,
            del: del,
            edit: edit,
            create: create
        };

        return service;

        function all() {
            return entity.$search().$then(function(data) {
                logger.success('Users loaded');
                return data;
            }, function() {
                logger.error('there is no users');
            });
        }

        function by(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
            var order = 'asc';
            if (!orderByReverse) {
                order = 'desc';
            }
            return entity.$search({
                page: currentPage,
                limit: pageItems,
                orderBy: orderBy,
                filterByFields: filterByFields,
                order: order
            });
        }

        function edit(id) {
            var model = entity.$find(id).$then(function(_data) {
                logger.success("User loaded");
                return _data;
            }, function(reason) {
                logger.error('Resource not found');
                $state.go('users.index');
            });
            return model;
        }

        function create() {
            return entity.$build({
                'firstName': '',
                'lastName': '',
                'email': '',
                'dayTimePhone': '',
                'mobilePhone': '',
                'postcode': '',
                'comment': '',
                'status': '',
                'listenFrom': '',
                'isOnline': '',
            });
        }

        function del(id) {
            var model = entity.$find(id);
            model.$destroy().$then(function() {
                logger.info('User destroyed');
            }, function() {
                logger.error('Something went wrong');
            });
        }
    }
})();
