(function() {
    'use strict';

    angular
        .module('app.concentrated', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'concentrated': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'concentrated.index': {
                url: '/concentrated',
                parent: 'concentrated',
                templateUrl: '/app/admin/concentrated/html/concentrated.html',
                controller: 'ConcentratedCtrl as vm',
                title: 'Member concentrated page',
                data: {
                    nav: 1,
                    pageTitle: 'concentrated',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'member concentrated page',
                        subtitle: 'member concentrated page'
                    },
                    breadcrumbs: [{
                        title: 'Member'
                    }, {
                        title: 'member concentrated page'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-xeditable/dist/css/xeditable.css',
                                    '/js/plugins/ckeditor/contents.css',
                                    pluginPath + '/v-accordion/dist/v-accordion.min.css'
                                ]
                            }, {
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'xeditable',
                                files: [
                                    pluginPath + '/angular-xeditable/dist/js/xeditable.js'
                                ]
                            }, {
                                name: 'vAccordion',
                                files: [
                                    pluginPath + '/v-accordion/dist/v-accordion.min.js'
                                ]
                            }, {
                                name: 'ngCkeditor',
                                files: [
                                    '/js/plugins/ckeditor/ckeditor.js',
                                    pluginPath + '/ng-ckeditor/ng-ckeditor.min.js',
                                ]
                            }, {
                                name: 'app.concentrated',
                                files: [
                                    '/app/admin/concentrated/ctrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
