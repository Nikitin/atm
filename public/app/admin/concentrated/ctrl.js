(function() {
    'use strict';
    angular.module('app.concentrated').controller('ConcentratedCtrl', ConcentratedCtrl);

    ConcentratedCtrl.$inject = ['logger', 'Upload', '$timeout', '$http', '$q'];

    function ConcentratedCtrl(logger, Upload, $timeout, $http, $q) {

        var vm = this;
        vm.getTable = getTable;
        vm.saveTable = saveTable;
        vm.resetTableData = resetTableData;
        vm.getText = getText;
        vm.saveText = saveText;
        vm.text = {};
        vm.editorOptions = {
            language: 'en',
            uiColor: '#000000'
        };

        activate();

        function activate() {
            resetTableData();
        }

        function getTable(name, type, count) {
            var path = '/api/v1/portfolio/table/' + name + '/' + type + '/' + count;
            $http.get(path)
                .success(function(data, status, headers, config) {
                    vm.tableData = data;
                })
                .error(function(data, status, headers, config) {
                    logger.error('table data not received');
                });
        }

        // mark user as deleted
        vm.deleteItem = function(item, index) {
            item.isDeleted = true;
            vm.tableData.splice(index, 1);

        };

        // add user
        vm.addItem = function(num) {
            var item = new Array(num);
            for (var i = 0; i < item.length; i++) {
                item[i] = ' ';
            }
            item.isNew = true;
            vm.tableData.push(item);
        };

        // cancel all changes
        vm.cancel = function() {
            for (var i = vm.tableData.length; i--;) {
                var item = vm.tableData[i];
                // undelete
                if (item.isDeleted) {
                    delete item.isDeleted;
                }
                // remove new
                if (item.isNew) {
                    vm.tableData.splice(i, 1);
                }
            }
        };

        // save edits
        function saveTable(name, type) {
            var path = '/api/v1/portfolio/table';
            $http.post(path, {
                    csv: vm.tableData,
                    name: name,
                    type: type
                })
                .success(function(data, status, headers, config) {
                    logger.success(data.message);
                })
                .error(function(data, status, headers, config) {
                    logger.error(data.message);
                });
        }

        function getText(type) {
            var path = '/api/v1/portfolio/text/' + type;
            $http.get(path)
                .success(function(data, status, headers, config) {
                    vm.text = data;
                })
                .error(function(data, status, headers, config) {
                    logger.error('text data not received');
                });
        }

        function saveText() {
            var path = '/api/v1/portfolio/text';
            $http.post(path, {
                    title: 'Concentrated',
                    text: vm.text.body,
                    topRight: vm.text.topRight,
                    topLeft: vm.text.topLeft,
                    type: 'Concentrated'
                })
                .success(function(data, status, headers, config) {
                    logger.success(data.message);
                })
                .error(function(data, status, headers, config) {
                    logger.error(data.message);
                });
        }

        function resetTableData(index) {
            vm.tableData = null;
        }
    }
})();
