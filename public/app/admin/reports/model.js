(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('reports.model', ReportsModel);

    ReportsModel.$inject = ['restmod', 'logger', '$state'];

    function ReportsModel(restmod, logger, $state) {
        var entity = restmod.model('/api/v1/reports').mix({

            $config: {
                name: 'report',
                plural: 'reports'
            }
        });
        //var collection = reports.$collection();

        var service = {
            all: all,
            by: by,
            del: del,
            edit: edit,
            create: create
        };

        return service;

        function all() {
            return entity.$search().$then(function(data) {
                logger.success('Resource loaded');
                return data;
            }, function() {
                logger.error('there is no reports');
            });
        }

        function by(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
            var order = 'asc';
            if (!orderByReverse) {
                order = 'desc';
            }
            return entity.$search({
                page: currentPage,
                limit: pageItems,
                orderBy: orderBy,
                filterByFields: filterByFields,
                order: order
            });
        }

        function edit(id) {
            return entity.$find(id).$then(function(_data) {
                logger.success("Resource loaded");
                return _data;
            }, function(reason) {
                logger.error('Resource not found');
                $state.go('reports.index');
            });
        }

        function create() {
            return entity.$build({
                'name': '',
                'author': '',
                'description': '',
                'topTrades': '',
                'monthly': '',
                'daily': '',
                'investor': ''
            });
        }

        function del(id) {
            var model = entity.$find(id);
            model.$destroy().$then(function() {
                logger.info('Resource destroyed');
            }, function() {
                logger.error('Something went wrong');
            });
        }
    }
})();
