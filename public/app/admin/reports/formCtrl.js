(function() {
    'use strict';

    angular
        .module('app.reports')
        .controller('ReportsFormCtrl', ReportsFormCtrl);

    ReportsFormCtrl.$inject = ['reports.model', 'logger', 'reports.form', '$stateParams'];

    function ReportsFormCtrl(model, logger, FormService, $stateParams) {

        var vm = this;
        var report = null;
        vm.formData = {};
        vm.submitFailed = submitFailed;
        vm.validationFailed = validationFailed;

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.formData = model.edit($stateParams.id);
                vm.title = 'Edit report';
            } else {
                vm.formData = model.create();
                vm.title = 'Create report';
            }
            vm.validationRules = FormService.getValidationRules();
            vm.submit = vm.validationRules.submit;
        }

        function validationFailed() {
            return logger.error("validaton faliled");
        }

        function submitFailed(error) {
            logger.error(error);
        }
    }
})();
