(function() {
    'use strict';

    angular.module('app.data')
        .factory('reports.form', FormService);

    FormService.$inject = ['common', 'logger', '$state'];

    function FormService(common, logger, $state) {
        var service = {
            getValidationRules: getValidationRules
        };

        return service;

        function getValidationRules() {
            return {
                rules: {
                    name: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    author: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    },
                    description: {
                        required: {
                            rule: true,
                            message: 'Field is required'
                        }
                    }
                },
                submit: function(data) {
                    var deferred = common.$q.defer();
                    common.$timeout(function() {
                        data.$save().$then(function() {
                                logger.success('form has been successfully saved');
                                $state.go('reports.index');
                            },
                            function() {
                                return deferred.reject('Form validation failed');
                            });
                    }, 100);
                    return deferred.promise;
                }
            };
        }
    }
}());
