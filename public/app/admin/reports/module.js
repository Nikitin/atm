(function() {
    'use strict';

    angular
        .module('app.reports', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];
    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'reports': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'reports.index': {
                url: '/reports',
                templateUrl: '/app/admin/reports/html/table.html',
                controller: 'ReportsTableCtrl as vm',
                title: 'Reports',
                parent: 'reports',
                data: {
                    nav: 4,
                    pageTitle: 'Reports',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Reports',
                        subtitle: 'all reports'
                    },
                    breadcrumbs: [{
                        title: 'Reports'
                    }, {
                        title: 'All reports'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    //pluginPath + '/datatables/media/css/dataTables.bootstrap.min.css',
                                    //pluginPath + '/datatables-responsive-helper/files/1.10/css/datatables.responsive.css',
                                    //pluginPath + '/fuelux/dist/css/fuelux.min.css',
                                    //pluginPath + '/v-accordion/dist/v-accordion.min.css'

                                ]
                            }, {
                                name: 'app.reports',
                                files: [
                                    '/app/admin/reports/tableCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/reports/model.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'reports.create': {
                url: '/reports/create',
                templateUrl: '/app/admin/reports/html/form.html',
                controller: 'ReportsFormCtrl as vm',
                title: 'Reports.Create',
                parent: 'reports',
                data: {
                    nav: 0,
                    pageTitle: 'Reports',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Reports',
                        subtitle: 'create report'
                    },
                    breadcrumbs: [{
                        title: 'Create'
                    }, {
                        title: 'Create new report'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'app.reports',
                                files: [
                                    '/app/admin/reports/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/reports/model.js',
                                    '/app/admin/reports/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'reports.edit': {
                url: '/reports/:id/edit',
                templateUrl: '/app/admin/reports/html/form.html',
                controller: 'ReportsFormCtrl as vm',
                title: 'Edit report',
                parent: 'reports',
                data: {
                    nav: 0,
                    pageTitle: 'Reports',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Reports',
                        subtitle: 'edit report'
                    },
                    breadcrumbs: [{
                        title: 'Edit'
                    }, {
                        title: 'Edit report'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'app.reports',
                                files: [
                                    '/app/admin/reports/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/reports/model.js',
                                    '/app/admin/reports/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'reports.files': {
                url: '/reports/:id/files',
                templateUrl: '/app/admin/reports/html/files.html',
                controller: 'ReportFilesCtrl as vm',
                title: 'Report files',
                parent: 'reports',
                data: {
                    nav: 0,
                    pageTitle: 'Report files',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Reports',
                        subtitle: 'files report'
                    },
                    breadcrumbs: [{
                        title: 'Files'
                    }, {
                        title: 'Report files'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'app.reports',
                                files: [
                                    '/app/admin/reports/filesCtrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
