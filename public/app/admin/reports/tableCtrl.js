(function() {
    'use strict';

    angular
        .module('app.reports')
        .controller('ReportsTableCtrl', ReportsTableCtrl);

    ReportsTableCtrl.$inject = ['reports.model', 'logger'];

    function ReportsTableCtrl(model, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.fields = ['id', 'name', 'author', 'topTrades', 'monthly', 'daily', 'investor', 'created_at', 'updated_at'];
        vm.del = del;
        vm.tableActions = tableActions;
        vm.getAll = getAll;
        vm.modalMessage = "Are you sure?";

        activate();

        function activate() {
            vm.title = 'All reports';
            getAll();
        }

        function getAll() {
            vm.tableItems = model.all();
        }

        function tableActions(current, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {

            vm.tableItems = Reports.by(current, pageItems, filterBy, filterByFields, orderBy, orderByReverse);

            vm.tableItems.$then(function(_collection) {
                vm.totalItems = _collection.$metadata.totalItems;
                vm.pageItems = _collection.$metadata.limit;
                vm.currentPage = _collection.$metadata.pageNumber ? _collection.$metadata.pageNumber : 0;
            });
            logger.info('Reports loaded');
        }

        function del(gridItem) {
            if (window.confirm(vm.modalMessage)) {
                model.del(gridItem.id);
                vm.tableItems.splice(vm.tableItems.indexOf(gridItem), 1);
            }
        }

    }
})();
