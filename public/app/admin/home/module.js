(function() {
    'use strict';

    angular
        .module('app.home', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'home': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'home.index': {
                url: '/home',
                parent: 'home',
                templateUrl: '/app/admin/home/home.html',
                controller: 'HomeCtrl as vm',
                title: 'Member home page',
                data: {
                    nav: 1,
                    pageTitle: 'home',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'member home page',
                        subtitle: 'member home page'
                    },
                    breadcrumbs: [{
                        title: 'Member'
                    }, {
                        title: 'member home page'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-xeditable/dist/css/xeditable.css'
                                ]
                            }, {
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'xeditable',
                                files: [
                                    pluginPath + '/angular-xeditable/dist/js/xeditable.js'
                                ]
                            }, {
                                name: 'app.home',
                                files: [
                                    '/app/admin/home/ctrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/home/model.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
