(function() {
    'use strict';
    angular.module('app.home').controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['logger', 'home.model', 'Upload', '$timeout', '$http', '$filter', '$q'];

    function HomeCtrl(logger, model, Upload, $timeout, $http, $filter, $q) {

        var vm = this;
        vm.getTopTradesTable = getTopTradesTable;
        vm.uploadFile = uploadFile;
        vm.uploadStocksFile = uploadStocksFile;
        vm.saveTopTradesTable = saveTopTradesTable;
        vm.data = {};

        activate();

        function activate() {
            getTopTradesTable();
        }

        function uploadFile(file) {
            file.upload = Upload.upload({
                url: '/api/v1/home/upload',
                data: {
                    file: file
                },
            });

            file.upload.then(function(response) {
                $timeout(function() {
                    file.result = response.data;
                    logger.info(response.data.message);
                });
            }, function(response) {
                if (response.status > 0)
                    vm.errorMsg = response.status + ': ' + response.data.message;
            }, function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }

        function uploadStocksFile(file) {
            file.upload = Upload.upload({
                url: '/api/v1/home/uploadStocks',
                data: {
                    file: file
                },
            });

            file.upload.then(function(response) {
                $timeout(function() {
                    file.result = response.data;
                    logger.info(response.data.message);
                });
            }, function(response) {
                if (response.status > 0)
                    vm.stocksErrorMsg = response.status + ': ' + response.data.message;
            }, function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }

        function getTopTradesTable() {
            $http.get("/api/v1/home/topTradesTable")
                .success(function(data, status, headers, config) {
                    vm.topTrades = data;
                })
                .error(function(data, status, headers, config) {
                    logger.error('top trades not received');
                });
        }

        // mark user as deleted
        vm.deleteItem = function(item, index) {
            item.isDeleted = true;
            vm.topTrades.splice(index, 1);
        };

        // add user
        vm.addItem = function() {
            var item = new Array(3);
            for (var i = 0; i < item.length; i++) {
                item[i] = '';
            }
            item.isNew = true;
            vm.topTrades.push(item);
        };

        // cancel all changes
        vm.cancel = function() {
            for (var i = vm.topTrades.length; i--;) {
                var item = vm.topTrades[i];
                // undelete
                if (item.isDeleted) {
                    delete item.isDeleted;
                }
                // remove new
                if (item.isNew) {
                    vm.topTrades.splice(i, 1);
                }
            }
        };

        // save edits
        function saveTopTradesTable() {
            $http.post('/api/v1/home/topTradesTable', vm.topTrades);
        }
    }
})();
