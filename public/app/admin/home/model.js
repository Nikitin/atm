(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('home.model', HomeModel);

    HomeModel.$inject = ['$http', 'logger', 'common', '$timeout'];

    function HomeModel($http, logger, common, $timeout) {

        var service = {
            getTopTradesTable: getTopTradesTable
        };

        return service;

        function getTopTradesTable() {
            var result;
            $http.get("/api/v1/home/topTradesTable")
                .success(function(data, status, headers, config) {
                    console.log(data[0][1]);
                    result = data;
                })
                .error(function(data, status, headers, config) {
                    logger.error('top trades not received');
                });
            /*$timeout(function() {
            }, 500);*/
            return result;
        }
    }
})();
