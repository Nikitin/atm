(function() {
    'use strict';

    angular.module('app.notes').controller('NotesFormCtrl', NotesFormCtrl);

    NotesFormCtrl.$inject = ['notes.model', 'logger', 'notes.form', '$stateParams'];

    function NotesFormCtrl(model, logger, FormService, $stateParams) {
        var vm = this;
        vm.formData = {};
        vm.submitFailed = submitFailed;
        vm.validationFailed = validationFailed;
        //vm.handleFileSelect = handleFileSelect;
        vm.editorOptions = {
            language: 'en',
            uiColor: '#000000'
        };
        vm.myImage = '';
        vm.myCroppedImage = '';

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.formData = model.edit($stateParams.id);
                vm.title = 'Edit note';
            } else {
                vm.formData = model.create();
                vm.title = 'Create note';
            }
            vm.validationRules = FormService.getValidationRules();
            vm.submit = vm.validationRules.submit;
        }

        function validationFailed() {
            return logger.error("validaton faliled");
        }

        function submitFailed(error) {
            logger.error(error);
        }

        /*function handleFileSelect(evt) {
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.myImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        }
        angular.element(document.querySelector('#fileInput')).on('change', vm.handleFileSelect);*/
    }
})();
