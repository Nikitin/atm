(function() {
    'use strict';

    angular.module('app.data').factory('notes.form', FormService);

    FormService.$inject = ['common', 'logger', '$state'];

    function FormService(common, logger, $state) {
        var service = {
            getValidationRules: getValidationRules
        };

        return service;

        function getValidationRules() {
            return {
                rules: {
                    title: {
                        required: {
                            rule: true,
                            message: 'Title is required'
                        }
                    },
                    quote: {
                        required: {
                            rule: true,
                            message: 'Quote is required'
                        }
                    }
                },
                submit: function(data) {
                    var deferred = common.$q.defer();
                    common.$timeout(function() {
                        data.$save().$then(function() {
                                logger.success('form has been successfully saved');
                                return $state.go('notes.index');
                            },
                            function() {
                                return deferred.reject('Form validation failed');
                            });
                    }, 100);
                    return deferred.promise;
                }
            };
        }
    }
}());
