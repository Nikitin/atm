(function() {
    'use strict';

    angular.module('app.notes').controller('NoteFilesCtrl', NoteFilesCtrl);

    NoteFilesCtrl.$inject = ['Upload', 'logger', '$stateParams', '$timeout', '$state', '$http'];

    function NoteFilesCtrl(Upload, logger, $stateParams, $timeout, $state, $http) {
        var vm = this;
        vm.upload = upload;
        vm.get = get;
        vm.del = del;
        vm.initialize = initialize;
        vm.croppedDataUrl = '';
        vm.picFile = '';
        vm.modalMessage = 'are you sure?';

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.title = 'File for note';
                //get($stateParams.id);
            } else {
                logger.error('there is no such path');
                $state.go('notes.index');
            }
        }

        function get(id) {
            var path = '/api/v1/notes/image/' + id;
            $http.get(path)
                .success(function(data, status, headers, config) {
                    if (data.data !== '') {
                        vm.croppedDataUrl = data.data;
                        vm.picFile = Upload.dataUrltoBlob(data.data);
                    }
                })
                .error(function(data, status, headers, config) {
                    vm.croppedDataUrl = null;
                    logger.error('file for this note not loaded');
                });
        }

        function upload(dataUrl) {
            Upload.upload({
                url: '/api/v1/notes/image/' + $stateParams.id,
                data: {
                    file: Upload.dataUrltoBlob(dataUrl)
                },
            }).then(function(response) {
                $timeout(function() {
                    vm.result = response.data;
                });
            }, function(response) {
                if (response.status > 0) vm.errorMsg = response.status + ': ' + response.data;
            }, function(evt) {
                vm.progress = parseInt(100.0 * evt.loaded / evt.total);
            });
        }

        function del(id) {
            if (window.confirm(vm.modalMessage)) {
                var path = '/api/v1/notes/image/' + id;
                $http.delete(path)
                    .success(function(data, status) {
                        logger.info('file deleted');
                    })
                    .error(function(data, status) {
                        logger.error('file not deleted');
                    });
            }
        }

        function initialize() {
            return get($stateParams.id);
        }
    }
})();
