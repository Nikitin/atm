(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('notes.model', UsersModel);

    UsersModel.$inject = ['restmod', 'logger', '$state'];

    function UsersModel(restmod, logger, $state) {
        var entity = restmod.model('/api/v1/notes').mix({
            $config: {
                name: 'note',
                plural: 'notes'
            }
        });
        //var collection = notes.$collection();

        var service = {
            all: all,
            by: by,
            del: del,
            edit: edit,
            create: create
        };

        return service;

        function all() {
            return entity.$search().$then(function(data) {
                logger.success('Resource loaded');
                return data;
            }, function() {
                logger.error('there is no notes');
            });
        }

        function by(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
            var order = 'asc';
            if (!orderByReverse) {
                order = 'desc';
            }
            return entity.$search({
                page: currentPage,
                limit: pageItems,
                orderBy: orderBy,
                filterByFields: filterByFields,
                order: order
            });
        }

        function edit(id) {
            var model = entity.$find(id).$then(function(_data) {
                logger.success("Resource loaded");
                return _data;
            }, function(reason) {
                logger.error('Resource not found');
                $state.go('notes.index');
            });
            return model;
        }

        function create() {
            return entity.$build({
                'title': '',
                'pick': '',
                'ytd': '',
                'stockLink': '',
                'quote': '',
                'body': '',
            });
        }

        function del(id) {
            var model = entity.$find(id);
            model.$destroy().$then(function() {
                logger.info('Resource destroyed');
            }, function() {
                logger.error('Something went wrong');
            });
        }
    }
})();
