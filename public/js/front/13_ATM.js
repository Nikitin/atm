$(document).ready(function () {
    enableSetEqualHeight();


    // Global Variables
    window_w = $(window).width();
    window_h = $(window).height();
    window_s = $(window).scrollTop();

    $html = $('html');
    $body = $('body');

    if (history.pushState)
        var ionic_current_path = location.pathname;

    // Window Load and Resize
    $(window).bind('resize load', function () {

        window_w = $(window).width();
        window_h = $(window).height();
        window_s = $(window).scrollTop();

    });

    // Window Scroll
    $(window).scroll(function () {

        window_s = $(window).scrollTop();

    });

    enableLabelauty();
    enableChangeSignTable();
    enableSearchBox();
    enableWOWAnimate();
    enableTooltips();
    enableTextareaResize();
    enableBackToTop();
    menuVerticalHeight();
    enablesetTotal();
    enableHightChartsAUPortfolio();
    enableHightChartsNZPortfolio();
    highchartsPieAuPortfolio();
    highchartsPieNzPortfolio();
    enableHightChartsGtdPricesRecovering();
    EnableSelectClick();
    EnableSortTable();
    EnableSortDate();
    getBrowser();
    motifsSumAndCharVal();
    tableRubberPortfolio();
    /*    modalVerifyPhone();
     modalVerifyPhonePopUp();
     modalVerifyPhoneResetCode();*/
    registerLog();

});



$(window).load(function () {
    layoutTable();
});


function registerLog() {
    $('body').on('submit', '#payment-form', function () {
        $.post("/register/log", {
            _token: $('meta[name="csrf-token"]').attr('content'),
            name: $("#payment-form input[name=firstName]").val(),
            email: $("#payment-form input[name=email]").val(),
            mobilePhone: $("#payment-form input[name=mobilePhone]").val()
        });
    });
}

function modalVerifyPhone() {
    $('body').on('submit', '#payment-form', function (e) {
        e.preventDefault();
        var $form = $(this);
        //var correctCode = '1111';

        if ($('#payment-form .prettyforms-validation-error').length == 0) {

            //get code for sms
            sendCode($form);

            $(".modal-content h3").html('We have sent a code to your mobile phone, it will be there shortly. Please enter the code after you receive it.<br><br> <small>If you have any problems with entering the code please call: <br> <a title="+64278783600" href="tel:+64 27 878 3600">+64 27 878 3600</a> (New Zealand) or <a title="1800026778" href="tel:1800026778">1800-026-778</a> (toll free Australia).</small>');
            $(".modal-content form > *").remove();
            $(".modal-content form").attr('id', 'verify-phone').attr('action', '/').attr('novalidate', 'novalidate').append('<div class="form_group"><input type="text" name="codeVerify" id="codeVerify" value="" placeholder="Code"></div><input class="button-form" id="sendVerifyCode" type="submit" value="Send"><input class="button-form" type="button" id="changePhone" value="Change Phone"><input class="button-form" type="button" id="resendCode" value="Resend Code">');
            $('#codeVerify').attr('data-validation', 'notempty;minlength:6;maxlength:6');

            if ($('#verify-phone').length) {
                $('.modal').addClass('verify-modal');
            }

            $('#atm-modal').modal('show');

            $('#changePhone').on('click', function () {
                $('#atm-modal').modal('hide');
                $('#payment-form input[name="mobilePhone"]').val('').focus();
            });
        }
        else {
            alert("ERROR WHILE REGISTERING");
            $.post("/register/log", {
                name: $("#payment-form input[name=firstName]").val(),
                email: $("#payment-form input[name=email]").val(),
                mobilePhone: $("#payment-form input[name=mobilePhone]").val()
            });
        }
    });

}

function modalVerifyPhonePopUp() {
    $('body').on('submit', '#verify-phone', function (e) {
        e.preventDefault();

        if ($('#verify-phone .prettyforms-validation-error').length == 0) {
            $.post("/twilio/check", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    code: $("#verify-phone input[name=codeVerify]").val(),
                    mobilePhone: $("#payment-form input[name=mobilePhone]").val()
                },
                function (data) {
                    if (data.status == 'error') {
                        $(".modal-content h3").html(data.message);
                    } else if (data.status == 'ok') {
                        $("#register-form input[name=mobilePhone]").val($("#payment-form input[name=mobilePhone]").val());
                        $("#register-form input[name=firstName]").val($("#payment-form input[name=firstName]").val());
                        $("#register-form input[name=email]").val($("#payment-form input[name=email]").val());
                        $("#register-form input[name=password]").val($("#payment-form input[name=password]").val());
                        $("#register-form").submit();
                    }
                });
        }

        return false
    });
}

function modalVerifyPhoneResetCode() {
    $('body').on('click', '#verify-phone #resendCode', function (e) {
        var input = this;
        input.disabled = true;
        setTimeout(function () {
            input.disabled = false;
        }, 9000);
        return sendCode();
    });
}

function sendCode() {
    return $.post("/twilio/call", {
            _token: $('meta[name="csrf-token"]').attr('content'),
            mobilePhone: $("#payment-form input[name=mobilePhone]").val(),
            firstName: $("#payment-form input[name=firstName]").val()
        },
        function (data) {
            console.log(data);
        });
}


function layoutTable() {
    var trial = $('.portfolio-center-column-container[data-trial="true"]');

    if ($(trial.length)) {
        var h = $(trial).find('.table-responsive').height() - $(trial).find('.table-responsive thead').height();
        $(trial).find('.table-responsive').append('<div data-wow-offset="" class="layout-table wow fadeInCenter-one"><h2 data-wow-offset="0" class="wow fadeInCenter-two">&#10004;  To see the full ATM portfolio, <a href="/member/renew">upgrade your subscription</a> to the full member status.</h2></div>');
        $('.layout-table').attr('data-wow-offset', '-' + h);
    }
}

function tableRubberPortfolio() {
    $('.portfolio-center-column-container > table').each(function () {
        $(this).wrap('<div class="table-responsive"></div>');
    });
}

function motifsSumAndCharVal() {
    $('.accordion-table').each(function () {
        var sum = 0;
        var n = 0;

        $(this).find('tbody tr td:last-child').each(function () {

            sum += parseFloat($(this).text());

            if ($(this).text().charAt(0).indexOf('-') == 0) {
                $(this).toggleClass('table-negative-value');
                $(this).html('<span class="char-negative">-</span>' + $(this).text().replace(/^-/, ''));
            }

            $(this).parents('td[colspan="4"]').find('.motifs-table-separate thead tr th:last-child').text(sum.toFixed(1) + ' %');
            n++;
            if ($(this).parents('tr').length + 1 == n) {
                sum = 0;
                n = 0;
            }
        })

    });

    $('.motifs-table-separate').find('thead > tr > th:last-child').each(function () {
        if ($(this).text().charAt(0).indexOf('-') == 0) {
            $(this).toggleClass('table-negative-value');
            $(this).html('<span class="char-negative">-</span>' + $(this).text().replace(/^-/, ''));
        }
    });

    $('.separate-motifs-value li > span').each(function () {
        if ($(this).text().charAt(0).indexOf('-') == 0) {
            $(this).toggleClass('table-negative-value');
            $(this).html('<span class="char-negative">-</span>' + $(this).text().replace(/^-/, ''));
        }
    })

}


function EnableSortTable() {
    $('.tablesorter').each(function () {
        $('.false-sort-first-column').tablesorter({
            headers: {
                0: {
                    sorter: false
                }
            }
        });
        $('.topTradesBlock').tablesorter({});

        $('.perfomance-table').tablesorter({});
    });
}

function menuVerticalHeight() {

    $('.menu-vertical .menu--item__has_sub_menu').on('click', function () {
        var sum = 0;
        $('.menu-vertical .menu--item').each(function () {
            heightMenu = $(this).height();
            sum += heightMenu;
            $('.menu-vertical, .vertical_nav').height(sum + 20);
        });

    })

}


function EnableSortDate() {
    $('.reports-archive-table').each(function () {
        $('.reports-archive-table').tablesorter({
            dateFormat: "mmddyyyy",
            headers: {
                0: {sorter: "shortDate", dateFormat: "ddmmyyyy"},
                1: {
                    sorter: true
                }
            }

        });

    });
}


function EnableSelectClick() {
    $('.profile-form .form_group select').on('click', function () {
        $(this).toggleClass('arrow-up');
    });
}

function getBrowser() {
    var BrowserDetect = {
        init: function () {
            this.browser = this.searchString(this.dataBrowser) || "Other";
        },
        searchString: function (data) {
            for (var i = 0; i < data.length; i++) {
                var dataString = data[i].string;

                if (dataString.indexOf(data[i].subString) !== -1) {
                    return data[i].identity;
                }
            }
        },


        dataBrowser: [
            {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
            {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
            {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
            {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
            {string: navigator.userAgent, subString: "Opera", identity: "Opera"},
            {string: navigator.userAgent, subString: "OPR", identity: "Opera"},

            {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
            {string: navigator.userAgent, subString: "Safari", identity: "Safari"}
        ]
    };

    BrowserDetect.init();

    if (BrowserDetect.browser.search('Explorer') == 0) {
        $('html').addClass('ie')
    } else if (BrowserDetect.browser.search('Firefox') == 0) {
        $('html').addClass('ff')
    } else if (BrowserDetect.browser.search('MS Edge') == 0) {
        $('html').addClass('edge');
    }
    ;

}

function enableTooltips() {
    $("[data-toggle='tooltip']").tooltip();
};


function enableTextareaResize() {
    $('textarea').autoResize({
            animateDuration: 300,
            extraSpace: 0
        }
    );
};


function enableSearchBox() {

    $('.search_box a').click(function () {
        $('.search_box').toggleClass('search_box_opened');
    });

    $(document).click(function (e) {
        if (!$('.search_box a, .search_box *').is(e.target)) {
            $('.search_box').removeClass('search_box_opened');
        }
    });


};


function enableWOWAnimate() {
    wow = new WOW(
        {
            boxClass: 'wow',      // default
            animateClass: 'animated', // default
            offset: 150,          // default
            mobile: true,       // default
            live: true        // default
        }
    )
    wow.init();
};


function enableBackToTop() {

    $(window).scroll(function () {
        var offset = $(document).height() - window_h - 500;
        if ($('.footer').length)
            offset = $('.footer').offset().top - window_h;

        if (window_s > offset && window_w > 767) {
            $('.back-to-top').fadeIn(400);
        } else {
            $('.back-to-top').fadeOut(400);
        }
    });


};


$('.back-to-top').on('click', function (e) {
    $('html,body').stop().animate({scrollTop: $('#anchor-landmark').offset().top}, 1000);
    e.preventDefault();
});


function enableSetEqualHeight() {
    var mq = window.matchMedia("(min-width: 767px)");
    if (mq.matches) {
        $(function () {
            $('.what-sets-container .what-sets-column').matchHeight();
            $('.products-container figure').matchHeight();
            $('.description_main_image > a > h4').css('font-size', '23px').css('line-height', '1.5');
            $('.description_main_image > a').matchHeight();
            $('.optional-time').matchHeight();
        });
    }

};

var setTotal = function (tableSelector, colsIndexes) {
    var sums = {};

    $(tableSelector).find('tbody > tr').each(function () {
        for (var i = 0; i < colsIndexes.length; i++) {
            if (!sums[colsIndexes[i]]) {
                sums[colsIndexes[i]] = 0;
            }

            sums[colsIndexes[i]] += parseFloat($(this).find('td').eq(colsIndexes[i]).text());
        }
    });


    for (i = 0; i < colsIndexes.length; i++) {

        $(tableSelector + ' tr:last td').eq(colsIndexes[i]).text(sums[colsIndexes[i]]);

        $(tableSelector + ' tr:last td').eq(colsIndexes[i]).text(Math.round((sums[colsIndexes[i]]) * 100) / 100);

    }
}

function enablesetTotal() {
    setTotal('.portfolio-breakdown-table-container .top-ten-holdings', [1, 2]);
    setTotal('.portfolio-breakdown-table-container .sector-breakdown', [1, 2]);
    setTotal('.portfolio-breakdown-table-container .regional-breakdown', [1, 2]);


    //setTotal('.portfolio-breakdown-table-container .perfomance-table', [1,2,3]);

    if ($('.perfomance-table tfoot td').length = !0) {
        $('.perfomance-table tfoot td:not(:first-child)').append('%');
    }
};


function enableChangeSignTable() {


    $('.members_info_container .member_home_table tbody td:last-child').each(function () {


        if ($(this).html()[0].indexOf('-') != -1) {
            $(this).toggleClass('negative-value');
        }
        else {
            $(this).toggleClass('positive-value');
        }

    });

}

function enableLabelauty() {
    $(":checkbox, :radio").labelauty({
        label: false
    });
};


function enableHightChartsGtdPricesRecovering() {

    if ($('#homePageChart').length) {
        var chart;
        var jsonString = document.getElementById('homePageChart').getAttribute('data-csv');

        var json = $.parseJSON(jsonString).slice(1);
        var title = $.parseJSON(jsonString).shift()[0];
        var titleAxe = $.parseJSON(jsonString).shift()[1].split('--');


        var options = {
            plotOptions: {
                spline: {
                    lineWidth: 3,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 2,
                                lineWidth: 1,

                            }
                        }
                    }
                }
            },
            chart: {
                renderTo: 'container_gtd_prices_recovering',
                type: 'line',
            },
            title: {
                text: title,
                style: {
                    "fontSize": "18px",
                    "fontFamily": "Open Sans"
                }
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: titleAxe[1]
                }
            },
            series: [{
                name: titleAxe[0],
                data: []
            }]
        };


        val1 = [];

        $.each(json, function (key, value) {

            if ($(this).length != 1) {
                val1.push([Date.parse(value[0]), parseFloat(value[1])]);
            }

        });
        options.series[0].data = val1;

        chart = new Highcharts.Chart(options);
    }

}


function enableHightChartsAUPortfolio() {

    if ($('#auPortfolioChart').length) {

        var chart;
        var jsonString = document.getElementById('auPortfolioChart').getAttribute('data-csv');
        var json = jQuery.parseJSON(jsonString);


        var options = {

            chart: {
                renderTo: 'container-auportfolio',
                type: 'line'
            },
            title: {
                text: 'Portfolio Performance',
                style: {
                    "fontSize": "18px",
                    "fontFamily": "Open Sans"
                }
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'ATM AU Portfolio',
                data: []
            }, {
                name: 'Benchmark',
                data: []
            }]
        };

        val1 = [];
        val2 = [];

        $.each(json, function (key, value) {

            if ($(this).length != 1) {
                val1.push([Date.parse(value[0]), parseFloat(value[1])]);
                val2.push([Date.parse(value[0]), parseFloat(value[2])]);
            }

        });
        options.series[0].data = val1;
        options.series[1].data = val2;

        chart = new Highcharts.Chart(options);

    }
}


function enableHightChartsNZPortfolio() {

    if ($('#nzPortfolioChart').length) {

        var chart;
        var jsonString = document.getElementById('nzPortfolioChart').getAttribute('data-csv');
        var json = jQuery.parseJSON(jsonString);


        var options = {
            plotOptions: {
                spline: {
                    lineWidth: 3,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 2,
                                lineWidth: 1,

                            }
                        }
                    }
                }
            },
            chart: {
                renderTo: 'container-nzportfolio',
                type: 'line',
            },
            title: {
                text: 'Portfolio Performance',
                style: {
                    "fontSize": "18px",
                    "fontFamily": "Open Sans"
                }
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'ATM NZ Portfolio',
                data: []
            }, {
                name: 'Benchmark',
                data: []
            }]
        };

        val1 = [];
        val2 = [];

        $.each(json, function (key, value) {

            if ($(this).length != 1) {
                val1.push([Date.parse(value[0]), parseFloat(value[1])]);
                val2.push([Date.parse(value[0]), parseFloat(value[2])]);
            }

        });
        options.series[0].data = val1;
        options.series[1].data = val2;

        chart = new Highcharts.Chart(options);

    }
}

function pieChartVisualize() {
    Highcharts.visualize = function (data, options)
    {

        var cvs = JSON.parse(data);
        var name = Array();
        var dot = Array();

        options.xAxis.categories = [];
        options.series['0'].data = [];


        for(i=0;i<cvs.length;i++) {
            name[i] = cvs[i][0];
            dot[i] = cvs[i][1];
            options.series['0'].data.push({name: name[i], y: parseFloat(dot[i])});

        }

        var chart = new Highcharts.Chart(options);
    };
}


function highchartsPieAuPortfolio() {
    if ($('#au-thematic-expsoure').length || $('#au-industry-weightings').length){
        pieChartVisualize();

        var auThematicExpsoure = $('#au-thematic-expsoure').attr('data-csv');
        var auIndustryWeightings = $('#au-industry-weightings').attr('data-csv');

        options1 = {
            chart: {
                renderTo: 'au-thematic-expsoure',
                type: 'pie'
            },
            title: {
                text: 'AU Portfolio Thematic Exposure'
            },
            xAxis: {
            },
            yAxis: {
                title: {
                    text: 'Units'
                }
            },
            tooltip:
            {
                formatter: function()
                {
                    return this.y +' '+ this.point.name;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        color: '#000000',
                        connectorColor: '#000000'
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '',
                colorByPoint: true
            }]
        };

        options2 = {
            chart: {
                renderTo: 'au-industry-weightings',
                type: 'pie'
            },
            title: {
                text: 'AU Portfolio Industry Weightings'
            },
            xAxis: {
            },
            yAxis: {
                title: {
                    text: 'Units'
                }
            },
            tooltip:
            {
                formatter: function()
                {
                    return this.y +' '+ this.point.name;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        color: '#000000',
                        connectorColor: '#000000'
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '',
                colorByPoint: true
            }]
        };

        Highcharts.visualize(auThematicExpsoure, options1);
        Highcharts.visualize(auIndustryWeightings, options2);
    }

}

function highchartsPieNzPortfolio() {

    if ($('#nz-thematic-expsoure').length || $('#nz-industry-weightings').length){
        pieChartVisualize();

        var nzThematicExpsoure = $('#nz-thematic-expsoure').attr('data-csv');
        var nzIndustryWeightings = $('#nz-industry-weightings').attr('data-csv');


        options1 = {
            chart: {
                renderTo: 'nz-thematic-expsoure',
                type: 'pie'
            },
            title: {
                text: 'NZ Portfolio Thematic Exposure'
            },
            xAxis: {
            },
            yAxis: {
                title: {
                    text: 'Units'
                }
            },
            tooltip:
            {
                formatter: function()
                {
                    return this.y +' '+ this.point.name;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        color: '#000000',
                        connectorColor: '#000000'
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '',
                colorByPoint: true
            }]
        };

        options2 = {
            chart: {
                renderTo: 'nz-industry-weightings',
                type: 'pie'
            },
            title: {
                text: ' NZ Portfolio Industry Weightings'
            },
            xAxis: {
            },
            yAxis: {
                title: {
                    text: 'Units'
                }
            },
            tooltip:
            {
                formatter: function()
                {
                    return this.y +' '+ this.point.name;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        color: '#000000',
                        connectorColor: '#000000'
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '',
                colorByPoint: true
            }]
        };

        Highcharts.visualize(nzThematicExpsoure, options1);
        Highcharts.visualize(nzIndustryWeightings, options2);
    }

}











	    
		
		