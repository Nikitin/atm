<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Note;

/**
 * Class PostTransformer
 * @package namespace App\Transformers;
 */
class NoteTransformer extends TransformerAbstract
{
    /**
     * Transform the \Post entity
     * @param Note $model
     *
     * @return array
     */
    public function transform(Note $model)
    {
        return [
            'id' => (int)$model->id,

            'title' => $model->title,
            'quote' => $model->quote,
            'body' => $model->body,
            'ytd' => $model->ytd,
            'pick' => $model->pick,
            'stockLink' => $model->stockLink,

            'created_at' => $model->created_at->diffForHumans(),
            'updated_at' => $model->updated_at->diffForHumans()
        ];
    }


}
