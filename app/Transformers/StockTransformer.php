<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Stock;

/**
 * Class PostTransformer
 * @package namespace App\Transformers;
 */
class StockTransformer extends TransformerAbstract
{
    /**
     * Transform the \Post entity
     * @param Stock $model
     *
     * @return array
     */
    public function transform(Stock $model)
    {
        return [
            'id' => (int)$model->id,

            'code' => $model->code,
            'name' => $model->name,
            'recommendation' => $model->recommendation,
            'returnSince' => $model->returnSince,
            'sharePrice' => $model->sharePrice,
            'price' => $model->price,
            'dividend' => $model->dividend,
            'lastYearReturn' => $model->lastYearReturn,
            'marketCap' => $model->marketCap,
            'region' => $model->region,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }


}
