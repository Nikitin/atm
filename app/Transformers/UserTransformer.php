<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;

/**
 * Class PostTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the \Post entity
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id' => (int)$model->id,

            'firstName' => $model->firstName,
            'lastName' => $model->lastName,
            'email' => $model->email,
            'dayTimePhone' => $model->dayTimePhone,
            'mobilePhone' => $model->mobilePhone,
            //'postcode' => $model->postCode,
            'comment' => $model->comment,
            'status' => $model->getStatusName(),
            'ends' => $this->calculateDate($model),
            //'listenFrom' => $model->listenFrom,
            'isOnline' => $model->isOnline,

            //'created_at' => $model->created_at,
            //'updated_at' => $model->updated_at,
            'deleted_at' => $model->deleted_at ? 'Yes' : 'No'
        ];
    }

    private function calculateDate(User $model){
        if($model->status == 'trial' && $model->payment_date){
            return $model->payment_date->addDays(15)->toDateTimeString();
        }

        if($model->status == 'full' && $model->payment_date){
            if (strpos($model->transaction_subject,'6 Months') !== false) {
                return $model->payment_date->addDays(183)->toDateTimeString();
            }
            if (strpos($model->transaction_subject,'12 Months') !== false) {
                return $model->payment_date->addDays(365)->toDateTimeString();
            }
        }
        return null;
    }


}
