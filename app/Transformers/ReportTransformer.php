<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Report;

/**
 * Class PostTransformer
 * @package namespace App\Transformers;
 */
class ReportTransformer extends TransformerAbstract
{
    /**
     * Transform the \Post entity
     * @param Report $model
     *
     * @return array
     */
    public function transform(Report $model)
    {
        return [
            'id' => (int)$model->id,

            'author' => $model->author,
            'name' => $model->name,
            'description' => $model->description,
            'topTrades' => (int)$model->topTrades,
            'monthly' => (int)$model->monthly,
            'daily' => (int)$model->daily,
            'investor' => (int)$model->investor,

            'created_at' => $model->created_at->diffForHumans(),
            'updated_at' => $model->updated_at->diffForHumans()
        ];
    }


}
