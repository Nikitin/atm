<?php namespace App\Models;

use App\Models\User as UserModel;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\User
 *
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $password
 * @property string $dayTimePhone
 * @property string $mobilePhone
 * @property string $postCode
 * @property string $comment
 * @property string $sessionId
 * @property string $userHash
 * @property string $ip
 * @property string $listenFrom
 * @property string $status
 * @property string $isOnline
 * @property string $remember_token
 * @property string $activation_code
 * @property string $transaction_subject
 * @property string $payer_email
 * @property \Carbon\Carbon $payment_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|Permission[] $permissions
 * @method static Builder|UserModel whereId($value)
 * @method static Builder|UserModel whereFirstName($value)
 * @method static Builder|UserModel whereLastName($value)
 * @method static Builder|UserModel whereEmail($value)
 * @method static Builder|UserModel wherePassword($value)
 * @method static Builder|UserModel whereDayTimePhone($value)
 * @method static Builder|UserModel whereMobilePhone($value)
 * @method static Builder|UserModel wherePostCode($value)
 * @method static Builder|UserModel whereComment($value)
 * @method static Builder|UserModel whereListenFrom($value)
 * @method static Builder|UserModel whereStatus($value)
 * @method static Builder|UserModel whereIsOnline($value)
 * @method static Builder|UserModel whereRememberToken($value)
 * @method static Builder|UserModel whereCreatedAt($value)
 * @method static Builder|UserModel whereUpdatedAt($value)
 * @property string $postcode
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePostcode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereSessionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUserHash($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereActivationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereTransactionSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePayerEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePaymentDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereDeletedAt($value)
 * @mixin \Eloquent
 */
class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstName', 'lastName', 'email', 'password', 'dayTimePhone', 'mobilePhone', 'postcode', 'comment',
        'listenFrom', 'status', 'sessionId', 'ip', 'isOnline', 'transaction_subject', 'payment_date'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'payment_date'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'sessionId'];

    public function isAdmin()
    {
        return $this->status === 'admin';
    }

    public function isBlocked()
    {
        return $this->status === 'blocked';
    }

    public function isTrial()
    {
        return $this->status === 'trial';
    }

    public function isSubscriber()
    {
        return $this->status === 'subscriber';
    }

    public function isActive()
    {
        return $this->status !== 'notActive' && $this->status !== 'subscriber';
    }

    public function isMember()
    {
        return ($this->status === 'trial' || $this->status === 'full');
    }

    public function isOnline()
    {
        //return ($this->isOnline === 'Yes') ? true : false;
        return ($this->sessionId === \Session::getId());
    }

    public function checkIsSameSession()
    {
        return ($this->sessionId === \Session::getId());
    }

    public function checkIsSameIp($ip = '')
    {
        return ($this->ip === $ip);
    }

    public function swap()
    {
        $hash = bcrypt(auth()->user()->getKey() . microtime());

        \Session::put('userHash', $hash);

        $this->userHash = $hash;
        $this->isOnline = 'Yes';
        $this->save();
    }

    public function isExpired()
    {

        if ($this->status == 'trial' && $this->payment_date) {
            return $this->payment_date->addDays(15)->toDateTimeString() < Carbon::now()->toDateTimeString();
        }

        if ($this->status == 'full' && $this->payment_date) {
            if (strpos($this->transaction_subject, '6 Months') !== false) {
                return $this->payment_date->addDays(183)->toDateTimeString() < Carbon::now()->toDateTimeString();
            }
            if (strpos($this->transaction_subject, '12 Months') !== false) {
                return $this->payment_date->addDays(365)->toDateTimeString() < Carbon::now()->toDateTimeString();
            }
            if (strpos($this->transaction_subject, '1 Month') !== false) {
                return $this->payment_date->addDays(31)->toDateTimeString() < Carbon::now()->toDateTimeString();
            }
        }

        if ($this->status == 'admin') {
            return false;
        }

        return true;

    }

    public function getStatusName()
    {
        if ($this->status == 'full' && $this->payment_date) {
            if (strpos($this->transaction_subject, '6 Months') !== false) {
                return 'full6';
            }
            if (strpos($this->transaction_subject, '12 Months') !== false) {
                return 'full12';
            }
        }
        if ($this->status == 'full' && !$this->payment_date) {
            return 'dummy full';
        }
        return $this->status;
    }


}
