<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * App\User
 *
 * @property integer $id
 * @property string $title
 * @property string $pick
 * @property string $ytd
 * @property string $stockLink
 * @property string $quote
 * @property string $slug
 * @property string $body
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereQuote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereYtd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note wherePick($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Note whereStockLink($value)
 * @mixin \Eloquent
 */
class Note extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'quote', 'body', 'pick', 'ytd', 'stockLink'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

}
