<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Document
 *
 * @property string $id
 * @property string $author
 * @property string $name
 * @property string $type
 * @property string $category
 * @property string $categoryId
 * @property string $status
 * @property string $path
 * @property string $documentId
 * @property string $viewUrl
 * @property string $assetsUrl
 * @property string $realtimeUrl
 * @property string $expiresAt
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereAuthor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereDocumentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereViewUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereAssetsUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereRealtimeUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document whereExpiresAt($value)
 * @mixin \Eloquent
 */
class Document extends Model {

	protected $table = 'documents';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['author' , 'name', 'type', 'category', 'category_id', 'status', 'path', 'documentId', 'viewUrl',
        'assetsUrl', 'realtimeUrl', 'expiresAt'];

}
