<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Stock
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $recommendation
 * @property string $returnSince
 * @property string $sharePrice
 * @property string $price
 * @property string $dividend
 * @property string $lastYearReturn
 * @property string $marketCap
 * @property string $region
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $risk
 * @property string $capitalReturn
 * @property string $totalReturn
 * @property string $recommendation_date
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereRecommendation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereReturnSince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereSharePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereDividend($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereLastYearReturn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereMarketCap($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereRisk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereCapitalReturn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereTotalReturn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock whereRecommendationDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock documents()
 * @mixin \Eloquent
 */
class Stock extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'recommendation', 'recommendation_date', 'returnSince', 'sharePrice', 'price',
        'dividend', 'lastYearReturn', 'marketCap', 'region', 'capitalReturn', 'totalReturn', 'risk'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stocks';

    protected $dates = ['recommendation_date'];

    /**
     * Returns stocks with associated documents
     *
     * @param $query
     * @return mixed
     */
    public function scopeDocuments($query)
    {
        return $query->join('documents', 'stocks.id', '=', 'documents.categoryId')
            ->where('documents.category', 'stocks')
            ->select('documents.*');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = [''];
}
