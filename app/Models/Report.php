<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Report
 *
 * @property integer $id
 * @property string $name
 * @property string $author
 * @property string $description
 * @property integer $topTrades
 * @property integer $monthly
 * @property integer $daily
 * @property integer $investor
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereAuthor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereTopTrades($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereMonthly($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereDaily($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereInvestor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report documents()
 * @mixin \Eloquent
 */
class Report extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'author', 'description', 'topTrades', 'monthly', 'daily', 'investor'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports';

    /**
     * Returns reports with associated documents
     *
     * @param $query
     * @return mixed
     */
    public function scopeDocuments($query)
    {
        return $query->join('documents', 'reports.id', '=', 'documents.categoryId')
            ->where('documents.category', 'reports')
            ->select('documents.*');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = [''];
}
