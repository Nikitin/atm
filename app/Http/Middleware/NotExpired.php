<?php namespace App\Http\Middleware;

use Closure;

class NotExpired {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (me()->isExpired()) {
            return redirect('/expired');
        }

        return $next($request);
    }

}
