<?php namespace App\Http\Middleware;

use Closure;

class MustBeBlocked {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()) {
            if (me()->isMember() && me()->isOnline()) {
                return redirect('/member');
            }
            if (me()->isBlocked()) {
                return $next($request);
            }
        }

        return redirect('/auth/login');
    }

}
