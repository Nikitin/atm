<?php namespace App\Http\Middleware;

use Closure;

class CheckUserSession {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userHash   = \Session::get('userHash');
        $sessionId = \Session::getId();
        // Alex, please handle this. If we do register a new user, we are always redirected to the index page
        /*
        if (!auth()->guest() && me()->userHash != $userHash) {
            \Session::getHandler()->destroy($sessionId);
            return redirect('/');
        }*/

        return $next($request);
    }

}
