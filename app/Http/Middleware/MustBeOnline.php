<?php namespace App\Http\Middleware;

use Closure;

class MustBeOnline {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()) {
            if(me()->isBlocked()){
                return redirect('/blocked');
            }
            if (me()->isMember() && me()->isOnline) {
                return $next($request);
            }
        }
        return redirect('/auth/login');
    }

}
