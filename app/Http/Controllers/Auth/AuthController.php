<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Illuminate\Http\Request;
use Log;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $loginPath = '/auth/login';
    protected $redirectTo = '/index';
    protected $redirectPath = '/auth/login';
    protected $redirectAfterLogout = '/auth/login';

    /**
     * Create a new authentication controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getRegisterTopTen()
    {
        return view('auth.registerTopTen');
    }

    public function getRegisterSubscriber($email, $name="")
    {
       return view('auth.registerSubscriber')
            -> with('email', $email)
            -> with ('name', $name);
    }

    public function getRegisterSubscriberNoParams()
    {
        return view('auth.registerSubscriber');
    }

    public function getUpgradeTrial($email)
    {
        if (Auth::check()) {
            return view('member.renew')
                ->with('message','WELCOME '. me()->firstName .'!<br>SELECT A PLAN BELOW TO ACCESS FULL ATM EQUITY PORTFOLIOS AND ANALYSIS');;
        }
        $user = User::where('email', $email)->first();
        if (is_null($user))
            return view('auth.login');
        Auth::login($user);
        $this->authenticated(new \Request(), $user);
        return view('member.renew')
            ->with('message','WELCOME '. $user->firstName .'!<br>SELECT A PLAN BELOW TO ACCESS FULL ATM EQUITY PORTFOLIOS AND ANALYSIS');
    }
    /**
     * Activate newly registered user
     * @param string $code
     * @param string $email
     * @return \Illuminate\Http\Response
     */
    public function getActivate($code, $email)
    {
        if (Auth::check()) {
            Auth::logout();
        }
        $user = User::where('email', $email)->first();
        if ($user && ($user->activation_code == $code) && ($user->status == 'notActive')) {
            $user->update(['status' => 'trial', 'activation_code' => '']);
            Auth::login($user);
            return $this->authenticated(new \Request(), $user);
        }
        return redirect('/auth/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $user = User::withTrashed()->whereEmail($data['email'])->first();
        if (isset($user) && $user != null) {
            if ($user->status == "subscriber") {
                $user->forceDelete(); //pomer Trofim da i hooi s nim
            }
        }

        $validator = Validator::make($data, [
            'firstName' => 'required|max:255',
            //'lastName' => 'required|max:255',
            'mobilePhone' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
        ]);

        $validator->after(function($validator) use($data) {

            $phone = isset($data['mobilePhone']) ? $data['mobilePhone'] : '';
            if (!$phone) {
                return $validator->errors()->add('mobilePhone', 'Phone number not entered.');
            }
            if (!strpos($phone, '+')) {
                $phone = "+" . $phone;
            }
            /*$number = \DB::table('numbers')->where('phone', $phone)->first();
            if (!$number || !$number->verified) {
                return $validator->errors()->add('mobilePhone', 'Your phone number not validated.');
            }*/
        });

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->firstName = $data['firstName'];
        $user->lastName = isset($data['lastName']) ? $data['lastName'] : "";
        $user->dayTimePhone = isset($data['dayTimePhone']) ? $data['dayTimePhone'] : '';
        $user->mobilePhone = $data['mobilePhone'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->status = 'notActive';
        /*$activation_code = str_random(60);
        $user->activation_code = $activation_code;*/
        //$user->save();
        if ($user->save()) {
            $data = [
                'email' => $user->email,
                'name' => $user->firstName,
            ];
            $this->sendMail($data);
        }
        return $user;
    }

    function authenticated($request, $user)
    {
        $user->swap();
        if ($user->isAdmin()) {
            return redirect('/admin/#/home');
        } elseif ($user->isBlocked()) {
            return redirect('/blocked');
        } elseif ($user->isSubscriber()) {
            return redirect('/auth/register');
        } elseif (!$user->isActive()) {
            return redirect('/become-member');
        } else {
            return redirect('disclaimer');
        }

    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */

    public function logSignupError()
    {
        $input = \Input::all();
        unset($input["_token"]);
        Log::info("USER TRIED TO SIGN UP", $input);
    }

    public function getLogout()
    {
        $this->setUserIsOnline('No', true);
        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    protected function setUserIsOnline($isOnline = 'No', $removeSession = false)
    {
        if (Auth::check()) {
            $user = User::find(me()->id);
            if ($removeSession) {
                $user->sessionId = null;
            }
            $user->isOnline = $isOnline;
            $user->save();
            return Auth::setUser($user);
        }
        return null;
    }

    protected function sendMail($data)
    {
        \Mail::send('emails.welcome', $data, function ($m) use ($data) {
            $m->to($data['email'], $data['name'])->subject('Welcome to Australasian Trading Management!');
        });
    }
}
