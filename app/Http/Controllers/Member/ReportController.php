<?php namespace App\Http\Controllers\Member;

use App\Models\Document;
use App\Models\Report;
use App\Models\Note;

class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }

    public function index()
    {
        $topTrades = Report::where('topTrades', 1)->take(3)->latest()->get();
        $monthly = Report::where('monthly', 1)->take(3)->latest()->get();
        $daily = Report::where('daily', 1)->take(3)->latest()->get();
        $investor = Report::where('investor', 1)->take(3)->latest()->get();
        $ytdNotes = Note::take(2)->where('pick', '1')->latest()->get();
        return view('member.report.index', compact('topTrades', 'monthly', 'daily', 'investor'))
            ->with('ytdNotes', ($ytdNotes->count() == 2) ? $ytdNotes : null);
    }

    public function getByType($type) {
        $reports = Report::where($type, 1)->latest()->paginate(25);
        $ytdNotes = Note::take(2)->where('pick', '1')->latest()->get();
        $report = $reports[0];
        $doc = Document::where('category', 'reports')->where('categoryId', $report->id)->orderBy('id', 'DESC')->first();
        return view('member.report.byType')->with('reports', $reports)
            ->with('type', $type)
            ->with('doc', $doc)
            ->with('ytdNotes', ($ytdNotes->count() == 2) ? $ytdNotes : null);
    }

    public function show($type, $id){
        $report = Report::where($type, 1)->where('id', $id)->first();
        $reports = Report::where($type, 1)->where('id', '<>',  $id)->take(3)->latest()->get();
        if(!$report){
            return redirect()->back();
        }
        $doc = Document::where('category', 'reports')->where('categoryId', $report->id)->orderBy('id', 'DESC')->first();

        return view('member.report.show')->with('report', $report)
            ->with('reports', $reports)
            ->with('type', $type)
            ->with('doc', $doc);
    }

}