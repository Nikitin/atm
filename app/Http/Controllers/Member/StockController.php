<?php namespace App\Http\Controllers\Member;

use App\Models\Document;
use App\Models\Stock;

class StockController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }


    public function index($region = 'au')
    {
        $stocks = Stock::where('region', $region)->orderBy('name', 'asc')->paginate(100);
        return view('member.stock.index')
            ->with('stocks', $stocks);
    }

    public function show($code, $docId = null){
        $stock = Stock::where('code',$code)->first();
        if(!$stock){
            return redirect()->back();
        }
        $documents = Document::where('category', 'stocks')->where('categoryId', $stock->id)->orderBy('id', 'DESC')->get();
        $doc = null;
        if(!$docId){
            $doc = $documents->first();
        }else {
            $doc = $documents->filter(function ($item) use ($docId) {
                return $item->id == $docId;
            })->first();
        }
        return view('member.stock.show')->with('stock', $stock)
            ->with('doc', $doc)
            ->with('documents', $documents);
    }

}