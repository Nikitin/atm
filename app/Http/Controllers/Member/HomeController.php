<?php namespace App\Http\Controllers\Member;

use App\Models\Document;
use App\Models\Note;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use DateTime;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }

    public function index()
    {
        $document = Document::where('path', 'member/home/forHomePage.pdf')->first();
        $topTrades = null;
        $chart = null;
        if (Storage::exists('member/home/topTrades.csv')) {
            $topTrades = Reader::createFromPath(storage_path('app/member/home/topTrades.csv'));
        }
        if (Storage::exists('member/home/chartData.csv')) {
            $chart = Reader::createFromPath(storage_path('app/member/home/chartData.csv'));
        }
        $ytdNotes = Note::take(2)->where('pick', '1')->latest()->get();
        return view('member.home')
            ->with('topTrades', $topTrades ? $topTrades->fetchAll() : null)
            ->with('chart', $chart ? $chart->fetchAll() : null)
            ->with('ytdNotes', ($ytdNotes->count() == 2) ? $ytdNotes : null)
            ->with('document', $document ? $document : null);
    }

    public function AUPortfolio()
    {
        $csvs = [];
        $names = ['topTenHoldings', 'regionalBreakdown', 'sectorBreakdown', 'lastRight', 'chartOfMoment', 'newZelandPortfolioPerformance'];
        foreach ($names as $name) {
            $path = 'member/AUPortfolio/' . $name . '.csv';
            if (Storage::exists($path)) {
                $csvs[$name] = Reader::createFromPath(storage_path('app/' . $path))->fetchAll();
            } else {
                $csvs[$name] = null;
            }
        }
        $text = Post::where('type', 'AUPortfolio')->where('title', 'AUPortfolio')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '3-Feb-2016')) {
            $trial = "true";
        }
        return view('member.auportfolio')->with('csvs', $csvs)->with('text', $text)
            ->with('trial', $trial)
            ->with('chart', $csvs['chartOfMoment'] ? $csvs['chartOfMoment'] : null)
            ->with('topTen', $csvs['topTenHoldings'] ? $csvs['topTenHoldings'] : null)
            ->with('thematicExpsoure', $csvs['sectorBreakdown'] ? $csvs['sectorBreakdown'] : null)
            ->with('industryWeightings', $csvs['regionalBreakdown'] ? $csvs['regionalBreakdown'] : null);
    }

    public function NZPortfolio()
    {
        $csvs = [];
        $names = ['topTenHoldings', 'regionalBreakdown', 'sectorBreakdown', 'lastRight', 'chartOfMoment', 'newZelandPortfolioPerformance'];
        foreach ($names as $name) {
            $path = 'member/NZPortfolio/' . $name . '.csv';
            if (Storage::exists($path)) {
                $csvs[$name] = Reader::createFromPath(storage_path('app/' . $path))->fetchAll();
            } else {
                $csvs[$name] = null;
            }
        }
        $chart = null;
        if (Storage::exists('member/NZPortfolio/chartOfMoment.csv')) {
            $chart = Reader::createFromPath(storage_path('app/member/NZPortfolio/chartOfMoment.csv'));
        }
        $text = Post::where('type', 'NZPortfolio')->where('title', 'NZPortfolio')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '4-Feb-2016')) {
            $trial = "true";
        }
        return view('member.nzportfolio')->with('csvs', $csvs)->with('text', $text)
            ->with('trial', $trial)
            ->with('chart', $csvs['chartOfMoment'] ? $csvs['chartOfMoment'] : null)
            ->with('thematicExpsoure', $csvs['sectorBreakdown'] ? $csvs['sectorBreakdown'] : null)
            ->with('industryWeightings', $csvs['regionalBreakdown'] ? $csvs['regionalBreakdown'] : null);
    }


    public function Thematics()
    {
        $text = Post::where('type', 'Thematics')->where('title', 'Thematics')->first();
        return view('member.thematics')->with('text', $text);
    }


    public function ConcentratedPortfolio()
    {
        $csvs = [];
        $names = ['auIndustryWeightings', 'nzIndustryWeightings', 'auThematicExposure', 'nzThematicExposure'];
        foreach ($names as $name) {
            $path = 'member/Concentrated/' . $name . '.csv';
            if (Storage::exists($path)) {
                $csvs[$name] = Reader::createFromPath(storage_path('app/' . $path))->fetchAll();
            } else {
                $csvs[$name] = null;
            }
        }
        $text = Post::where('type', 'Concentrated')->where('title', 'Concentrated')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '3-Feb-2016')) {
            $trial = "true";
        }
        return view('member.concentrated-portfolio')->with('csvs', $csvs)->with('text', $text)
            ->with('trial', $trial);
    }


    /*public function getChartData()
    {
        if (Storage::exists('member/home/chartData.csv'))
            return Storage::get('member/home/chartData.csv');
        return null;
    }*/


}