<?php namespace App\Http\Controllers\Member;

use App\Models\User;
use Request;
use Auth;

class ContactDetailsController extends Controller
{

    /**
     * Create a new controller instance.
     */

    public function index()
    {
        $firstName = me() -> firstName;
        $mobilePhone = me() -> mobilePhone;
        $dayTimePhone = me() -> dayTimePhone;
        return view('member.contact-details')
            ->with('firstName', $firstName)
            ->with('mobilePhone', $mobilePhone)
            ->with('dayTimePhone', $dayTimePhone);
    }

    public function postIndex()
    {
        $user = User::find(me()->id);
        $user -> firstName = Request::get('firstName');
        $user -> dayTimePhone = Request::get('dayTimePhone');
        $user -> mobilePhone = Request::get('mobilePhone');
        $user -> save();
        Auth::setUser($user);

        $firstName = Request::get('firstName');
        $mobilePhone = Request::get('mobilePhone');
        $dayTimePhone = Request::get('dayTimePhone');
        return view('member.contact-details')
            ->with('success', 'true')
            ->with('firstName', $firstName)
            ->with('mobilePhone', $mobilePhone)
            ->with('dayTimePhone', $dayTimePhone);
    }


    public function update_email()
    {
        return view('member.update-email');
    }

    public function postUpdateEmail()
    {
        $user = User::find(me()->id);
        $user -> email = Request::get('newEmail');
        $user -> save();
        Auth::setUser($user);
        return view('member.update-email')
            ->with('success', 'true');
    }

    public function subscription()
    {

        if(me()->status == 'trial' && me()->payment_date){
            $end_date = me()->payment_date->addDays(15)->toDateString();
            $subscription = "trial";
            $message = "UPGRADE SUBSCRIPTION NOW";
        }

        if(me()->status == 'full' && me()->payment_date){
            if (strpos(me()->transaction_subject,'6 Months') !== false) {
                $end_date = me()->payment_date->addDays(183)->toDateString();
                $subscription = "1 Month";
            }
            if (strpos(me()->transaction_subject,'6 Months') !== false) {
                $end_date = me()->payment_date->addDays(183)->toDateString();
                $subscription = "6 Months";
            }
            if (strpos(me()->transaction_subject,'12 Months') !== false) {
                $end_date = me()->payment_date->addDays(365)->toDateString();
                $subscription = "12 Months";
            }
            $message = "RENEW SUBSCRIPTION NOW";
        }
        return view('member.subscription') -> with('subscription', $subscription)
            ->with('end_date',$end_date)
            ->with('subscription',$subscription)
            ->with('message',$message);
    }

    public function renew()
    {

        if(me()->status == 'trial'){
            $message = "PLEASE CHOOSE A PLAN TO UPGRADE YOUR SUBSCRIPTION";
        }
        else{
            $message = "PLEASE CHOOSE A PLAN TO RENEW YOUR SUBSCRIPTION";
        }
        return view('member.renew') -> with('message', $message);
    }

}