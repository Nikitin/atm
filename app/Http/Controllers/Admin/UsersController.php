<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repos\UserRepository;
use League\Csv\Writer;
use App\Models\User;


class UsersController extends Controller
{
    private $repo;

    /**
     * @param  \App\Repos\UserRepository $repo
     */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(forRestmod($this->repo->scopeQuery(function ($query) {
            return $query->withTrashed();
        })->all(), 'users'));
    }

    /**
     * Get All Users as csv file
     */
    public function create()
    {
        $users = $this->repo->scopeQuery(function ($query) {
            return $query->withTrashed();
        })->all();

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne(['id', 'firstName', 'lastName', 'email', 'dayTimePhone', 'mobilePhone', 'comment', 'status', 'ends', 'deleted']);

        foreach ($users['data'] as $user) {
            unset($user['isOnline']);
            $csv->insertOne($user);
        }

        $csv->output('users.csv');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if ($input['status'] == 'trial' || $input['status'] == 'full6' || $input['status'] == 'full12') {
            $input['payment_date'] = Carbon::now('Pacific/Auckland');
            if($input['status'] == 'trial'){
                $input['transaction_subject'] = 'Trial button';
            }
            if($input['status'] == 'full6' || $input['status'] == 'full12') {
                if ($input['status'] == 'full12') {
                    $input['transaction_subject'] = 'ATM 12 Months Subscription';
                }
                if ($input['status'] == 'full6') {
                    $input['transaction_subject'] = 'ATM 6 Months Subscription';
                }
                $input['status'] = 'full';
            }
        }
        $input['password'] = bcrypt($input['password']);
        if ($this->repo->create($input)) {
            return $this->respondOK('Resource created');
        } else {
            return $this->errorNotFound('resource not created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(forRestmod($this->repo->find($id), 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findOrFail($id);
        if (($user->status !== $input['status']) && ($input['status'] == 'trial' || $input['status'] == 'full6' || $input['status'] == 'full12')) {
            $input['payment_date'] = Carbon::now('Pacific/Auckland');
            if($input['status'] == 'trial'){
                $input['transaction_subject'] = 'Trial button';
            }
            if($input['status'] == 'full6' || $input['status'] == 'full12') {
                if ($input['status'] == 'full12') {
                    $input['transaction_subject'] = 'ATM 12 Months Subscription';
                }
                if ($input['status'] == 'full6') {
                    $input['transaction_subject'] = 'ATM 6 Months Subscription';
                }
                $input['status'] = 'full';
            }
        }
        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }
        if ($this->repo->update($input, $id)) {
            return $this->respondOK("Resource updated");
        } else {
            return $this->errorNotFound('resource not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('users')->where('id', '=', $id)->delete();
        //$this->repo->delete($id);
        //$user = User::withTrashed()->whereId($id)->first();
        //$user->forceDelete(); // yes, customer wants us to delete users, not store them. If we would not need this, it's better to comment out these 2 lines.
    }
}
