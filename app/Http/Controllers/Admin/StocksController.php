<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\RemoveDocument;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repos\StockRepository;
use Validator;

class StocksController extends Controller
{
    private $repo;
    /**
     * @param  \App\Repos\StockRepository $repo
     */
    public function __construct(StockRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(forRestmod($this->repo->all(), 'stocks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if($this->repo->create($input)){
            return $this->respondOK('resource created');
        }else{
            return $this->errorNotFound('resource not created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(forRestmod($this->repo->find($id), 'stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        if($this->repo->update($input, $id)){
            return $this->respondOK("resource updated");
        }else{
            return $this->errorNotFound('resource not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->repo->delete($id)) {
            $this->dispatch(new RemoveDocument('stocks', $id));
            $path = public_path().'/uploads/stocks/' . $id;
            if (\File::exists($path)) {
                \File::deleteDirectory($path);
            }
        }
    }

    /**
     * Manipulate stock's image
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function image($id)
    {
        $stock = Stock::where('id', $id)->first();
        if(!$stock){
            return $this->errorNotFound('there is no such resource');
        }
        $path = public_path().'/uploads/stocks/' . $id;
        if (\Request::isMethod('post')) {
            $input = \Request::all();
            $file = $input['file'];
            $rules = array('file' => 'required|image');
            $validator = Validator::make(array('file' => $file), $rules);
            if ($validator->passes()) {
                $name = 'image.' . $file->guessExtension();
                $name260 ="image-260x260-resize." . $file->guessExtension();
                $name200 ="image-200x200-resize.". $file->guessExtension();
                if(!\File::exists($path)){
                    \File::makeDirectory($path, 0775, true);
                }else{
                    \File::deleteDirectory($path);
                    if(!\File::exists($path)){
                        \File::makeDirectory($path, 0775, true);
                    }
                }
                if($file->move($path, $name))
                {
                    $file = $path . '/' . $name;
                    $resizedFile = $path . '/' . $name260;
                    app('App\Http\Controllers\Admin\NotesController')->smart_resize_image($file , null, 260, 260, false , $resizedFile , false , false ,100 );

                    $resizedFile = $path . '/' . $name200;
                    app('App\Http\Controllers\Admin\NotesController')->smart_resize_image($file , null, 200, 200, false , $resizedFile , false , false ,100 );
                }
                $stock->image = '/uploads/stocks/' . $id . '/' . $name;
                $stock->update();
                return $this->respondOK('file uploaded');
            }
            return $this->errorInternalError('file not uploaded, failed to pass validation');
        } elseif (\Request::isMethod('get')) {
            if($stock->image){
                if (\File::exists(public_path().$stock->image)) {
                    $type = pathinfo($stock->image, PATHINFO_EXTENSION);
                    $data = \File::get(public_path().$stock->image);
                    $output = [];
                    $output['image'] = 'image';
                    $output['data'] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    return json_encode($output);
                }
            }else{
                return json_encode(['image' => 'image', 'data' => '']);
            }
        } elseif (\Request::isMethod('delete')) {
            if (\File::exists($path)) {
                \File::deleteFile($path);
            }
            $stock->image = null;
            $stock->update();
        }
    }
}
