<?php namespace App\Http\Controllers\Admin;

use App\Jobs\SendFileToBoxViewer;
use App\Models\Stock;
use Carbon\Carbon;
use League\Csv\Writer;
use League\Csv\Reader;
use Request;
use Validator;
use Storage;

class HomeController extends Controller
{

    /**
     * Display a top trades table.
     *
     * @return Response
     */
    public function topTradesTable()
    {
        $csv = null;
        if (Storage::exists('member/home/topTrades.csv')) {
            $csv = Reader::createFromPath(storage_path('app/member/home/topTrades.csv'));
        } else {
            $csv = Reader::createFromString('NZDUSD,0.6358,+1.3%', "\n");
        }
        return response()->json($csv);
    }


    /**
     * Same top trades table
     * @return Response
     */
    public function saveTopTradesTable()
    {
        $input = Request::all();
        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        foreach ($input as $row) {
            $csv->insertOne($row);
        }
        Storage::put('member/home/topTrades.csv', $csv);
    }

    /**
     * Upload pdf and csv files for member home page.
     *
     * @return Response
     */
    public function upload()
    {
        $file = Request::file('file');
        $rules = array('file' => 'required|mimes:csv,txt,pdf');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            if ($file->getMimeType() == 'application/pdf') {
                Storage::put(
                    'member/home/forHomePage.pdf',
                    file_get_contents($file->getRealPath())
                );
                $this->dispatch(new SendFileToBoxViewer('member/home/forHomePage.pdf', 'unknown', 'Daily', 'pdf', 'portfolio', 0, null));
            } else {
                Storage::put(
                    'member/home/chartData.csv',
                    file_get_contents($file->getRealPath())
                );
            }
            return $this->respondOK('file uploaded');
        }
        return $this->errorInternalError('file not uploaded, failed to pass validation');
    }

    /**
     * Upload  csv files for stocks.
     *
     * @return Response
     */
    public function uploadStocks()
    {
        $file = Request::file('file');
        $rules = array('file' => 'required|mimes:csv,txt');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            if (!ini_get("auto_detect_line_endings")) {
                ini_set("auto_detect_line_endings", '1');
            }
            if ($file->getMimeType() == 'application/csv') {
                Storage::put(
                    'member/home/tempData.csv',
                    file_get_contents($file->getRealPath())
                );
            }
            $inputCsv = Reader::createFromPath(storage_path('app/member/home/tempData.csv'));
            $inputCsv->setDelimiter(',');
            foreach ($inputCsv as $key => $row) {
                if($key == 0){
                    continue;
                }
                //dd($row[0]);
                $stock = Stock::firstOrCreate(['code' => $row[0]]);
                $stock->name = $row[1];
                $stock->recommendation = $row[2];
                $stock->recommendation_date = Carbon::createFromFormat('d/m/Y', $row[3]);
                $stock->sharePrice = $row[4];
                $stock->price = $row[5];
                $stock->dividend = $row[6];
                $stock->marketCap = $row[7];
                $stock->capitalReturn = $row[8];
                $stock->totalReturn = $row[9];
                $stock->updated_at = Carbon::now();
                $stock->risk = $row[11];
                $stock->save();
                //$stock->save();
            }
            return $this->respondOK('file uploaded');
        }
        return $this->errorInternalError('file not uploaded, failed to pass validation');
    }

}