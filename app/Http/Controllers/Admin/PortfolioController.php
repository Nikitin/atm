<?php namespace App\Http\Controllers\Admin;

use App\Models\Post;
use League\Csv\Writer;
use League\Csv\Reader;
use Request;
use Validator;
use Storage;

class PortfolioController extends Controller
{

    /**
     * Display table.
     * @param string
     * @param string
     * @param int
     * @return Response
     */
    public function table($name, $portfolioType, $rowCount)
    {
        $csv = null;
        $path = 'member/' . $portfolioType . '/' . $name . '.csv';
        if (Storage::exists($path)) {
            $csv = Reader::createFromPath(storage_path('app/' . $path));
        }else {
            $string = '';
            for($i = 0; $i < $rowCount; $i++){
                $string .= 'empty,';
            }
            $csv = Reader::createFromString(rtrim($string,','), "\n");
        }
        return response()->json($csv);
    }


    /**
     * Save table
     * @return Response
     */
    public function saveTable()
    {
        $input = Request::all();
        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        foreach ($input['csv'] as $row) {
            if($row) {
                $csv->insertOne($row);
            }
        }
        $path = 'member/' . $input['type'] . '/' . $input['name'] . '.csv';
        Storage::put($path, $csv);
        return $this->respondOK('file saved');
    }

    /**
     * Upload pdf and csv files for member home page.
     *
     * @return Response
     */
    public function upload()
    {
        $input = Request::all();
        $file = $input['file'];
        $path = 'member/' . $input['portfolioType'] . '/' . $input['fileName'] . '.csv';
        $rules = array('file' => 'required|mimes:csv,txt');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            Storage::put(
                $path,
                file_get_contents($file->getRealPath())
            );
            return $this->respondOK('file uploaded');
        }
        return $this->errorInternalError('file not uploaded, failed to pass validation');
    }

    /**
     * get text data
     * @param $type string
     * @return Response
     */
    public function text($type){
        $post = Post::where('type', $type)->where('title', $type)->first();
        if(!$post){
            $body = '';
            $path = 'member/' . $type . '/centralText.html';
            if(Storage::exists($path)){
                $body = Storage::get($path);
            }
            $post = new Post();
            $post->type = $type;
            $post->title = $type;
            $post->body = $body;
        }
        return $post;
    }

    /**
     * save text data
     *
     * @return Response
     */
    public function saveText(){
        $input = Request::all();
        $post = Post::where('type', $input['type'])->where('title', $input['title'])->first();
        if(!$post){
            $post = new Post();
        }
        $post->title = $input['title'];
        $post->body = $input['text'];
        $post->type = $input['type'];
        if(isset($input['topRight']) && isset($input['topLeft'])){
            $post->topLeft = $input['topLeft'];
            $post->topRight = $input['topRight'];
        }
        $post->save();
        return $this->respondOK('text saved');
    }
}