<?php namespace App\Http\Controllers;

use App\Models\Note;
use Request;
use App\Repos\NoteRepositoryEloquent;

class NotesController extends Controller
{
    private $repo;

    /**
     * Create a new controller instance.
     * @param NoteRepositoryEloquent $repo
     */
    public function __construct(NoteRepositoryEloquent $repo)
    {
        //$this->middleware('auth'); //hope this will allow non-reistered users to view Notes
        $this->repo = $repo;
    }

    public function index()
    {
        //$notes = Note::orderBy('id', 'DESC')->paginate(10);
        $notes = $this->repo->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->skipPresenter()->paginate(10);

        return view('notes.index')
            ->with('notes', ($notes) ? $notes : null);
    }

    public function show($slug) {
        $note = Note::where('slug', $slug)->first();
        //$lastNotes =  Note::take(5)->latest()->get();
        $lastNotes = $this->repo->scopeQuery(function($query){
            return $query->take(5)->latest();
        })->skipPresenter()->all();
        return view('notes.show')
            ->with('note', ($note) ? $note : null)
            ->with('lastNotes', ($lastNotes) ? $lastNotes : null);
    }
}