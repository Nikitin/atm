<?php namespace App\Http\Controllers;

use App\Http\Requests\SubscribeRequest;
use Auth;
use App\Models\User;
use Request;
use App\Models\Note;

class IndexController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('member', ['only' => 'disclaimer']);
        $this->middleware('blocked', ['only' => 'blocked']);
    }

    public function index()
    {
        $ytdNotes = Note::take(5)->where('pick', '1')->latest()->get();
        //$notes = Note::take(4)->where('pick', '<>', '1')->latest()->get();
        $notes = Note::take(6)->latest()->get();
        return view('index.index')
            ->with('notes', ($notes->count() == 6) ? $notes : null)
            ->with('ytdNotes', ($ytdNotes->count() == 5) ? $ytdNotes : null);
    }

    public function expired()
    {
        return view('index.expired');
    }

    public function products()
    {
        return view('index.products');
    }

    public function thanks()
    {
        if (isset($_GET['tx']) && ($_GET['tx']) != null && ($_GET['tx']) != "")
        {
            $tx = $_GET['tx']; //transaction ID
            $identity = 'RsLh7L7XDQNtFSD05N7CWoGCOe5TNCmjd5XisvUAS27Yf0A78i87l7GYkym';
            $ch = curl_init();
            $url = 'https://www.paypal.com/cgi-bin/webscr';
            $fields = array(
                'cmd' => '_notify-synch',
                'tx' => $tx,
                'at' => $identity,
            );
            if ($tx=="trial0002016") //fixed value for all trial users in January
            {
                $user = User::find(me()->id);
                $user->transaction_subject = 'Trial';
                $user->status = 'trial';
                $subscription = 'Trial';
                $user->payer_email = $user->email;
                $date = new \DateTime();
                $date->setTimezone(new \DateTimezone('Pacific/Auckland'));
                $user->payment_date = $date->format('Y-m-d H:i:s');
                $user->isOnline = 'Yes';
                $user->sessionId = \Session::getId();
                $user->ip = Request::ip();
                if ($user->save()) {
                    Auth::setUser($user);
                    $data = [
                        'email' => $user->email,
                        'name' => $user->firstName,
                    ];
                    $this->sendThanks($data);
                    return view('index.thanks')->with('subscription', $subscription);
                }
                return redirect('/sorry');
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_USERAGENT, 'cURL/PHP');
            $res = curl_exec($ch);             // FUCK YEAH, IT WORKS! LET'S GO GET DRUNK AND SLEEP 24H.
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $subscription = "empty";
            if (!$res) {
                //HTTP ERROR
                return redirect('/sorry');
            } else {
                // parse the data
                $lines = explode("\n", $res);
                $keyarray = array();
                if (strcmp($lines[0], "SUCCESS") == 0) {
                    for ($i = 1; $i < count($lines) - 1; $i++) { //last string is empty
                        list($key, $val) = explode("=", $lines[$i]);
                        $keyarray[urldecode($key)] = urldecode($val);
                    }
                    $user = User::find(me()->id);
                    $user->transaction_subject = $keyarray['item_name'];
                    if ($keyarray['item_name'] === 'Trial') {
                        if ($user->payer_email) {
                            return redirect('/auth/login');
                        }
                        $user->status = 'trial';
                    } else {
                        $user->status = 'full';
                    }
                    $subscription = $keyarray['item_name'];
                    $user->payer_email = $keyarray['payer_email'];
                    $date = new \DateTime($keyarray['payment_date']);
                    $date->setTimezone(new \DateTimezone('Pacific/Auckland'));
                    $user->payment_date = $date->format('Y-m-d H:i:s');
                    $user->isOnline = 'Yes';
                    $user->sessionId = \Session::getId();
                    $user->ip = Request::ip();
                    if ($user->save()) {
                        Auth::setUser($user);
                        $data = [
                            'email' => $user->email,
                            'name' => $user->firstName,
                        ];
                        $this->sendThanks($data);
                    } else {
                        return redirect('/sorry');
                    }
                } else if (strcmp($lines[0], "FAIL") == 0) {
                    // show error message - failed transaction
                    return redirect('/sorry');
                }
            }
            curl_close($ch);
        } else {
            //show error message - no transaction code received
            return redirect('/sorry');
        }
        return view('index.thanks')->with('subscription', $subscription);
    }

    public function sorry()
    {
        return view('index.sorry');
    }

    public function pricing()
    {
        return view('index.pricing');
    }

    public function become_member()
    {
        if (auth()->guest()) {
            return redirect()->back();
        }
        return view('index.becomeMember');
    }

    public function about()
    {
        return view('index.about');
    }

    public function motifs()
    {
        return view('member.motifs.motifsIndex');
    }

    public function motifsShow()
    {
        return view('member.motifs.motifsShow');
    }

    public function subscribe()
    {
        $input = \Input::all();
        $user = User::where('email', $input['email'])->first();
        if ($user && ($user->status == 'subscriber')) {
            return \Redirect::back()->withCookie(cookie()->forever('subscriber', $input['email']));
        } elseif ($user && ($user !== 'notActive')) {
            return \Redirect::back()->withCookie(cookie()->forever('subscriber', $input['email']));
        }
        $user = new User();
        if (isset($input['name'])) {
            $user->firstName = $input['name'];
        }
        else
        {
            $user->firstName = 'Subscriber';
        }
        $user->lastName = "";
        $user->dayTimePhone = '';
        if (isset($input['mobilePhone'])) {
            $user->mobilePhone = $input['mobilePhone'];
        }
        else
        {
            $user->mobilePhone = '';
        }
        $user->email = $input['email'];
        $user->password = bcrypt(str_random(16));
        $user->activation_code = '';
        $user->status = 'subscriber';
        $address = Request::server('HTTP_REFERER') . "/success";
        $parse = parse_url($address);
        if ($user->save()) {
            if ($parse["path"] != "/success" && $parse["path"] != "//success")
                return redirect($parse["path"])->withCookie(cookie()->forever('subscriber', $input['email']))->withCookie(cookie('conversion', 'true', 1));
            else
                return redirect('/success')->withCookie(cookie()->forever('subscriber', $input['email']))->withCookie(cookie('conversion', 'true', 1));
        }
        return Redirect::back();
    }

    public function disclaimer()
    {
        if (Request::isMethod('post')) {
            $input = Request::all();
            if (isset($input['understand']) && isset($input['agreed'])) {
                $this->setUserIsOnline('Yes');
                return redirect('/member');
            } else {
                return redirect('/disclaimer');
            }
        }
        return view('index.disclaimer');
    }

    private function setUserIsOnline($isOnline = 'No')
    {
        $user = User::find(me()->id);
        $user->isOnline = $isOnline;
        $user->sessionId = \Session::getId();
        $user->ip = Request::ip();
        $user->save();
        return Auth::setUser($user);
    }

    protected function sendThanks($data)
    {
        \Mail::send('emails.thanks', $data, function ($m) use ($data) {
            $m->to($data['email'], $data['name'])->subject('Thank you for subscribing!');
        });
    }
}