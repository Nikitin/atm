<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/index', 'IndexController@index');
Route::get('/success', 'IndexController@index');
Route::get('/blocked', 'IndexController@blocked');
Route::get('/expired', 'IndexController@expired');
Route::get('/disclaimer', 'IndexController@disclaimer');
Route::get('/thanks', 'IndexController@thanks');
Route::get('/sorry', 'IndexController@sorry');
Route::get('/pricing', 'IndexController@pricing');
Route::post('/disclaimer', 'IndexController@disclaimer');
Route::post('/subscribe', 'IndexController@subscribe');

Route::get('/motifs', 'IndexController@motifs');
Route::get('/motifs-show', 'IndexController@motifsShow');

Route::get('notes', 'NotesController@index');
Route::get('notes/{slug}', 'NotesController@show');
Route::get('notes/{slug}/success', 'NotesController@show');

// About routes...
Route::get('/about', 'IndexController@about');
Route::post('/about', 'IndexController@about');

// Products routes...
Route::get('/products', 'IndexController@products');
Route::post('/products', 'IndexController@products');

// Become a member routes...
Route::get('/become-member', 'IndexController@become_member');
Route::post('/become-member', 'IndexController@become_member');

// Authentication routes...
Route::get('auth/', 'Auth\AuthController@getLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('auth/activate/{code}/{email}', 'Auth\AuthController@getActivate');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::get('auth/registerTopTen', 'Auth\AuthController@getRegisterTopTen');
Route::get('auth/registerSubscriber/{email}/{name}', 'Auth\AuthController@getRegisterSubscriber');
Route::get('auth/registerSubscriber/{email}', 'Auth\AuthController@getRegisterSubscriber');
Route::get('auth/upgradeTrial/{email}', 'Auth\AuthController@getUpgradeTrial');



Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::post('register/log', 'Auth\AuthController@logSignupError');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Twilio routes
Route::group(['prefix' => 'twilio'], function () {
    Route::post('check', 'TwilioController@check');
    Route::post('call', 'TwilioController@call');
});

// Member routes...
Route::group(['prefix' => 'member', 'namespace' => 'Member', 'middleware' => ['online', 'checkSession', 'notExpired']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('home/getChartData', 'HomeController@getChartData');
    Route::get('AUPortfolio', 'HomeController@AUPortfolio');
    Route::get('NZPortfolio', 'HomeController@NZPortfolio');
    Route::get('Thematics', 'HomeController@Thematics');
    Route::get('ConcentratedPortfolio', 'HomeController@ConcentratedPortfolio'); //remove later
    Route::get('report', 'ReportController@index');
    Route::get('report/{type}/{id}', 'ReportController@show')->where(['id' => '[0-9]+', 'type' => 'topTrades|monthly|daily|investor']);
    Route::get('report/{type}', 'ReportController@getByType')->where(['type' => 'topTrades|monthly|daily|investor']);
    Route::get('stock/{code}/{docId?}', 'StockController@show');
    Route::get('stocks/{region?}', 'StockController@index')->where(['region' => 'au|nz']);
    Route::get('stock-specific', 'StockController@index');
    Route::get('contact-details', 'ContactDetailsController@index');
    Route::post('contact-details', 'ContactDetailsController@postIndex');
    Route::get('update-email', 'ContactDetailsController@update_email');
    Route::post('update-email', 'ContactDetailsController@postUpdateEmail');
    Route::get('subscription', 'ContactDetailsController@subscription');
    Route::get('renew', 'ContactDetailsController@renew');
    Route::get('upgrade', 'ContactDetailsController@upgrade');
});


/**
 * Admin area
 */
Route::get('admin', ['middleware' => 'admin', function () {
    return view('admin/index');
}]);

Route::group(['prefix' => 'api/v1', 'namespace' => 'Admin', 'middleware' => ['admin']], function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('home/topTradesTable', 'HomeController@topTradesTable');
    Route::post('home/topTradesTable', 'HomeController@saveTopTradesTable');
    Route::post('home/upload', 'HomeController@upload');
    Route::post('home/uploadStocks', 'HomeController@uploadStocks');
    Route::get('portfolio/table/{name}/{portfolioType}/{rowCount}', 'PortfolioController@table');
    Route::post('portfolio/table', 'PortfolioController@saveTable');
    Route::post('portfolio/upload', 'PortfolioController@upload');
    Route::get('portfolio/text/{portfolioType}', 'PortfolioController@text');
    Route::post('portfolio/text', 'PortfolioController@saveText');
    //Route::get('users/export', 'UsersController@export');
    Route::resource('users', 'UsersController');
    Route::resource('notes', 'NotesController', ['except' => ['create']]);
    Route::post('notes/image/{id}', 'NotesController@image');
    Route::get('notes/image/{id}', 'NotesController@image');
    Route::delete('notes/image/{id}', 'NotesController@image');
    Route::resource('stocks', 'StocksController', ['except' => ['create']]);
    Route::post('stocks/image/{id}', 'StocksController@image');
    Route::get('stocks/image/{id}', 'StocksController@image');
    Route::resource('reports', 'ReportsController', ['except' => ['create']]);
    Route::post('documents/store', 'DocumentsController@store');
    Route::delete('documents/{id}', 'DocumentsController@destroy');
    Route::get('documents/all/{category}/{id}', 'DocumentsController@index');
});
