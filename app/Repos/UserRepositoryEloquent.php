<?php

namespace App\Repos;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Repos\UserRepository;
use App\Models\User;

/**
 * Class PostRepositoryEloquent
 * @package namespace App\Repos;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Specify Validator Rules
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'firstName' => 'required',
            //'lastName' => 'required',
            'email' => 'required|unique:users',
            'password'=> 'required|string|min:6'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'firstName' => 'required',
            //'lastName' => 'required',
            'email' => 'required|unique:users',
            'status' => 'required',
            /*'dayTimePhone' => 'required',
            'mobilePhone' => 'required',
            'postcode' => 'required'*/
        ]
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Specify User Presenter
     * @return string
     */
    public function presenter()
    {
        return "App\\Presenters\\UserPresenter";
    }
}
