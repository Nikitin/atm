<?php

namespace App\Repos;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Repos\NoteRepository;
use App\Models\Note;

/**
 * Class PostRepositoryEloquent
 * @package namespace App\Repos;
 */
class NoteRepositoryEloquent extends BaseRepository implements NoteRepository, CacheableInterface
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Note::class;
    }

    /**
     * Specify Validator Rules
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title' => 'required',
            'quote' => 'required',
            'body' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title' => 'required',
            'quote' => 'required',
            'body' => 'required',
        ]
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Specify User Presenter
     * @return string
     */
    public function presenter()
    {
        return "App\\Presenters\\NotePresenter";
    }
}
